<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/')->group(function () {
    Route::get('/','Homepage\HomepageController@index')->name('home');

    Route::get('/pendaftaran','Pendaftaran\PendaftaranController@index')->name('pendaftaran');
    Route::post('/pendaftaran','Pendaftaran\PendaftaranController@store')->name('pendaftaran_post');
    Route::get('/pendaftaran/kelurahan','Pendaftaran\PendaftaranController@getKelurahan')->name('pendaftaran.get_kelurahan');
    Route::get('/pendaftaran/sesi_vaksin','Pendaftaran\PendaftaranController@getSesi')->name('pendaftaran.get_sesi');
    Route::get('/pendaftaran/get_kabkot','Pendaftaran\PendaftaranController@getKabKot')->name('pendaftaran.get_kab_kot');
    Route::get('/pendaftaran/get_kecamatan','Pendaftaran\PendaftaranController@getKecamatan')->name('pendaftaran.get_kecamatan');

    Route::get('/faq','Faq\FaqController@index')->name('faq');
    Route::get('/konfirmasi','Konfirmasi\KonfirmasiController@index')->name('konfirmasi');
    Route::get('/konfirmasi_pdf','Konfirmasi\KonfirmasiController@konfirmasi_pdf')->name('konfirmasi_pdf');
    Route::get('/konfirmasi_pdf_aqiqah','Konfirmasi\KonfirmasiController@konfirmasi_pdf_aqiqah')->name('konfirmasi_pdf_aqiqah');
    Route::get('/format_screening_pdf','Konfirmasi\KonfirmasiController@format_screening_pdf')->name('format_screening_pdf');

    Route::get('/checking','Checking\CheckingController@index')->name('checking');
    Route::post('/checking','Checking\CheckingController@qr')->name('checking_post');

    Route::get('/pendaftaran_aqiqah','Pendaftaran\PendaftaranController@aqiqah')->name('pendaftaran-aqiqah');
    Route::post('/pendaftaran_aqiqah','Pendaftaran\PendaftaranController@store_aqiqah')->name('pendaftaran_post_aqiqah');

    Route::get('/konfirmasi_aqiqah','Konfirmasi\KonfirmasiController@konfirmasi_aqiqah')->name('konfirmasi_aqiqah');
    //login
});

Route::prefix('/admin-page')->group(function () {
    Route::get('/', 'Admin\AdminController@showFormLogin')->name('login');
    Route::post('login', 'Admin\AdminController@login')->name('login_post');
    Route::post('logout', 'Admin\AdminController@logout')->name('logout');
});

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('/admin-page')->group(function () {
        Route::get('scan', 'Admin\PendafaranListController@scan')->name('admin.scan.index');

        Route::get('data-pendaftaran-aqiqah', 'Admin\PendafaranListController@index_aqiqah')->name('admin.pendaftaran_aqiqah.index');
        Route::get('data-pendaftaran', 'Admin\PendafaranListController@index')->name('admin.pendaftaran.index');
        Route::get('data-pendaftaran/export_print', 'Admin\PendafaranListController@export_print')->name('admin.pendaftaran.export_print');
        Route::get('data-pendaftaran/export_excel', 'Admin\PendafaranListController@export_excel')->name('admin.pendaftaran.export_excel');
        Route::get('data-pendaftaran/detail', 'Admin\PendafaranListController@detail')->name('admin.pendaftaran.detail');
        Route::get('data-pendaftaran/hapus/{id}', 'Admin\PendafaranListController@HapusPendaftaran')->name('admin.pendaftaran.delete');
        Route::get('data-pendaftaran/detail-qr', 'Admin\PendafaranListController@detail_qr')->name('admin.pendaftaran.detail-qr');
        Route::post('data-pendaftaran/qr', 'Admin\PendafaranListController@qr')->name('admin.pendaftaran.qr');
        //get data sesi jadwal pendaftaran
        Route::get('data-pendaftaran/sesi-pendaftaran/{id}', 'Admin\PendafaranListController@sesiPendaftaran')->name('admin.pendaftaran.getsesi');
        Route::post('data-pendaftaran/check_in', 'Admin\PendafaranListController@check_in')->name('admin.pendaftaran.check_in');

        Route::get('data-pendaftaran/detail-qr-aqiqah', 'Admin\PendafaranListController@detail_qr_aqiqah')->name('admin.pendaftaran.detail-qr-aqiqah');


        
        Route::group(['middleware' => 'admin'], function () {
            Route::post('data-pendaftaran/approve', 'Admin\PendafaranListController@approve')->name('admin.pendaftaran.approve');
            // Route::post('data-pendaftaran/check_in', 'Admin\PendafaranListController@check_in')->name('admin.pendaftaran.check_in');
            Route::post('data-pendaftaran/cancel', 'Admin\PendafaranListController@cancel')->name('admin.pendaftaran.cancel');
            

            Route::get('faq', 'Admin\FaqController@index')->name('admin.faq.index');
            Route::get('faq/add', 'Admin\FaqController@add')->name('admin.faq.add');
            Route::post('faq/store', 'Admin\FaqController@store')->name('admin.faq.store');
            Route::get('faq/edit', 'Admin\FaqController@edit')->name('admin.faq.edit');
            Route::post('faq/update', 'Admin\FaqController@update')->name('admin.faq.update');
            Route::post('faq/delete', 'Admin\FaqController@delete')->name('admin.faq.delete');

            Route::get('syarat-ketentuan', 'Admin\SyaratController@index')->name('admin.syarat.index');
            Route::post('syarat-ketentuan/store', 'Admin\SyaratController@store')->name('admin.syarat.store');

            Route::prefix('/setting')->group(function () {
                Route::get('user-management', 'Admin\UserManagementController@index')->name('admin.user_management.index');
                Route::get('user-management/add', 'Admin\UserManagementController@add')->name('admin.user_management.add');
                Route::post('user-management/store', 'Admin\UserManagementController@store')->name('admin.user_management.store');
                Route::post('user-management/update', 'Admin\UserManagementController@update')->name('admin.user_management.update');
                Route::post('user-management/delete', 'Admin\UserManagementController@delete')->name('admin.user_management.delete');

                Route::get('jadwal-vaksinasi', 'Admin\JadwalVaksinasiController@index')->name('admin.jadwal_vaksinasi.index');
                Route::get('jadwal-vaksinasi/add', 'Admin\JadwalVaksinasiController@add')->name('admin.jadwal_vaksinasi.add');
                Route::post('jadwal-vaksinasi/store', 'Admin\JadwalVaksinasiController@store')->name('admin.jadwal_vaksinasi.store');
                Route::post('jadwal-vaksinasi/update', 'Admin\JadwalVaksinasiController@update')->name('admin.jadwal_vaksinasi.update');
                Route::post('jadwal-vaksinasi/delete', 'Admin\JadwalVaksinasiController@delete')->name('admin.jadwal_vaksinasi.delete');
                
                Route::get('setting-sub-judul', 'Admin\SettingSubJudulController@index')->name('admin.setting_subjudul.index');
                Route::post('setting-sub-judul/store', 'Admin\SettingSubJudulController@store')->name('admin.setting_subjudul.store');

                Route::get('setting-form', 'Admin\SettingFormController@index')->name('admin.setting_form.index');
                Route::post('setting-form/store', 'Admin\SettingFormController@store')->name('admin.setting_form.store');
            });
        });
    });
});