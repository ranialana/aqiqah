<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  	<title>Registrasi</title>
  	<meta name="description" content="">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="msapplication-TileColor" content="#ffc40d">
	<meta name="theme-color" content="#ffffff">
  <!-- Place favicon.ico in the root directory -->

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <!-- Vendors -->
  <link rel="stylesheet" href="{{asset('vendors/datatables/dataTables.bootstrap4.min.css')}}">

  <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">

  <meta name="theme-color" content="#fafafa">

  <style>
	  .sub_judul {
		  margin-top: -10px;
	  }
	  .sub_judul::after {
		content: none;
	  }
  </style>
</head>

<body>
  <main>
  	<section id="list">
	  	<div class="container py-5 table-content">
	    </div>
	    <div class="container table-content">
		<div class="row">
	    		<div class="col-12 d-flex flex-md-row justify-content-center">
					<img src="{{ asset('assets/img/elemen_rspv_1.png') }}" style="width:80%" class="img-responsive">
	    		</div>
				<div class="col-12 d-flex flex-md-row justify-content-center">
					<img src="{{ asset('assets/img/element_2.png') }}" style="width:80%" class="img-responsive">
	    		</div>
	    	</div>
		    <div class="row">
		    	<div class="col-sm-12">
		    		<div class="mt-4">
		    			<div class="row">
							<div class="col-12 col-md-2">
		    				</div>
		    				<div class="col-12 col-md-4">
		    					<div class="d-grid gap-2">
			    					<a href="{{route('checking')}}" class="btn btn-lg btn-primary">
			    						<i class="bi bi-search"></i> Cek RSVP
									</a>
								</div>
		    				</div>
		    				<div class="col-12 col-md-4">
		    					<div class="d-grid gap-2">
			    					<a href="{{route('pendaftaran-aqiqah')}}" class="btn btn-lg btn-secondary">
				    					<i class="bi bi-laptop"></i> Daftar RSVP
				    				</a>
				    			</div>
		    				</div>
							<div class="col-12 col-md-2">
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>

	    </div>
  	</section>
  </main>


  <!-- Footer -->
  <footer class="pt-4 pt-md-5">
  	<div class="real-footer text-center py-2">
  		<span>2022 © Kabayan</span>
  	</div>
  </footer>

  <!-- Bootstrap JS -->
  <script src="{{asset('vendors/jquery-3.5.1.js')}}"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <!-- Vendors -->

  <!-- Theme -->
  <script src="{{asset('assets/js/plugins.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>

  <script>
	$(document).ready(function() {
	    $('.datepicker').datepicker();
	});
  </script>	
</body>

</html>
