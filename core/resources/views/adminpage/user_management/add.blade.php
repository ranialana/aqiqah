@extends('adminpage.template.apps')

@section('title')
Add User - Admin
@endsection

@section('custom_css')
<style>
.select2-container {
    display:block !important
}
</style>
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active"> Add User
                                </li>
                            </ol>
                        </div>
                        <div class="col-12">
                            <h5>User</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
               <section id="basic-tabs-components">
                    <div class="card col-lg-5">
                        <div class="card-header">
                            <div class="card-title">
                                <h4>Add User</h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="id" aria-labelledby="id-tab" role="tabpanel">
                                        <form id="add_to" action="{{route('admin.user_management.store')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Nama User</label>
                                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama User" required>
                                                    </fieldset>
                                                </div>
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Email User</label>
                                                        <input type="email" class="form-control" id="email" name="email" placeholder="Masukan Email User" required>
                                                    </fieldset>
                                                </div>
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Password</label>
                                                        <input type="password" class="form-control" id="password" name="password" placeholder="Masukan Password" required>
                                                    </fieldset>
                                                </div>
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Password Kofirmasi</label>
                                                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Masukan Password Konfirmasi" required>
                                                    </fieldset>
                                                </div>
                                                @if (Auth::user()->role==1)
                                                    <div class="col-12">
                                                        <fieldset class="form-group">
                                                            <label for="basicInput">Role User</label>
                                                            <select type="text" class="form-control" id="role" name="role" placeholder="Masukan Role User" required>
                                                                @foreach ($roles as $r)
                                                                    <option value="{{$r->id}}">{{$r->nama_role}}</option>
                                                                @endforeach
                                                            </select>
                                                        </fieldset>
                                                    </div>
                                                    <div id="kota_section" class="col-12" style="display:none">
                                                        <fieldset class="form-group">
                                                            <label for="basicInput">Kota / Kabupaten</label>
                                                            <select class="form-control" id="kota_id" name="kota_id" placeholder="Masukan Kota" required>
                                                                <option value="">Pilih kota</option>
                                                                @foreach ($kota as $r)
                                                                    <option value="{{$r->kode}}">{{$r->nama}}</option>
                                                                @endforeach
                                                            </select>
                                                        </fieldset>
                                                    </div>
                                                @endif
                                                <!-- Submit Button -->
                                                <div class="col-12">
                                                    <a href="javascript:void(0)" class="btn btn-success round glow" style="float:right" onclick="submit()">
                                                        <span class="align-middle ml-25">Submit</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    $("#add_to").validate({
        // Specify validation rules
        rules: {
            password: {
                required: true,
                minlength: 6
            },
            password_confirm : {
                required: true,
                minlength : 6,
                equalTo : "#password"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $("#kota_id").select2();

    $("#role").change(function() {
        if ($(this).val()==3){
            $("#kota_section").show();
        } else {
            $("#kota_section").hide();
        }
    });

    //submit listener
    function submit(){
        if($("#add_to").valid()){
            $( "#add_to" ).submit();
            $(".cover-spin").show();
        }
    };
</script>
@endsection