@extends('adminpage.template.apps')

@section('title')
User Management - Admin
@endsection

@section('custom_css')
<style>
.select2-container {
    display:block !important
}
</style>
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-6">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">User Management
                                    </li>
                                </ol>
                            </div>
                            <div class="col-12">
                                <h5>User Management</h5>
                            </div>
                        </div>
                        <div class="col-6">
                            <a href="{{route('admin.user_management.add')}}" class="btn btn-success round glow" style="float:right">
                                <i class="bx bx-plus"></i>
                                <span class="align-middle ml-25">Tambah User</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table id="user_table" class="table table-bordered table-hover zero-configuration text-center">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Email</th>
                                                        <th>Peran</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($resultQuery as $r)
                                                    <tr>
                                                        <td>{{ $r->name }}</td>
                                                        <td>{{ $r->email }}</td>
                                                        <td>{{ $r->nama_role }}</td>
                                                        <td class="text-center">
                                                            @if($r->role_id!=1)
                                                                <button type="button" class="btn btn-icon rounded-circle btn-primary glow mr-1 mb-1" title="Sunting" onClick="popModalEdit('{{ $r->id }}','{{ $r->name }}','{{ $r->email }}','{{ $r->role }}','{{ $r->kota_id }}')"><i class="bx bxs-edit-alt"></i></button>
                                                                <button type="button" class="btn btn-icon rounded-circle btn-danger glow mr-1 mb-1" title="Delete" onClick="popModalDelete('{{ $r->id }}','{{ $r->name }}')"><i class="bx bxs-trash"></i></button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
    </div>
<!-- Modal Edit -->
<div class="modal text-left" id="modal_edit" role="dialog" aria-labelledby="myModalLabel140" aria-modal="true" style="padding-right: 16px;">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header bg-warning">
            <h5 class="modal-title white" id="myModalLabel140">Sunting User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
            <i class="bx bx-x"></i>
            </button>
        </div>
        <div class="modal-body">
            <form id="edt_form">
                <div class="row">
                    <div class="col-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Name User</label>
                            <input type="text" class="form-control" id="edt_nama" name="edt_nama" placeholder="Masukan Name User" required>
                        </fieldset>
                    </div>
                    <div class="col-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Email User</label>
                            <input type="email" class="form-control" id="edt_email" name="edt_email" placeholder="Masukan Email User" required>
                        </fieldset>
                    </div>
                    <div class="col-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Password (Isi Jika Akan Mengganti Password)</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Masukan Password Baru (Isi Jika Akan Mengganti Password)">
                        </fieldset>
                    </div>
                    <div class="col-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Password Konfirmasi (Isi Jika Akan Mengganti Password)</label>
                            <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Masukan Password Konfirmasi (Isi Jika Akan Mengganti Password)">
                        </fieldset>
                    </div>
                    <div id="kota_section" class="col-12" style="display:none">
                        <fieldset class="form-group">
                            <label for="basicInput">Kota / Kabupaten</label>
                            <select class="form-control" id="kota_id" name="kota_id" placeholder="Masukan Kota" required>
                                <option value="">Pilih kota</option>
                                @foreach ($kota as $r)
                                    <option value="{{$r->kode}}">{{$r->nama}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
            <i class="bx bx-x d-block d-sm-none"></i>
            <span class="d-none d-sm-block">Tutup</span>
            </button>

            <button type="button" class="btn btn-warning ml-1"  onClick="submitEdit()">
            <i class="bx bx-check d-block d-sm-none"></i>
            <span class="d-none d-sm-block">Sunting</span>
            </button>
        </div>
        </div>
    </div>
</div>
<!-- Modal Delete -->
<div class="modal text-left" id="modal_delete"  role="dialog" aria-labelledby="myModalLabel120" aria-modal="true" style="padding-right: 16px;">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header bg-danger">
            <h5 class="modal-title white" id="myModalLabel120">Delete User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
            <i class="bx bx-x"></i>
            </button>
        </div>
        <div class="modal-body">
            <p>Apakah anda yakin menghapus <span id="del_nama"></span> ?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                <i class="bx bx-x d-block d-sm-none"></i>
                <span class="d-none d-sm-block">Tutup</span>
            </button>
            <button type="button" class="btn btn-danger ml-1" data-dismiss="modal"  onClick="submitDelete()">
                <i class="bx bx-check d-block d-sm-none"></i>
                <span class="d-none d-sm-block">Delete</span>
            </button>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    @if(Session::get('status'))
        @if(Session::get('status') ==  1)         
            $("#toast-success-content").html("{{Session::get('status_message')}}");
            $("#toast-success").toast('show');
        @elseif (Session::get('status') ==  -1)  
            $("#toast-error-content").html("{{Session::get('status_message')}}");
            $("#toast-error").toast('show');   
        @endif
    @endif
</script>

<script>
$("#edt_form").validate({
    // Specify validation rules
    rules: {
        nama: "required",
        email: "required",
        password: {
            minlength: 6
        },
        password_confirm : {
            minlength : 6,
            equalTo : "#password"
        }
    },
    // Specify validation error message
    messages: {
        nama: "*Masukan Nama User",
        email: "*Masukan Email User"
    },
    submitHandler: function(form) {
    }
});


$("#user_table").DataTable({
    "oLanguage": {
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sSearch": "Cari:",
        "sInfo": "Tampilkan _START_ Sampai _END_ dari _TOTAL_ Entri",
        "sInfoEmpty": "Tampilkan 0 Sampai 0 dari 0 Entri",
        "sZeroRecords": "Tidak ada data yang ditemukan",
        "oPaginate": {
            "sFirst":    "Awal",
            "sLast":     "Akhir",
            "sNext":     "Selanjutkan",
            "sPrevious": "Sebelumnya"
        },
    },
    "order": [[ 1, "desc" ]]
});

var idTarget="";
function popModalEdit(id,nama,email,role,kota_id){
    idTarget = id
    $("#edt_nama").val(nama);
    $("#edt_email").val(email);
    if (role==3){
        $("#kota_section").show();
        
        $("#kota_id").val(kota_id);
        $("#kota_id").select2();
    } else {
        $("#kota_section").hide();
    }
    
    $('#modal_edit').modal('show');
}

function submitEdit(){
    $(".cover-spin").fadeIn(500)
    var edt_nama = $("#edt_nama").val();
    var edt_email = $("#edt_email").val();

    var kota_id = $("#kota_id").val();
    if ($("#edt_form").valid()){
        $.post( "{{route('admin.user_management.update')}}", { 
            "_token": "{{ csrf_token() }}", 
            "id": idTarget, 
            "nama": edt_nama,
            "email": edt_email,
            "kota_id": kota_id,
            "password":$("#password").val()
        }).done(function( res ) {
            if (res==1){
                $("#toast-success-content").html('Berhasil mengedit '+edt_nama);
                $("#toast-success").toast('show');
                setTimeout(function(){ 
                    $(".cover-spin").fadeOut(500)
                    window.location.href=window.location.href;
                }, 1000);
            } else {
                $("#toast-error-content").html("Gagal mengedit : Email "+edt_nama+" telah digunakan/tidak valid");
                $("#toast-error").toast('show');
                $(".cover-spin").fadeOut(500)
            }
        }).fail(function (jqXHR, textStatus) {
            $("#toast-error-content").html("Server Error");
            $("#toast-error").toast('show');
            $(".cover-spin").fadeOut(500)
        });
    } else {
        $("#toast-error-content").html("Form tidak valid");
        $("#toast-error").toast('show');
        $(".cover-spin").fadeOut(500)
    }
}

function popModalDelete(id,nama){
    idTarget = id;
    $("#del_nama").html(nama);
    $('#modal_delete').modal('show');
}

function submitDelete(){
    $(".cover-spin").fadeIn(500)
    del_nama = $("#del_nama").html()
    $.post( "{{route('admin.user_management.delete')}}", { "_token": "{{ csrf_token() }}", "id": idTarget })
    .done(function( res ) {
        if (res==1){
            $("#toast-success-content").html('Berhasil menghapus '+del_nama);
            $("#toast-success").toast('show');
            setTimeout(function(){ 
                $(".cover-spin").fadeOut(500)
                window.location.href=window.location.href;
            }, 1000);
        } else {
            $("#toast-error-content").html("Gagal menghapus "+del_nama);
            $("#toast-error").toast('show');
            $(".cover-spin").fadeOut(500)
        }
    }).fail(function (jqXHR, textStatus) {
        $("#toast-error-content").html("Server Error");
        $("#toast-error").toast('show');
        $(".cover-spin").fadeOut(500)
    });
}
</script>
@endsection