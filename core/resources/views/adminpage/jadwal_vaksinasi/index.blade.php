@extends('adminpage.template.apps')

@section('title')
Jawal Vaksiansi - Admin
@endsection

@section('custom_css')
<style>
.select2-container {
    display:block !important
}
</style>
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-6">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">Jadwal Vaksinasi
                                    </li>
                                </ol>
                            </div>
                            <div class="col-12">
                                <h5>Jadwal Vaksinasi</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- <div class="card-header">
                                    <div class="row">
                                        <div class="col-12">
                                            <a href="{{route('admin.jadwal_vaksinasi.add')}}" class="btn btn-primary round glow" style="float:right">
                                                <i class="bx bx-plus"></i>
                                                <span class="align-middle ml-25">Add Data</span>
                                            </a>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table id="jadwal_table" class="table table-bordered table-hover zero-configuration text-center">
                                                <thead>
                                                    <tr>
                                                        <th>Lokasi</th>
                                                        <th>Tanggal Vaksin</th>
                                                        <th>Kabupaten/Kota</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($jadwal as $f)
                                                    <tr>
                                                        <td>{{$f->lokasi}}</td>
                                                        <td>{{$f->tanggal_vaksin}}</td>
                                                        <td>{{$f["kota"]["nama"]}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon rounded-circle btn-primary glow mr-1 mb-1" title="Sunting" onClick="popModalEdit('{{ $f->id }}','{{ $f->lokasi }}','{{ $f->tanggal_vaksin }}','{{ $f->sesi }}','{{ $f->kota_id }}')"><i class="bx bxs-edit-alt"></i></button>
                                                            <button type="button" class="btn btn-icon rounded-circle btn-danger glow mr-1 mb-1" title="Sunting" onClick="popModalDelete('{{ $f->id }}')"><i class="bx bxs-trash-alt"></i></button>
                                                        </td>
                                                    </tr> 
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
    </div>

<!-- Modal Edit -->
<div class="modal text-left" id="modal_edit" role="dialog" aria-labelledby="myModalLabel140" aria-modal="true" style="padding-right: 16px;">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header bg-warning">
            <h5 class="modal-title white" id="myModalLabel140">Sunting Jadwal</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
            <i class="bx bx-x"></i>
            </button>
        </div>
        <div class="modal-body">
            <form id="edt_form">
                <div class="row">
                    <div class="col-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Lokasi</label>
                            <input type="text" class="form-control" id="edt_lokasi" name="edt_lokasi" placeholder="Masukan Lokasi" required>
                        </fieldset>
                    </div>
                    <div class="col-12">
                        <fieldset class="form-group">
                            <label for="basicInput">Tanggal Vaksin</label>
                            <input type="text" class="form-control" id="edt_tanggal" name="edt_tanggal" placeholder="Masukan Tanggal Vaksin" required>
                        </fieldset>
                    </div>
                    <div id="kota_section" class="col-12" style="display:none">
                        <fieldset class="form-group">
                            <label for="basicInput">Kota / Kabupaten</label>
                            <select class="form-control" id="kota_id" name="kota_id" placeholder="Masukan Kota" required>
                                <option value="">Pilih kota</option>
                                @foreach ($kota as $r)
                                    <option value="{{$r->kode}}">{{$r->nama}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                    </div>
                    <div id="time_section" class="col-12 row" >
                    </div>
                    <div class="col-12">
                        <fieldset class="form-group">
                            <button type="text" class="btn btn-primary" id="new_time">Tambah</button>
                        </fieldset>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
            <i class="bx bx-x d-block d-sm-none"></i>
            <span class="d-none d-sm-block">Tutup</span>
            </button>

            <button type="button" class="btn btn-warning ml-1" data-dismiss="modal" onClick="submitEdit()">
            <i class="bx bx-check d-block d-sm-none"></i>
            <span class="d-none d-sm-block">Sunting</span>
            </button>
        </div>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div class="modal text-left" id="modal_delete" role="dialog" aria-labelledby="myModalLabel140" aria-modal="true" style="padding-right: 16px;">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header bg-danger">
            <h5 class="modal-title white" id="myModalLabel140">Delete Jadwal</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
            <i class="bx bx-x"></i>
            </button>
        </div>
        <div class="modal-body">
            <form id="edt_form">
                <div class="row">
                    <div class="col-12">
                        <fieldset class="form-group">
                            <label for="pertanyaan">Apakah ingin menghapus data ?</label>
                        </fieldset>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
            <i class="bx bx-x d-block d-sm-none"></i>
            <span class="d-none d-sm-block">Tutup</span>
            </button>

            <button type="button" class="btn btn-danger ml-1" data-dismiss="modal" onClick="submitDelete()">
            <i class="bx bx-check d-block d-sm-none"></i>
            <span class="d-none d-sm-block">Hapus</span>
            </button>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

<script>
$("#jadwal_table").DataTable({
    "oLanguage": {
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sSearch": "Cari:",
        "sInfo": "Tampilkan _START_ Sampai _END_ dari _TOTAL_ Entri",
        "sInfoEmpty": "Tampilkan 0 Sampai 0 dari 0 Entri",
        "sZeroRecords": "Tidak ada data yang ditemukan",
        "oPaginate": {
            "sFirst":    "Awal",
            "sLast":     "Akhir",
            "sNext":     "Selanjutkan",
            "sPrevious": "Sebelumnya"
        },
    }
});
</script>

<script>
    @if(Session::get('status'))
        @if(Session::get('status') ==  1)         
            $("#toast-success-content").html("{{Session::get('status_message')}}");
            $("#toast-success").toast('show');
        @elseif (Session::get('status') ==  -1)  
            $("#toast-error-content").html("{{Session::get('status_message')}}");
            $("#toast-error").toast('show');   
        @endif
    @endif
</script>

<script>

$("#edt_form").validate({
    submitHandler: function(form) {
    }
});

var idTarget="";
var id_delete=[];
    function popModalEdit(id,lokasi,tanggal,sesi,kota_id){
        idTarget = id
        $("#edt_lokasi").val(lokasi);
        $("#edt_tanggal").val(tanggal);
        
        array_sesi = JSON.parse(sesi);

        @if (Auth::user()->role!=3)
            $("#kota_section").show();
            $("#kota_id").select2();
            $("#kota_id").val(kota_id).change();
        @else
            $("#kota_section").hide();
        @endif

        add_time_section(array_sesi.length);
        var i = 1;
        for (const val of array_sesi) { 
            time_awal = val.sesi.split("-")[0];
            time_akhir = val.sesi.split("-")[1];
            $("#timeAwal"+i).val(time_awal);
            $("#timeAkhir"+i).val(time_akhir);
            $("#kuota"+i).val(val.kuota);
            $("#id_exist"+i).val(val.id);
            $("#delete_kuota"+i).attr("value",val.id);
            i++;
        }

        $('#modal_edit').modal('show');
    }

    function submitEdit(){
        $(".cover-spin").fadeIn(500);

        edt_lokasi = $("#edt_lokasi").val();
        edt_tanggal = $("#edt_tanggal").val();
        kuota = $("input[name='kuota[]']").map(function(){return $(this).val();}).get()
        time_awal = $("select[name='timeAwal[]']").map(function(){return $(this).val();}).get()
        time_akhir = $("select[name='timeAkhir[]']").map(function(){return $(this).val();}).get()
        id_exist  = $("input[name='id_exist[]']").map(function(){return $(this).val();}).get()

        kuota_new = $("input[name='kuotaNew[]']").map(function(){return $(this).val();}).get()
        time_awal_new = $("select[name='timeAwalNew[]']").map(function(){return $(this).val();}).get()
        time_akhir_new = $("select[name='timeAkhirNew[]']").map(function(){return $(this).val();}).get()
        kota = $('#kota_id').val();
        $('#modal_edit').modal('show');
        if ($("#edt_form").valid()){
            $.post( "{{route('admin.jadwal_vaksinasi.update')}}", { 
                "_token": "{{ csrf_token() }}",
                "id": idTarget, 
                "lokasi": edt_lokasi, 
                "kota_id" : kota,
                "tanggal": edt_tanggal,
                "kuota": kuota,
                "timeAwal": time_awal,
                "timeAkhir": time_akhir,
                "idExist": id_exist,
                "kuotaNew": kuota_new,
                "timeAwalNew": time_awal_new,
                "timeAkhirNew": time_akhir_new,
                "idDelete": id_delete
            }).done(function( res ) {
                if (res==1){
                    $("#toast-success-content").html('Berhasil mengedit '+edt_lokasi);
                    $("#toast-success").toast('show');
                    setTimeout(function(){ 
                        $(".cover-spin").fadeOut(500)
                        window.location.href=window.location.href;
                    }, 1000);
                } else {
                    $("#toast-error-content").html("Gagal mengedit "+edt_lokasi);
                    $("#toast-error").toast('show');
                    $(".cover-spin").fadeOut(500)
                }
            }).fail(function (jqXHR, textStatus) {
                $("#toast-error-content").html("Server Error");
                $("#toast-error").toast('show');
                $(".cover-spin").fadeOut(500)
            });
        } else {
            $("#toast-error-content").html("Form tidak valid");
            $("#toast-error").toast('show');
            $(".cover-spin").fadeOut(500)
        }
    }

    function popModalDelete(id){
        idTarget = id
        $('#modal_delete').modal('show');
    }
    
    function submitDelete(){
        $.post( "{{route('admin.jadwal_vaksinasi.delete')}}", { 
            "_token": "{{ csrf_token() }}",
            "id": idTarget, 
        }).done(function( res ) {
            if (res==1){
                $("#toast-success-content").html('Berhasil delete data');
                $("#toast-success").toast('show');
                setTimeout(function(){ 
                    $(".cover-spin").fadeOut(500)
                    window.location.href=window.location.href;
                }, 1000);
            } else {
                $("#toast-error-content").html("Gagal delete data");
                $("#toast-error").toast('show');
                $(".cover-spin").fadeOut(500)
            }
        }).fail(function (jqXHR, textStatus) {
            $("#toast-error-content").html("Server Error");
            $("#toast-error").toast('show');
            $(".cover-spin").fadeOut(500)
        });
    }


    //form pop up
    var nama_sesi = 1;

    function add_time_section(jumlah_sesi){

        $(function () {
            $("#edt_tanggal").datepicker({ 
                autoclose: true, 
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        });

        $("#time_section").html(``);
        
        for (i = 1; i <= jumlah_sesi; i++){
            $("#time_section").append(`
            <div id="time_data`+i+`" class="row" style="margin-right: 0px;margin-left: 0px;">
                <input type="text" class="form-control" id="id_exist`+i+`" name="id_exist[]" style="display:none" required>
                <div class="col-4">
                    <fieldset class="form-group">
                        <label for="basicInput">Waktu Awal `+i+`</label>
                        <select class="form-control" id="timeAwal`+i+`" name="timeAwal[]" required>
                            <option value="00:00">00.00</option>
                            <option value="00:30">00.30</option>
                            <option value="01:00">01.00</option>
                            <option value="02:30">01.30</option>
                            <option value="02:00">02.00</option>
                            <option value="03:30">02.30</option>
                            <option value="03:00">03.00</option>
                            <option value="03:30">03.30</option>
                            <option value="04:00">04.00</option>
                            <option value="04:30">04.30</option>
                            <option value="05:00">05.00</option>
                            <option value="05:30">05.30</option>
                            <option value="06:00">06.00</option>
                            <option value="06:30">06.30</option>
                            <option value="07:00">07.00</option>
                            <option value="07:30">07.30</option>
                            <option value="08:00">08.00</option>
                            <option value="08:30">08.30</option>
                            <option value="09:00">09.00</option>
                            <option value="09:30">09.30</option>
                            <option value="10:00">10.00</option>
                            <option value="10:30">10.30</option>
                            <option value="11:00">11.00</option>
                            <option value="11:30">11.30</option>
                            <option value="12:00">12.00</option>
                            <option value="12:30">12.30</option>
                            <option value="13:00">13.00</option>
                            <option value="13:30">13.30</option>
                            <option value="14:00">14.00</option>
                            <option value="14:30">14.30</option>
                            <option value="15:00">15.00</option>
                            <option value="15:30">15.30</option>
                            <option value="16:00">16.00</option>
                            <option value="16:30">16.30</option>
                            <option value="17:00">17.00</option>
                            <option value="17:30">17.30</option>
                            <option value="18:00">18.00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                        </select>
                    </fieldset>
                </div>
                <div class="col-4">
                    <fieldset class="form-group">
                        <label for="basicInput">Waktu Akhir `+i+`</label>
                        <select class="form-control" id="timeAkhir`+i+`" name="timeAkhir[]" required>
                            <option value="00:00">00.00</option>
                            <option value="00:30">00.30</option>
                            <option value="01:00">01.00</option>
                            <option value="02:30">01.30</option>
                            <option value="02:00">02.00</option>
                            <option value="03:30">02.30</option>
                            <option value="03:00">03.00</option>
                            <option value="03:30">03.30</option>
                            <option value="04:00">04.00</option>
                            <option value="04:30">04.30</option>
                            <option value="05:00">05.00</option>
                            <option value="05:30">05.30</option>
                            <option value="06:00">06.00</option>
                            <option value="06:30">06.30</option>
                            <option value="07:00">07.00</option>
                            <option value="07:30">07.30</option>
                            <option value="08:00">08.00</option>
                            <option value="08:30">08.30</option>
                            <option value="09:00">09.00</option>
                            <option value="09:30">09.30</option>
                            <option value="10:00">10.00</option>
                            <option value="10:30">10.30</option>
                            <option value="11:00">11.00</option>
                            <option value="11:30">11.30</option>
                            <option value="12:00">12.00</option>
                            <option value="12:30">12.30</option>
                            <option value="13:00">13.00</option>
                            <option value="13:30">13.30</option>
                            <option value="14:00">14.00</option>
                            <option value="14:30">14.30</option>
                            <option value="15:00">15.00</option>
                            <option value="15:30">15.30</option>
                            <option value="16:00">16.00</option>
                            <option value="16:30">16.30</option>
                            <option value="17:00">17.00</option>
                            <option value="17:30">17.30</option>
                            <option value="18:00">18.00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                        </select>
                    </fieldset>
                </div>
                <div class="col-3">
                    <fieldset class="form-group">
                        <label for="basicInput">Kuota `+i+`</label>
                        <input type="number" class="form-control" id="kuota`+i+`" name="kuota[]" placeholder="Masukan Kuota" required>
                    </fieldset>
                </div>
                <div class="col-1">
                    <fieldset class="form-group">
                        <button style="margin-top:20px" type="button" class="btn btn-danger" id="delete_kuota`+i+`" value=""><i class="bx bxs-trash-alt"></i></button>
                    </fieldset>
                </div>
            </div>`)
            nama_sesi = i;

            $("#delete_kuota"+i).click(function() {
                id = $(this).attr("id").split("delete_kuota")[1];
                $("#time_data"+id).remove();
                id_delete.push($(this).attr("value"));
            });
        }
    };

    $("#new_time").click(function() {
        i = nama_sesi + 1;
        $("#time_section").append(`
            <div id="time_data`+i+`" class="row" style="margin-right: 0px;margin-left: 0px;">
                <div class="col-4">
                    <fieldset class="form-group">
                        <label for="basicInput">Waktu Awal `+i+`</label>
                        <select class="form-control" id="timeAwal`+i+`" name="timeAwalNew[]" required>
                            <option value="00:00">00.00</option>
                            <option value="00:30">00.30</option>
                            <option value="01:00">01.00</option>
                            <option value="02:30">01.30</option>
                            <option value="02:00">02.00</option>
                            <option value="03:30">02.30</option>
                            <option value="03:00">03.00</option>
                            <option value="03:30">03.30</option>
                            <option value="04:00">04.00</option>
                            <option value="04:30">04.30</option>
                            <option value="05:00">05.00</option>
                            <option value="05:30">05.30</option>
                            <option value="06:00">06.00</option>
                            <option value="06:30">06.30</option>
                            <option value="07:00">07.00</option>
                            <option value="07:30">07.30</option>
                            <option value="08:00">08.00</option>
                            <option value="08:30">08.30</option>
                            <option value="09:00">09.00</option>
                            <option value="09:30">09.30</option>
                            <option value="10:00">10.00</option>
                            <option value="10:30">10.30</option>
                            <option value="11:00">11.00</option>
                            <option value="11:30">11.30</option>
                            <option value="12:00">12.00</option>
                            <option value="12:30">12.30</option>
                            <option value="13:00">13.00</option>
                            <option value="13:30">13.30</option>
                            <option value="14:00">14.00</option>
                            <option value="14:30">14.30</option>
                            <option value="15:00">15.00</option>
                            <option value="15:30">15.30</option>
                            <option value="16:00">16.00</option>
                            <option value="16:30">16.30</option>
                            <option value="17:00">17.00</option>
                            <option value="17:30">17.30</option>
                            <option value="18:00">18.00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                        </select>
                    </fieldset>
                </div>
                <div class="col-4">
                    <fieldset class="form-group">
                        <label for="basicInput">Waktu Akhir `+i+`</label>
                        <select class="form-control" id="timeAkhir`+i+`" name="timeAkhirNew[]" required>
                            <option value="00:00">00.00</option>
                            <option value="00:30">00.30</option>
                            <option value="01:00">01.00</option>
                            <option value="02:30">01.30</option>
                            <option value="02:00">02.00</option>
                            <option value="03:30">02.30</option>
                            <option value="03:00">03.00</option>
                            <option value="03:30">03.30</option>
                            <option value="04:00">04.00</option>
                            <option value="04:30">04.30</option>
                            <option value="05:00">05.00</option>
                            <option value="05:30">05.30</option>
                            <option value="06:00">06.00</option>
                            <option value="06:30">06.30</option>
                            <option value="07:00">07.00</option>
                            <option value="07:30">07.30</option>
                            <option value="08:00">08.00</option>
                            <option value="08:30">08.30</option>
                            <option value="09:00">09.00</option>
                            <option value="09:30">09.30</option>
                            <option value="10:00">10.00</option>
                            <option value="10:30">10.30</option>
                            <option value="11:00">11.00</option>
                            <option value="11:30">11.30</option>
                            <option value="12:00">12.00</option>
                            <option value="12:30">12.30</option>
                            <option value="13:00">13.00</option>
                            <option value="13:30">13.30</option>
                            <option value="14:00">14.00</option>
                            <option value="14:30">14.30</option>
                            <option value="15:00">15.00</option>
                            <option value="15:30">15.30</option>
                            <option value="16:00">16.00</option>
                            <option value="16:30">16.30</option>
                            <option value="17:00">17.00</option>
                            <option value="17:30">17.30</option>
                            <option value="18:00">18.00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                        </select>
                    </fieldset>
                </div>
                <div class="col-3">
                    <fieldset class="form-group">
                        <label for="basicInput">Kuota `+i+`</label>
                        <input type="number" class="form-control" id="kuota`+i+`" name="kuotaNew[]" placeholder="Masukan Kuota" required>
                    </fieldset>
                </div>
                <div class="col-1">
                    <fieldset class="form-group">
                        <button style="margin-top:20px" type="button" class="btn btn-danger" id="delete_kuota`+i+`" value=""><i class="bx bxs-trash-alt"></i></button>
                    </fieldset>
                </div>
            </div>
            `)

        $("#delete_kuota"+i).click(function() {
            id = $(this).attr("id").split("delete_kuota")[1];
            $("#time_data"+id).remove();
        });

        
        nama_sesi = i;
    });
</script>
@endsection