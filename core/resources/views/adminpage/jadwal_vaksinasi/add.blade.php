@extends('adminpage.template.apps')

@section('title')
Tambah Jadwal Vaksinasi - Admin
@endsection

@section('custom_css')
<style>
    label{margin-left: 20px;}
    #datepicker{width:180px; margin: 0 20px 20px 20px;}
    #datepicker > span:hover{cursor: pointer;}
</style>
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin.jadwal_vaksinasi.index')}}">Jadwal Vaksinasi</a>
                                </li>
                                <li class="breadcrumb-item active"> Tambah Jadwal Vaksinasi
                                </li>
                            </ol>
                        </div>
                        <div class="col-12">
                            <h5>Jadwal Vaksinasi</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
               <section id="basic-tabs-components">
                    <div class="card col-lg-9">
                        <div class="card-header">
                            <div class="card-title">
                                <h4>Tambah Jadwal Vaksinasi</h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="tab-content">
                                    <form id="form_create" action="{{route('admin.jadwal_vaksinasi.store')}}" method="post" accept-charset="utf-8">
                                        {{ csrf_field() }}
                                        <div class="tab-pane active" id="id" aria-labelledby="id-tab" role="tabpanel">
                                            <div class="row">
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Lokasi</label>
                                                        <input type="text" class="form-control" id="lokasi" name="lokasi" placeholder="Masukan Lokasi" required>
                                                    </fieldset>
                                                </div>
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Tanggal Vaksin</label>
                                                        <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Masukan Tanggal Vaksin" required>
                                                    </fieldset>
                                                </div>
                                                <div id="kota_section" class="col-12" style="display:none">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Kota / Kabupaten</label>
                                                        <select class="form-control" id="kota_id" name="kota_id" placeholder="Masukan Kota" required>
                                                            <option value="">Pilih kota</option>
                                                            @foreach ($kota as $r)
                                                                <option value="{{$r->kode}}">{{$r->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Jumlah Sesi</label>
                                                        <input min="1" max="10" type="number" class="form-control" id="jumlah_sesi" name="jumlah_sesi" placeholder="Masukan Jawaban" value="1" required>
                                                    </fieldset>
                                                </div>
                                                <div id="time_section" class="col-12 row" >
                                                </div>
                                                <!-- Submit Button -->
                                                <div class="col-12">
                                                    <a id="submit" href="javascript:void(0)" class="btn btn-success round glow" style="float:right">
                                                        <span class="align-middle ml-25">Tambah</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<script>
    $('#submit').click( function() {
        $('#form_create').validate();
        $('#form_create').submit();
    });

    $(function () {
        $("#tanggal").datepicker({ 
            autoclose: true, 
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        }).datepicker('update', new Date());
    });

    @if (Auth::user()->role!=3)
        $("#kota_section").show();
        $("#kota_id").select2();
    @else
        $("#kota_section").hide();
    @endif

    $("#jumlah_sesi").on("keyup change", function(e) {
        $("#time_section").html(``);

        var jumlah_sesi = $(this).val();
        
        for (i = 1; i <= jumlah_sesi; i++){
            $("#time_section").append(`
                <div class="col-4">
                    <fieldset class="form-group">
                        <label for="basicInput">Periode waktu awal `+i+`</label>
                        <select class="form-control" id="timeAwal`+i+`" name="timeAwal[]" required>
                            <option value="00:00">00.00</option>
                            <option value="00:30">00.30</option>
                            <option value="01:00">01.00</option>
                            <option value="02:30">01.30</option>
                            <option value="02:00">02.00</option>
                            <option value="03:30">02.30</option>
                            <option value="03:00">03.00</option>
                            <option value="03:30">03.30</option>
                            <option value="04:00">04.00</option>
                            <option value="04:30">04.30</option>
                            <option value="05:00">05.00</option>
                            <option value="05:30">05.30</option>
                            <option value="06:00">06.00</option>
                            <option value="06:30">06.30</option>
                            <option value="07:00">07.00</option>
                            <option value="07:30">07.30</option>
                            <option value="08:00">08.00</option>
                            <option value="08:30">08.30</option>
                            <option value="09:00">09.00</option>
                            <option value="09:30">09.30</option>
                            <option value="10:00">10.00</option>
                            <option value="10:30">10.30</option>
                            <option value="11:00">11.00</option>
                            <option value="11:30">11.30</option>
                            <option value="12:00">12.00</option>
                            <option value="12:30">12.30</option>
                            <option value="13:00">13.00</option>
                            <option value="13:30">13.30</option>
                            <option value="14:00">14.00</option>
                            <option value="14:30">14.30</option>
                            <option value="15:00">15.00</option>
                            <option value="15:30">15.30</option>
                            <option value="16:00">16.00</option>
                            <option value="16:30">16.30</option>
                            <option value="17:00">17.00</option>
                            <option value="17:30">17.30</option>
                            <option value="18:00">18.00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                        </select>
                    </fieldset>
                </div>
                <div class="col-4">
                    <fieldset class="form-group">
                        <label for="basicInput">Periode waktu akhir `+i+`</label>
                        <select class="form-control" id="timeAkhir`+i+`" name="timeAkhir[]" required>
                            <option value="00:00">00.00</option>
                            <option value="00:30">00.30</option>
                            <option value="01:00">01.00</option>
                            <option value="02:30">01.30</option>
                            <option value="02:00">02.00</option>
                            <option value="03:30">02.30</option>
                            <option value="03:00">03.00</option>
                            <option value="03:30">03.30</option>
                            <option value="04:00">04.00</option>
                            <option value="04:30">04.30</option>
                            <option value="05:00">05.00</option>
                            <option value="05:30">05.30</option>
                            <option value="06:00">06.00</option>
                            <option value="06:30">06.30</option>
                            <option value="07:00">07.00</option>
                            <option value="07:30">07.30</option>
                            <option value="08:00">08.00</option>
                            <option value="08:30">08.30</option>
                            <option value="09:00">09.00</option>
                            <option value="09:30">09.30</option>
                            <option value="10:00">10.00</option>
                            <option value="10:30">10.30</option>
                            <option value="11:00">11.00</option>
                            <option value="11:30">11.30</option>
                            <option value="12:00">12.00</option>
                            <option value="12:30">12.30</option>
                            <option value="13:00">13.00</option>
                            <option value="13:30">13.30</option>
                            <option value="14:00">14.00</option>
                            <option value="14:30">14.30</option>
                            <option value="15:00">15.00</option>
                            <option value="15:30">15.30</option>
                            <option value="16:00">16.00</option>
                            <option value="16:30">16.30</option>
                            <option value="17:00">17.00</option>
                            <option value="17:30">17.30</option>
                            <option value="18:00">18.00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                        </select>
                    </fieldset>
                </div>
                <div class="col-4">
                    <fieldset class="form-group">
                        <label for="basicInput">Kuota periode `+i+`</label>
                        <input type="number" class="form-control" id="kuota`+i+`" name="kuota[]" placeholder="Masukan Kuota" required>
                    </fieldset>
                </div>`)
        }
    });
    $("#jumlah_sesi").change();
</script>
@endsection