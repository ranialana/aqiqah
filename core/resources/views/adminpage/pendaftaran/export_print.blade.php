<html>

<head>
	<title>Detail Pendaftaran</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
	<style type="text/css">
		table tr td,
		table tr th {
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Detail Pendaftaran</h4>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr style="text-align:center">
				<th>No</th>
				<th>Kode Pendaftaran</th>
				<th>Lokasi Vaksin</th>
				<th>Tanggal Vaksinasi</th>
				<th>Sesi Vaksinasi</th>
				<th>NIK</th>
				<th>Nama</th>
				<th>Tanggal Lahir</th>
				<th>Umur</th>
				<th>Jenis Kelamin</th>
				<!-- <th>Provinsi</th>
				<th>Kabupaten/Kota</th>
				<th>Kecamatan</th>
				<th>Kelurahan</th> -->
				<th>Status Profesi/Pekerjaan</th>
				<th>Nama Instansi Tempat Kerja</th>
				<th>Riwayat Penyakit Kronis</th>
				<th>Riwayat Terkonfirmasi COVID 19</th>
				<th>Alamat Tinggal</th>
				<th>Nomor Telepon (WhatsApp)</th>
				<th>Alamat Email</th>
			</tr>
		</thead>
		<tbody id="data_content" style="text-align:center">

			@foreach ($data as $key => $d)
			@php
			$gender = $d->jenis_kelamin;
			if($gender==0){
			$gender = "Laki - laki";
			} else if ($gender==1){
			$gender = "Perempuan";
			} else {
			$gender = "Other";
			}
			$cek = strftime("%Y-%m-%d", strtotime($d->penerima->tanggal_lahir));
			$umur = floor((time() - strtotime($cek)) / 31556926);
			@endphp
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$d->code}}</td>
				<td>{{$d->sesi->jadwal->lokasi}}</td>
				<td>{{strftime("%d-%m-%Y", strtotime($d->sesi->jadwal->tanggal_vaksin))}}</td>
				<td>{{$d->sesi->nama}} ({{$d->sesi->sesi}})</td>
				<td>{{$d->penerima->nik}}</td>
				<td>{{$d->penerima->nama}}</td>
				<td>{{$d->penerima->tanggal_lahir}}</td>
				<td>{{$umur}} Tahun</td>
				<td>{{$gender}}</td>
				<!-- <td>{{$d->penerima->ref_prov}}</td>
					<td>{{$d->penerima->ref_kabkot}}</td>
					<td>{{$d->penerima->ref_kec}}</td>
					<td>{{$d->penerima->ref_kel}}</td> -->
				<td>{{$d->penerima->status_pekerjaan}}</td>
				<td>{{$d->penerima->nama_instansi}}</td>
				<td>
					<?php
					$riwayat_penyakit = json_decode($d->penerima->riwayat_penyakit);
					if ($riwayat_penyakit) {
						echo "<ol type='1'>";
						foreach ($riwayat_penyakit as $index => $value) {
							if ($value == 'Lainnya') {
								echo "<li>Yang Lain : " . ucfirst($value) . "</li>";
							} else {
								echo "<li>" . ucfirst($value) . "</li>";
							}
						}
						echo "</ol>";
					} else {
						echo 'Tidak Ada';
					}
					?>
				</td>
				<td>
					<?php
					$riwayat_covid = json_decode($d->penerima->riwayat_covid);
					if ($riwayat_covid) {
						if ($riwayat_covid->status == 0) {
							echo "Tidak Pernah";
						} else {
							echo "Pernah pada " . $riwayat_covid->tanggal;
						}
					} else {
						echo 'Tidak Ada';
					}
					?>
				</td>
				<td>{{$d->penerima->alamat_tinggal}}</td>
				<td>{{$d->penerima->no_telp}}</td>
				<td>{{$d->penerima->email}}</td>
			</tr>

			@endforeach
		</tbody>
	</table>
	<script>
		window.print()
	</script>
</body>

</html>