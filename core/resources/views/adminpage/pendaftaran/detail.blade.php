@extends('adminpage.template.apps')

@section('title')
Detail Pendaftaran - Admin
@endsection

@section('custom_css')
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">Detail Pendaftaran
                                </li>
                            </ol>
                        </div>
                        <div class="col-12">
                            <h5>Detail Pendaftaran</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="basic-tabs-components">
                    <div class="card col-lg-12">
                        <div class="card-header">
                            <div class="card-title">
                                <h4>Detail Pendaftar {{$data->register->code}}</h4>
                                <div style="float:right">
                                    <!--
                                    @if ($data->register->is_verified==0)
                                        <button type="button" class="btn btn-icon rounded-circle btn-success glow mr-1 mb-1" title="Setujui" id="approve{{$data->register->id}}" onclick="confirm_approve('{{$data->register->id}}')"><i class="bx bx-check"></i></button>
                                        <button type="button" class="btn btn-icon rounded-circle btn-danger glow mr-1 mb-1" title="Tolak" id="cancel{{$data->register->id}}" onclick="cancel_approve('{{$data->register->id}}')"><i class="bx bx-x"></i></button>
                                    @elseif ($data->register->is_verified==-1)
                                        <button type="button" class="btn btn-icon rounded-circle btn-danger glow mr-1 mb-1" title="Tolak" id="cancel{{$data->register->id}}"><i class="bx bx-user-x"></i></button>
                                    @else
                                        <button type="button" class="btn btn-icon rounded-circle btn-success glow mr-1 mb-1" title="Setujui" id="approve{{$data->register->id}}"><i class="bx bx-user-check"></i></button>
                                    @endif
                                    -->
                                </div>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div id="cancel_message">
                                </div>

                                <h5>Data Pendaftar</h5>
                                <div class="table-responsive"> 
                                    <table class="table table-striped">
                                        <tr>
                                            <td width="20%">Nama</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->nama}}</td>
                                        </tr>
                                        <tr>
                                            <td width="20%">NIK</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->nik}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td width="20%">Tanggal Lahir</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->tanggal_lahir}}</td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Tanggal Pendaftaran</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->created_at}}</td>
                                        </tr>
                                        <tr>
                                            <td width="20%">No Telp</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->no_telp}}</td>
                                        </tr>

                                        @if (json_decode($data->domisili)->provinsi != '' || json_decode($data->domisili)->kabupaten != '')
                                        <tr>
                                            <td width="20%">Provinsi</td>
                                            <td width="5%"> : </td>
                                            <td>
                                                <?php
                                                $provinsi = json_decode($data->domisili)->provinsi;
                                                    echo ucfirst($provinsi);
                                                ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="20%">Kota/Kabupaten</td>
                                            <td width="5%"> : </td>
                                            <td>
                                                <?php
                                                $kabupaten = json_decode($data->domisili)->kabupaten;
                                                //print_r(json_decode($data->domisili)->kabupaten);die();
                                                echo ucfirst($kabupaten);
                                                ?>
                                            </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td width="20%">Alamat</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->alamat_tinggal}}</td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Email</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->email}}</td>
                                        </tr>
                                        @if (isset($data->keterangan ) && $data->keterangan != ' ' && $data->keterangan != null)
                                        <tr>
                                            <td width="20%">Keterangan</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->keterangan}} </td>
                                        </tr>
                                        @endif
                                        @if (isset($data->keterangan_tambahan) && $data->keterangan_tambahan != ' ' && $data->keterangan_tambahan != null)
                                        <tr>
                                            <td width="20%">Keterangan Tambahan</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->keterangan_tambahan}}</td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td width="20%">Jenis Kelamin</td>
                                            <td width="5%"> : </td>
                                            @php
                                                $gender = $data->jenis_kelamin;
                                                if ($gender==1){
                                                    $gender = "Perempuan";
                                                } else if ($gender==0){
                                                    $gender = "Laki - laki";
                                                } else if ($gender==2){
                                                    $gender = "Dirahasiakan";
                                                }
                                            @endphp
                                            <td>{{$gender}}</td>
                                        </tr>

                                        <tr>
                                            <td width="20%">Kategori</td>
                                            <td width="5%"> : </td>
                                            @php
                                            $gender = $data->jenis_kelamin;
                                            $cek = strftime("%Y-%m-%d", strtotime($data->tanggal_lahir));
			                                $umur = floor((time() - strtotime($cek)) / 31556926);
                                                $is_pregnant = $data->is_pregnant;
                                                if ($is_pregnant==1 && $gender==1){
                                                    $is_pregnant = "Ibu hamil/menyusui";
                                                } else if ($is_pregnant==0 && $umur >= 18){
                                                    $is_pregnant = "Dewasa";
                                                } else if ($is_pregnant==0 && $umur < 18){
                                                    $is_pregnant = "Anak-anak";
                                                }
                                            @endphp
                                            <td>{{$is_pregnant}}</td>
                                        </tr>

                                        <tr>
                                            <td width="20%">Pekerjaan</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->status_pekerjaan}}</td>
                                        </tr>
                                        @if ($data->status_profesi != '' ||$data->nama_instansi != '' )
                                        <tr>
                                            <td width="20%">Nama Instansi Tempat Kerja</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->status_profesi}}</td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><?php if ($nama_instansi = $data->status_profesi == 'TNI' || $data->status_profesi == 'POLRI'){
                                                $nama_instansi = 'Kesatuan'; 
                                            } else if($nama_instansi = $data->status_profesi == 'PNS' || $data->status_profesi == 'Honorer'){
                                                $nama_instansi = 'Nama Instansi';
                                            } else if($nama_instansi = $data->status_profesi == 'Swasta'){
                                                $nama_instansi = 'Nama Tempat Bekerja';
                                            } 
                                            echo $nama_instansi;
                                            ?></td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->nama_instansi}}</td>
                                        </tr>
                                        @endif

                                        @if (isset($data->dosis ) && $data->dosis != ' ' && $data->dosis != null)
                                        <tr>
                                            <td width="20%">Dosis</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->dosis}} </td>
                                        </tr>
                                        @endif

                                        @if (isset($data->tgl_dosis1 ) && $data->tgl_dosis1 != ' ' && $data->tgl_dosis1 != null)
                                        <tr>
                                            <td width="20%">Tanggal Dosis 1</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->tgl_dosis1}}</td>
                                        </tr>
                                        @endif

                                        @if (isset($data->jenis_vaksin ) && $data->jenis_vaksin != ' ' && $data->jenis_vaksin != null)
                                        <tr>
                                            <td width="20%">Jenis Vaksin</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->jenis_vaksin}}</td>
                                        </tr>
                                        @endif

                                        @if (isset($data->tgl_dosis2 ) && $data->tgl_dosis2 != ' ' && $data->tgl_dosis2 != null)
                                        <tr>
                                            <td width="20%">Tanggal Akan Diberikan Dosis 2</td>
                                            <td width="5%"> : </td>
                                            <td>{{$data->tgl_dosis2}}</td>
                                        </tr>
                                        @endif

                                        <tr>
                                            <td width="20%">Riwayat Penyakit Kronis</td>
                                            <td width="5%"> : </td>
                                            <td>
                                                <?php
                                                $riwayat_penyakit = json_decode($data->riwayat_penyakit);
                                                if ($riwayat_penyakit) {
                                               
                                                    foreach ($riwayat_penyakit as $index => $value) {
                                                        if ($value == 'Lainnya') {
                                                            echo "<li>Yang Lain : " . ucfirst($value) . "</li>";
                                                        }else if ($value == 'tidak ada') {
                                                            echo ucfirst($value);
                                                        } else {
                                                            echo "<li>" . ucfirst($value) . "</li>";
                                                        }
                                                    }
                                                } else {
                                                    echo 'Tidak Ada';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Riwayat Terkonfirmasi COVID 19</td>
                                            <td width="5%"> : </td>
                                            <td>
                                            <?php
                                                $riwayat_covid = json_decode($data->riwayat_covid);
                                                if($riwayat_covid) {
                                                    if($riwayat_covid->status == 0) {
                                                        echo "Tidak Pernah";
                                                    }
                                                    else {
                                                        echo "Pernah pada ".$riwayat_covid->tanggal;
                                                    }
                                                }
                                                else {
                                                    echo 'Tidak Ada';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Foto KTP/KK</td>
                                            <td width="5%"> : </td>
                                            <td><a href="{{asset('KK_KTP')}}/{{$data->file}}" target="_blank"><img src="{{asset('KK_KTP')}}/{{$data->file}}" class="d-block mx-auto mb-2" alt="KTP/KK" height="150"><span class="text-center" style="color:red;font-size:12px">*Klik foto untuk membuka foto di halaman baru</span></a></td>
                                        </tr>
                                    </table>
                                </div>
                                <hr>
                                <h5>Data Lokasi</h5>
                                <div class="table-responsive"> 
                                    <table class="table table-striped">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Lokasi</th>
                                                <th>Tanggal Vaksin</th>
                                                <!-- <th>Sesi</th>   -->
                                                <th>Check In</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">
                                            <tr>
                                                <td>{{$data["register"]["sesi"]["jadwal"]["lokasi"]}}</td>
                                                <td>{{$data["register"]["sesi"]["jadwal"]["tanggal_vaksin"]}}</td>
                                                <!-- <td>{{$data["register"]["sesi"]["sesi"]}}</td> -->
                                                <td id="check_in{{$data->id}}" >
                                                    @if($data["register"]["is_verified"] != 1)
                                                        <button type="button" class="btn btn-icon btn-warning glow mr-1 mb-1" title="Check In" onClick="check_in('{{$data["register"]["id"]}}')">Check In</button></br><span class="text-center" style="color:red;font-size:12px">Klik disini untuk check in</span>
                                                    @else
                                                        <span style="color:#00ff00">Success</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <hr>
                                <h5>QR Code</h5>
                                <div id="qrcode"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{asset('admin_template/custom-js/qrcode.min.js')}}" ></script>
<script src="{{asset('admin_template/custom-js/html2canvas.js')}}" ></script>
<script src="{{asset('admin_template/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('admin_template/app-assets/vendors/js/extensions/polyfill.min.js')}}"></script>
<script>
new QRCode(document.getElementById("qrcode"), "{{$data->register->code}}");

@if(Auth::user()->role =='1') 
function check_in(id){
    var id_btn = id;
    Swal.fire({
        title: 'CHECK IN?',
        text: "Check In Pendaftar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Check In',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-outline-primary ml-1',
        buttonsStyling: false,
        }).then(function (result) {
        if (result.value) {
            $.ajax({
                type:"POST",
                url : "{{route('admin.pendaftaran.check_in')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: { 
                    "id": id,
                },
                success:function(data){
                    if (data==1) {
                        $("#check_in"+id_btn).html(`<span style="color:#00ff00">Success</span>`);
                    location.reload();
                    }
                }
            });
            
        }
    });
}
@endif

function confirm_approve(id){
    var id_btn = id;
    Swal.fire({
        title: 'Approve Pendaftaran?',
        text: "Data Tidak Dapat Dikembalikan",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Approve',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-outline-primary ml-1',
        buttonsStyling: false,
        }).then(function (result) {
        if (result.value) {
            $(".cover-spin").fadeIn(250);
            $.ajax({
                type:"POST",
                url : "{{route('admin.pendaftaran.approve')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: { 
                    "id": id,
                },
                success:function(id){
                    $(".cover-spin").fadeOut(250);
                    $("#approve"+id_btn).removeClass('btn-warning');
                    $("#approve"+id_btn).addClass('btn-success');
                    $("#approve"+id_btn+" > i").removeClass('bx-check');
                    $("#approve"+id_btn+" > i").addClass('bx-user-check');
                    $("#approve"+id_btn).removeAttr('onclick');
                    $("#cancel"+id_btn).remove();
                    Swal.fire(
                        {
                            type: "success",
                            title: 'Approved!',
                            text: 'Data Registrasi Telah Diterima.',
                            confirmButtonClass: 'btn btn-success',
                        }
                    )
                    
                }
            });
            
        }
    });
}

function cancel_approve(id){
    var id_btn = id;
    Swal.fire({
        title: 'Reject Pendaftaran?',
        input: 'text',
        inputPlaceholder: 'Masukan alasan reject',
        text: "Data Tidak Dapat Dikembalikan",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#D3514D',
        cancelButtonColor: '#d12900',
        confirmButtonText: 'OK',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-outline-danger ml-1',
        buttonsStyling: false,
        }).then(function (result) {
        if (result.value) {
            $(".cover-spin").fadeIn(250);
            $.ajax({
                type:"POST",
                url : "{{route('admin.pendaftaran.cancel')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: { 
                    "id": id,
                    "keterangan":result.value
                },
                // data: id,
                success:function(id){
                    $(".cover-spin").fadeOut(250);

                    $("#cancel"+id_btn).removeClass('btn-warning');
                    $("#cancel"+id_btn).addClass('btn-danger');
                    $("#cancel"+id_btn+" > i").removeClass('bx-x');
                    $("#cancel"+id_btn+" > i").addClass('bx-user-x');
                    $("#cancel"+id_btn).removeAttr('onclick');
                    $("#approve"+id_btn).remove();
                    Swal.fire(
                        {
                            type: "success",
                            title: 'Canceled!',
                            text: 'Data Registrasi Telah Ditolak.',
                            confirmButtonClass: 'btn btn-danger',
                        }
                    )
                    
                }
            });
            
        }
    });
}
</script>
@endsection