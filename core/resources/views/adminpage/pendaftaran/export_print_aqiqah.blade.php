<html>

<head>
	<title>Detail Pendaftaran</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
	<style type="text/css">
		table tr td,
		table tr th {
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Data Pendaftaran</h4>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr style="text-align:center">
				<th>No</th>
				<th>Kode Pendaftaran</th>
				<th>Nama Pendaftar</th>
				<th>Jumlah Anggota Keluarga</th>
				<th>Total Hadir</th>
				<th>Anggota</th>
			</tr>
		</thead>
		<tbody id="data_content" style="text-align:center">

			@php 
				$total_anggota_keluarga = 0; 
				$jumlah_anggota         = 0;
			@endphp
			@foreach ($data as $key => $d)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$d->nik}}</td>
				<td>{{$d->nama}}</td>
				<td>{{$d->jumlah_anggota}}</td>
				<td>{{$d->jumlah_anggota + 1}}</td>
				<td>
                    @php $ang_keluarga   = json_decode($d->anggota_keluarga); @endphp
					@php $jumlah_anggota          = $d->jumlah_anggota+1; @endphp
					@php $total_anggota_keluarga += $jumlah_anggota; @endphp

                    @if($ang_keluarga != NULL)
                    @if(count($ang_keluarga) > 0)
                        @foreach($ang_keluarga as $key => $row)
                            @php $keys = $key + 1; @endphp
                            Anggota keluarga Ke- {{$keys}} : {{$row}} <br>       
                        @endforeach
                    @endif
                    @endif
                </td>
			</tr>
			@endforeach
			<tr>
				<td colspan="4"><strong>Total</strong></td>
				<td><strong>{{$total_anggota_keluarga}}</strong></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<script>
		window.print()
	</script>
</body>

</html>