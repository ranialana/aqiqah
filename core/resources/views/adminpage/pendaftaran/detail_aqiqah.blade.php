@extends('adminpage.template.apps')

@section('title')
Detail Pendaftaran - Admin
@endsection

@section('custom_css')
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">Detail Pendaftaran
                                </li>
                            </ol>
                        </div>
                        <div class="col-12">
                            <h5>Detail Pendaftaran</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="basic-tabs-components">
                    <div class="card col-lg-12">
                        <div class="card-header">
                            <div class="card-title">
                                <h4>Detail Pendaftar {{$data->nik}}</h4>
                                <div style="float:right">
                                </div>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div id="cancel_message">
                                </div>
                                <h5>Data Pendaftar</h5>
                                <div class="table-responsive"> 
                                    <table class="table table-striped">
                                        <tr>
                                            <td width="20%">Nama</td>
                                            <td width="5%"> : </td>
                                            <td>@if($data) {{$data->nama}} @endif</td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Kode Pendaftaran</td>
                                            <td width="5%"> : </td>
                                            <td>@if($data) {{$data->nik}} @endif</td>
                                        </tr>
                                        @php 
                                        if($data){
                                            $anggota = "";
                                            $anggota = json_decode($data->anggota_keluarga);
                                        } @endphp

                                        @if($data)
                                        @if($data->jumlah_anggota > 0)
                                        @if(count($anggota) > 0)
                                            @foreach($anggota as $key => $value)
                                                @php $val = $key + 1; @endphp
                                            <tr>
                                                <td width="20%">Nama Anggota Keluarga Ke- {{$val}}</td>
                                                <td width="5%"> : </td>
                                                <td>{{$value}}</td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        @endif
                                        @endif
                                        <tr>
                                            <td width="20%">Jumlah Anggota Keluarga</td>
                                            <td width="5%"> : </td>
                                            <td>@if($data) {{$data->jumlah_anggota}} @endif</td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Total Hadir</td>
                                            <td width="5%"> : </td>
                                            <td>@if($data) {{$data->jumlah_anggota + 1}} @endif</td>
                                        </tr>
                                    </table>
                                </div>
                                <hr>
                                <!--
                                <h5>QR Code</h5>
                                <div id="qrcode2"></div>
                                -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{asset('admin_template/custom-js/qrcode.min.js')}}" ></script>
<script src="{{asset('admin_template/custom-js/html2canvas.js')}}" ></script>
<script src="{{asset('admin_template/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('admin_template/app-assets/vendors/js/extensions/polyfill.min.js')}}"></script>
<script>
new QRCode(document.getElementById("qrcode"), "");

@if(Auth::user()->role =='1') 
function check_in(id){
    var id_btn = id;
    Swal.fire({
        title: 'CHECK IN?',
        text: "Check In Pendaftar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Check In',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-outline-primary ml-1',
        buttonsStyling: false,
        }).then(function (result) {
        if (result.value) {
            $.ajax({
                type:"POST",
                url : "{{route('admin.pendaftaran.check_in')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: { 
                    "id": id,
                },
                success:function(data){
                    if (data==1) {
                        $("#check_in"+id_btn).html(`<span style="color:#00ff00">Success</span>`);
                    location.reload();
                    }
                }
            });
            
        }
    });
}
@endif

function confirm_approve(id){
    var id_btn = id;
    Swal.fire({
        title: 'Approve Pendaftaran?',
        text: "Data Tidak Dapat Dikembalikan",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Approve',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-outline-primary ml-1',
        buttonsStyling: false,
        }).then(function (result) {
        if (result.value) {
            $(".cover-spin").fadeIn(250);
            $.ajax({
                type:"POST",
                url : "{{route('admin.pendaftaran.approve')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: { 
                    "id": id,
                },
                success:function(id){
                    $(".cover-spin").fadeOut(250);
                    $("#approve"+id_btn).removeClass('btn-warning');
                    $("#approve"+id_btn).addClass('btn-success');
                    $("#approve"+id_btn+" > i").removeClass('bx-check');
                    $("#approve"+id_btn+" > i").addClass('bx-user-check');
                    $("#approve"+id_btn).removeAttr('onclick');
                    $("#cancel"+id_btn).remove();
                    Swal.fire(
                        {
                            type: "success",
                            title: 'Approved!',
                            text: 'Data Registrasi Telah Diterima.',
                            confirmButtonClass: 'btn btn-success',
                        }
                    )
                    
                }
            });
            
        }
    });
}

function cancel_approve(id){
    var id_btn = id;
    Swal.fire({
        title: 'Reject Pendaftaran?',
        input: 'text',
        inputPlaceholder: 'Masukan alasan reject',
        text: "Data Tidak Dapat Dikembalikan",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#D3514D',
        cancelButtonColor: '#d12900',
        confirmButtonText: 'OK',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-outline-danger ml-1',
        buttonsStyling: false,
        }).then(function (result) {
        if (result.value) {
            $(".cover-spin").fadeIn(250);
            $.ajax({
                type:"POST",
                url : "{{route('admin.pendaftaran.cancel')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: { 
                    "id": id,
                    "keterangan":result.value
                },
                // data: id,
                success:function(id){
                    $(".cover-spin").fadeOut(250);

                    $("#cancel"+id_btn).removeClass('btn-warning');
                    $("#cancel"+id_btn).addClass('btn-danger');
                    $("#cancel"+id_btn+" > i").removeClass('bx-x');
                    $("#cancel"+id_btn+" > i").addClass('bx-user-x');
                    $("#cancel"+id_btn).removeAttr('onclick');
                    $("#approve"+id_btn).remove();
                    Swal.fire(
                        {
                            type: "success",
                            title: 'Canceled!',
                            text: 'Data Registrasi Telah Ditolak.',
                            confirmButtonClass: 'btn btn-danger',
                        }
                    )
                    
                }
            });
            
        }
    });
}
</script>
@endsection