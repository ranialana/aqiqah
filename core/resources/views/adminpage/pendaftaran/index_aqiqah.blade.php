@extends('adminpage.template.apps')

@section('title')
Data Pendaftaran - Admin
@endsection

@section('custom_css')


    <style>
        .rounded-circle { 
            margin-bottom: 0rem !important;
            margin-right: 0rem !important;
        }
    </style>
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="row" style="margin-bottom: -20px;">
                                            <div class="col-5">
                                                <fieldset class="form-group">
                                                    <div class="input-group">
							                        <p>Data Pendaftar Aqiqah</p>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-12" style="margin-top:10px">
                                                <a id="link_print" target="_blank" href="{{route('admin.pendaftaran.export_print')}}" class="btn btn-primary round glow" style="float:right;margin-left:20px;margin-bottom:10px">
                                                    <i class="bx bx-printer"></i>
                                                    <span class="align-middle ml-25">Cetak</span>
                                                </a>
                                                <a id="link_export" href="{{route('admin.pendaftaran.export_excel')}}" target="_blank" class="btn btn-success round glow" style="float:right;margin-left:20px;margin-bottom:10px">
                                                    <i class="bx bx-upload"></i>
                                                    <span class="align-middle ml-25">Export</span>
                                                </a>
                                                <div style="float:right">
                                                    <a id="cancel-qr" href="javascript:void(0)" class="btn btn-danger round glow" style="display:none;float:right">
                                                        <i class="bx bx-x"></i>
                                                        <span class="align-middle ml-25">Cancel</span>
                                                    </a>
                                                </div>
                                            </div>
                                        <div class="table-responsive">
                                            <table id="booking_list" class="table table-bordered table-hover zero-configuration">
                                                <thead style="text-align:center">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode Pendaftar</th>
                                                        <th>Nama Pendaftar</th>
                                                        <th>Jumlah Anggota Keluarga</th>
                                                        <th>Total Hadir</th>
                                                        <td>Aksi</td>
                                                    </tr>
                                                </thead>
                                                <tbody id="data_content" style="text-align:center">
                                                    @foreach ($aqiqah as $key => $d)
                                                        <tr>
                                                            <td>{{$key+1}}</td>
                                                            <td>{{$d->nik}}</td>
                                                            <td>{{$d->nama}}</td>
                                                            <td>{{$d->jumlah_anggota}}</td>
                                                            <td>{{$d->jumlah_anggota + 1}}</td>
                                                            <td width="10%">
                                                                <a href="{{route('admin.pendaftaran.detail',['id' => (isset($d->id) ? $d->id : 'null' )])}}" class="btn btn-icon rounded-circle btn-primary glow mr-1 mb-1" title="Detail"><i class="bx bx-show"></i></a>
                                                                <a href="javascript:void(0);" data-nama = "{{(isset($d->nama) ? $d->nama : 'NULL' )}}" data-nik="{{(isset($d->nik) ? $d->nik : 'NULL' )}}" data-url ="{{route('admin.pendaftaran.delete',['id' => (isset($d->id) ? $d->id : 'null' )])}}" class="btn btn-icon rounded-circle btn-danger glow mr-1 mb-1" title="Hapus"><i class="bx bx-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
                                                        
        $('#booking_list').DataTable(
        {
            "oLanguage": {
                "sLengthMenu": "Tampilkan _MENU_ Entri",
                "sSearch": "Cari:",
                "sInfo": "Tampilkan _START_ Sampai _END_ dari _TOTAL_ Entri",
                "sInfoEmpty": "Tampilkan 0 Sampai 0 dari 0 Entri",
                "sZeroRecords": "Tidak ada data yang ditemukan",
                "oPaginate": {
                    "sFirst":    "Awal",
                    "sLast":     "Akhir",
                    "sNext":     "Selanjutkan",
                    "sPrevious": "Sebelumnya"
                },
            },
        });

        $('#booking_list tbody').on('click', '.btn-danger', function(){
            var url_hapus = $(this).attr('data-url');
            Swal.fire({
                    title: 'Hapus Data Pendaftaran',
                    text: "Apakah Anda yakin akan menghapus data atas nama "+$(this).attr('data-nama')+" dengan nomor NIK "+$(this).attr('data-nik')+" ?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#a5a5a5',
                    confirmButtonText: 'Hapus',
                    }).then(function (result) {
                    if (result.value) {
                        $(".cover-spin").fadeIn(250);
                        window.location = url_hapus;   
                    }
                });
        });

        
        $("#search_button").click(function() {
            var pekerjaan = $('#location_filter').val();
            var SesiFilter = $('#sesi_filter').val();

            window.location = "{{url('admin-page/data-pendaftaran')}}"+"?pekerjaan="+pekerjaan+"&sesi="+SesiFilter;
        });

        let scanner=""
        $("#scan-qr").click(function() {
            if ($("#cancel-qr").css("display")=="none"){
                $("#scan-qr").html(`<video id="preview" style="width:200px"></video>`)
                $("#cancel-qr").show()
                scanner = new Instascan.Scanner({ video: document.getElementById('preview'),mirror: false });
                scanner.addListener('scan', function (booking_id) {
                    //success lisener
                    $("#cancel-qr").click()

                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.pendaftaran.qr')}}",
                        dataType: "html",
                        data: {
                            '_token' : '{{csrf_token()}}',
                            'code'     : booking_id,
                        },
                        success : function(data){
                            if (data!=0){
                                window.location="{{route('admin.pendaftaran.detail-qr')}}?id="+data
                            } else {
                                Swal.fire(
                                    'Data tidak ditemukan',
                                    'QR Code yang anda scan salah',
                                    'error'
                                )
                            }
                        }
                    });


                });
                Instascan.Camera.getCameras().then(function (cameras) {
                    if (cameras.length > 0) {
                        if(typeof cameras[1] === 'undefined') {
                            var selectedCam = cameras[0];
                        } else {
                            var selectedCam = cameras[1];
                        }

                        scanner.start(selectedCam);
                    } else {
                        console.error('No cameras found.');
                    }
                }).catch(function (e) {
                    console.error(e);
                });
            }
        });

        $("#cancel-qr").click(function() {
            $("#scan-qr").html(`<a href="javascript:void(0)" class="btn btn-primary round glow">
                                    <i class="bx bx-printer"></i>
                                    <span class="align-middle ml-25">Pindai QR</span>
                                </a>`);
            $(this).hide()
            scanner.stop()
        });
        


        //fill link export
        if (getUrlParameter('location') == null){
            $("#link_export").attr("href",$("#link_export").attr("href"))
            $("#link_print").attr("href",$("#link_print").attr("href"))
        } 
        else{
            $("#link_export").attr("href",$("#link_export").attr("href")+"?location="+getUrlParameter('location')+"&sesi="+getUrlParameter('sesi'))
            $("#link_print").attr("href",$("#link_print").attr("href")+"?location="+getUrlParameter('location')+"&sesi="+getUrlParameter('sesi'))
        }

        //function get sesi jadwal vaksinasi
        $('#location_filter').on('change', function(){
            var idSesi = $('#location_filter').val();
            var t_url = "{{url('admin-page/data-pendaftaran/sesi-pendaftaran/')}}"+"/"+idSesi;
            var rows = '<option value="null">Pilih Sesi Vaksinasi</option>';
            $.ajax({
                type: "GET",
                url: t_url,
                dataType: "json",
                success : function(data){
                    $('#sesi_filter').prop('disabled', false);
                    $.each(data, function(i, item) {
                        rows += '<option value="'+item.id+'">'+item.nama+' ('+item.sesi+') </option>';
                    });
                   
                    $('#sesi_filter').html(rows);
                    
                }
            });
           
        });

        @if(Auth::user()->role =='1')
            function confirm_approve(id){
                var id_btn = id;
                Swal.fire({
                    title: 'Setujui Pendaftaran?',
                    text: "Data Tidak Dapat Dikembalikan",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Approve',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-outline-primary ml-1',
                    buttonsStyling: false,
                    }).then(function (result) {
                    if (result.value) {
                        $(".cover-spin").fadeIn(250);
                        $.ajax({
                            type:"POST",
                            url : "{{route('admin.pendaftaran.approve')}}",
                            headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                            },
                            data: { 
                                "id": id,
                            },
                            // data: id,
                            success:function(id){
                                $(".cover-spin").fadeOut(250);
                                $("#approve"+id_btn).removeClass('btn-warning');
                                $("#approve"+id_btn).addClass('btn-success');
                                $("#approve"+id_btn+" > i").removeClass('bx-check');
                                $("#approve"+id_btn+" > i").addClass('bx-user-check');
                                $("#approve"+id_btn).removeAttr('onclick');
                                $("#cancel"+id_btn).remove();
                                Swal.fire(
                                    {
                                        type: "success",
                                        title: 'Disetujui!',
                                        text: 'Data Registrasi Telah Diterima.',
                                        confirmButtonClass: 'btn btn-success',
                                    }
                                )
                                
                            }
                        });
                        
                    }
                });
            }

            function cancel_approve(id){
                var id_btn = id;
                Swal.fire({
                    title: 'Reject Pendaftaran?',
                    input: 'text',
                    inputPlaceholder: 'Masukan alasan reject',
                    text: "Data Tidak Dapat Dikembalikan",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#D3514D',
                    cancelButtonColor: '#d12900',
                    confirmButtonText: 'OK',
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-outline-danger ml-1',
                    buttonsStyling: false,
                    }).then(function (result) {
                    if (result.value) {
                        $(".cover-spin").fadeIn(250);
                        $.ajax({
                            type:"POST",
                            url : "{{route('admin.pendaftaran.cancel')}}",
                            headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                            },
                            data: { 
                                "id": id,
                                "keterangan":result.value
                            },
                            // data: id,
                            success:function(id){
                                $(".cover-spin").fadeOut(250);
                                $("#cancel"+id_btn).removeClass('btn-warning');
                                $("#cancel"+id_btn).addClass('btn-danger');
                                $("#cancel"+id_btn+" > i").removeClass('bx-x');
                                $("#cancel"+id_btn+" > i").addClass('bx-user-x');
                                $("#cancel"+id_btn).removeAttr('onclick');
                                $("#approve"+id_btn).remove();
                                Swal.fire(
                                    {
                                        type: "success",
                                        title: 'Ditolak!',
                                        text: 'Data Registrasi Telah Ditolak.',
                                        confirmButtonClass: 'btn btn-danger',
                                    }
                                )
                                
                            }
                        });
                        
                    }
                });
            }
        @endif
        
    </script>
@endsection