@extends('adminpage.template.apps')

@section('title')
Setting Form - Admin
@endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/tagsinput.css')}}">
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active"> Setting Form Pendaftaran
                                </li>
                            </ol>
                        </div>
                        <div class="col-12">
                            <h5>Setting Form Pendaftaran</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
               <section id="basic-tabs-components">
                    <div class="card col-lg-9">
                        <div class="card-header">
                            <div class="card-title">
                                <h4>Setting Form Pendaftaran</h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="tab-content">
                                    <form id="form_create" action="{{route('admin.setting_form.store')}}" method="post" accept-charset="utf-8">
                                        {{ csrf_field() }}
                                        <div class="tab-pane active" id="id" aria-labelledby="id-tab" role="tabpanel">
                                            <div class="row">
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Batasan Umur Tanggal Lahir</label>
                                                        <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Dalam Satuan Tahun (Contoh : 18)" value="{{$data->batasan_umur}}" required>
                                                        <div class="form-group form-check">
                                                            <input type="checkbox" class="form-check-input" name="is_batasan_umur" id="is_batasan_umur" @if ($data->is_batasan_umur == 1) {{'checked'}} @endif>
                                                            <label class="form-check-label" for="is_batasan_umur">Active</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Lock Kabupaten/Kota</label>
                                                        <select class="form-control" id="kota" name="kota" required>
                                                            @foreach ($kota as $k)
                                                                @if ($data->kota_id==$k->kode)
                                                                    <option value="{{$k->kode}}" selected>{{$k->nama}}</option>
                                                                @else 
                                                                    <option value="{{$k->kode}}">{{$k->nama}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <div class="form-group form-check">
                                                            <input type="checkbox" class="form-check-input" name="is_kota" id="is_kota" @if ($data->is_kota == 1) {{'checked'}} @endif>
                                                            <label class="form-check-label" for="is_kota">Active</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Lock NIK</label>
                                                        <input id="nik" name="nik" type="text" data-role="tagsinput" class="form-control" value="{{$data->nik}}" required>
                                                        <div class="form-group form-check">
                                                            <input type="checkbox" class="form-check-input" name="is_nik" id="is_nik" @if ($data->is_nik == 1) {{'checked'}} @endif>
                                                            <label class="form-check-label" for="is_nik">Active</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                
                                                <!-- Submit Button -->
                                                <div class="col-12">
                                                    <a id="submit" href="javascript:void(0)" class="btn btn-success round glow" style="float:right">
                                                        <span class="align-middle ml-25">Tambah</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{ asset('assets/js/tagsinput.js')}}"></script>
<script>
    @if(Session::get('status'))
        @if(Session::get('status') ==  1)         
            $("#toast-success-content").html("{{Session::get('status_message')}}");
            $("#toast-success").toast('show');
        @elseif (Session::get('status') ==  -1)  
            $("#toast-error-content").html("{{Session::get('status_message')}}");
            $("#toast-error").toast('show');   
        @endif
    @endif
</script>

<script>
    $('#submit').click( function() {
        $('#form_create').validate();
        $('#form_create').submit();
    });
    $("#kota").select2();
</script>
@endsection