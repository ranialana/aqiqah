@extends('adminpage.template.apps')

@section('title')
Scanner Pendaftaran - Admin
@endsection

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/sweetalert2.min.css')}}">

    <style>
        .rounded-circle { 
            margin-bottom: 0rem !important;
            margin-right: 0rem !important;
        }
    </style>
@endsection

@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
        <div class="content-wrapper">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center" style="margin-top:50px">
                            <div id="scan-qr">
                                <a href="javascript:void(0)" class="btn btn-primary glow" style="font-size:40px;padding:30px;border-radius:200px">
                                    <i class="bx bx-camera" style="font-size:32px;"></i>
                                    <span class="align-middle ml-25" >Pindai QR</span>
                                </a>
                            </div>
                            <a id="cancel-qr" href="javascript:void(0)" class="btn btn-danger round glow" style="display:none;">
                                <i class="bx bx-x"></i>
                                <span class="align-middle ml-25">Batalkan</span>
                            </a>
                        </div>
                        <div class="text-center row" style="margin-top:50px;">
                            <div class="col-2"></div>
                            <fieldset class="form-group col-8">
                                <label for="basicInput">atau Masukan Kode Pendaftaran / NIK</label>
                                <input type="text" class="form-control" id="kode" name="kode" placeholder="Masukan kode registrasi" style="text-align:center;">
                                <a id="submit_kode" href="javascript:void(0)" class="btn btn-success round glow" style="margin-top:20px">
                                    <span class="align-middle ml-25">Submit</span>
                                </a>
                            </fieldset>
                            <div class="col-2"></div>
                        </div>
                        <script>
                            $.ajax({
                                type: "POST",
                                url: "{{route('admin.pendaftaran.qr')}}",
                                dataType: "html",
                                data: {
                                    '_token' : '{{csrf_token()}}',
                                    'booking_id'     : booking_id,
                                },
                                success : function(data){
                                    if (data!=0){
                                        window.location="{{route('admin.pendaftaran.detail-qr',['id' => ""])}}/"+data
                                    } else {
                                        Swal.fire(
                                            'Data tidak ditemukan',
                                            'QR Code yang anda scan salah',
                                            'error'
                                        )
                                    }
                                }
                            });
                        </script>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('script')
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
                                                        
        $('#booking_list').DataTable(
        {
            "oLanguage": {
                "sLengthMenu": "Tampilkan _MENU_ Entri",
                "sSearch": "Cari:",
                "sInfo": "Tampilkan _START_ Sampai _END_ dari _TOTAL_ Entri",
                "sInfoEmpty": "Tampilkan 0 Sampai 0 dari 0 Entri",
                "sZeroRecords": "Tidak ada data yang ditemukan",
                "oPaginate": {
                    "sFirst":    "Awal",
                    "sLast":     "Akhir",
                    "sNext":     "Selanjutkan",
                    "sPrevious": "Sebelumnya"
                },
            },
            "order": [[ 3, "desc" ]]
        });

        
        $("#search_button").click(function() {
            date = $('#date_filter').val();

            window.location = "{{route('admin.pendaftaran.index')}}"+"?date="+date;
        });

        let scanner=""
        $("#scan-qr").click(function() {
            if ($("#cancel-qr").css("display")=="none"){
                $("#scan-qr").html(`<video id="preview" style="width:200px"></video>`)
                $("#cancel-qr").show()
                scanner = new Instascan.Scanner({ video: document.getElementById('preview'),mirror: false });
                scanner.addListener('scan', function (booking_id) {
                    //success lisener
                    $("#cancel-qr").click()
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.pendaftaran.qr')}}",
                        dataType: "html",
                        data: {
                            '_token' : '{{csrf_token()}}',
                            'code'     : booking_id,
                        },
                        success : function(data){
                            if (data!=0){
                                window.location="{{route('admin.pendaftaran.detail-qr-aqiqah')}}?id="+data
                            } else {
                                Swal.fire(
                                    'Data tidak ditemukan',
                                    'QR Code yang anda scan salah',
                                    'error'
                                )
                            }
                        }
                    });


                });
                Instascan.Camera.getCameras().then(function (cameras) {
                    if (cameras.length > 0) {
                        if(typeof cameras[1] === 'undefined') {
                            var selectedCam = cameras[0];
                        } else {
                            var selectedCam = cameras[1];
                        }

                        scanner.start(selectedCam);
                    } else {
                        console.error('No cameras found.');
                    }
                }).catch(function (e) {
                    console.error(e);
                });
            }
        });

        $("#cancel-qr").click(function() {
            $("#scan-qr").html(`<a href="javascript:void(0)" class="btn btn-primary round glow">
                                    <i class="bx bx-printer"></i>
                                    <span class="align-middle ml-25">Pindai QR</span>
                                </a>`);
            $(this).hide()
            scanner.stop()
        });

        $("#submit_kode").click(function() {
            $.ajax({
                type: "POST",
                url: "{{route('admin.pendaftaran.qr')}}",
                dataType: "html",
                data: {
                    '_token' : '{{csrf_token()}}',
                    'code'     : $("#kode").val(),
                },
                success : function(data){
                    if (data!=0){
                        window.location="{{route('admin.pendaftaran.detail-qr-aqiqah')}}?id="+data
                    } else {
                        Swal.fire(
                            'Data tidak ditemukan',
                            'QR Code yang anda scan salah',
                            'error'
                        )
                    }
                }
            });
        });
        

        // Always Show Calendar on Ranges
        $('#date_filter').daterangepicker({
            locale: {
            format: 'YYYY-MM-DD' // --------Here
            },
            singleDatePicker: true,
            startDate: getUrlParameter('date'), 
            alwaysShowCalendars: true,
        },function() {
            
        });


        //fill link export
        if (getUrlParameter('date')== null){
            date = $('#date_filter').val();
            $("#link_export").attr("href",$("#link_export").attr("href"))
            $("#link_print").attr("href",$("#link_print").attr("href"))
        } 
        else{
            $("#link_export").attr("href",$("#link_export").attr("href")+"?date="+getUrlParameter('date'))
            $("#link_print").attr("href",$("#link_print").attr("href")+"?date="+getUrlParameter('date'))
        }

        @if(Auth::user()->role =='1')
            function confirm_approve(id){
                var id_btn = id;
                Swal.fire({
                    title: 'Setujui Pendaftaran?',
                    text: "Data Tidak Dapat Dikembalikan",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Approve',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-outline-primary ml-1',
                    buttonsStyling: false,
                    }).then(function (result) {
                    if (result.value) {
                        $(".cover-spin").fadeIn(250);
                        $.ajax({
                            type:"POST",
                            url : "{{route('admin.pendaftaran.approve')}}",
                            headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                            },
                            data: { 
                                "id": id,
                            },
                            // data: id,
                            success:function(id){
                                $(".cover-spin").fadeOut(250);
                                $("#approve"+id_btn).removeClass('btn-warning');
                                $("#approve"+id_btn).addClass('btn-success');
                                $("#approve"+id_btn+" > i").removeClass('bx-check');
                                $("#approve"+id_btn+" > i").addClass('bx-user-check');
                                $("#approve"+id_btn).removeAttr('onclick');
                                $("#cancel"+id_btn).remove();
                                Swal.fire(
                                    {
                                        type: "success",
                                        title: 'Disetujui!',
                                        text: 'Data Registrasi Telah Diterima.',
                                        confirmButtonClass: 'btn btn-success',
                                    }
                                )
                                
                            }
                        });
                        
                    }
                });
            }

            function cancel_approve(id){
                var id_btn = id;
                Swal.fire({
                    title: 'Reject Pendaftaran?',
                    input: 'text',
                    inputPlaceholder: 'Masukan alasan reject',
                    text: "Data Tidak Dapat Dikembalikan",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#D3514D',
                    cancelButtonColor: '#d12900',
                    confirmButtonText: 'OK',
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-outline-danger ml-1',
                    buttonsStyling: false,
                    }).then(function (result) {
                    if (result.value) {
                        $(".cover-spin").fadeIn(250);
                        $.ajax({
                            type:"POST",
                            url : "{{route('admin.pendaftaran.cancel')}}",
                            headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                            },
                            data: { 
                                "id": id,
                                "keterangan":result.value
                            },
                            // data: id,
                            success:function(id){
                                $(".cover-spin").fadeOut(250);
                                $("#cancel"+id_btn).removeClass('btn-warning');
                                $("#cancel"+id_btn).addClass('btn-danger');
                                $("#cancel"+id_btn+" > i").removeClass('bx-x');
                                $("#cancel"+id_btn+" > i").addClass('bx-user-x');
                                $("#cancel"+id_btn).removeAttr('onclick');
                                $("#approve"+id_btn).remove();
                                Swal.fire(
                                    {
                                        type: "success",
                                        title: 'Ditolak!',
                                        text: 'Data Registrasi Telah Ditolak.',
                                        confirmButtonClass: 'btn btn-danger',
                                    }
                                )
                                
                            }
                        });
                        
                    }
                });
            }
        @endif
        
    </script>
@endsection