@extends('adminpage.template.apps')

@section('title')
Tambah FAQ - Admin
@endsection

@section('custom_css')
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin.faq.index')}}">FAQ</a>
                                </li>
                                <li class="breadcrumb-item active"> Tambah FAQ
                                </li>
                            </ol>
                        </div>
                        <div class="col-12">
                            <h5>FAQ</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
               <section id="basic-tabs-components">
                    <div class="card col-lg-9">
                        <div class="card-header">
                            <div class="card-title">
                                <h4>Tambah FAQ</h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="tab-content">
                                    <form id="form_create" action="{{route('admin.faq.store')}}" method="post" accept-charset="utf-8">
                                        {{ csrf_field() }}
                                        <div class="tab-pane active" id="id" aria-labelledby="id-tab" role="tabpanel">
                                        <div class="row">
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Pertanyaan</label>
                                                        <input type="text" class="form-control" id="pertanyaan" name="pertanyaan" placeholder="Masukan Pertanyaan" required>
                                                    </fieldset>
                                                </div>
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Jawaban</label>
                                                        <textarea class="form-control"  id="editor1" name="jawaban" required rows="3"></textarea>
                                                    </fieldset>
                                                </div>
                                                <!-- Submit Button -->
                                                <div class="col-12">
                                                    <a id="submit" href="javascript:void(0)" class="btn btn-success round glow" style="float:right">
                                                        <span class="align-middle ml-25">Tambah</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    $('#submit').click( function() {
        $('#form_create').submit();
    });
</script>
@endsection