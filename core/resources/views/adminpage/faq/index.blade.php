@extends('adminpage.template.apps')

@section('title')
FAQ - Admin
@endsection

@section('custom_css')
<style>
    .modal-lg {
        max-width: 80% !important;
    }
</style>
@endsection

@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-6">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">FAQ
                                    </li>
                                </ol>
                            </div>
                            <div class="col-12">
                                <h5>FAQ</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-12">
                                            <a href="{{route('admin.faq.add')}}" class="btn btn-primary round glow" style="float:right">
                                                <i class="bx bx-plus"></i>
                                                <span class="align-middle ml-25">Add Data</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            @if(Auth::user()->role =='1') 
                                            <table id="faq_table" class="table table-bordered table-hover zero-configuration text-center">
                                                <thead>
                                                    <tr>
                                                        <th>Pertanyaan</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $index = 0;
                                                    @endphp
                                                    @foreach ($faq as $f)
                                                    <tr>
                                                        <td>{{$f->pertanyaan}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon rounded-circle btn-primary glow mr-1 mb-1" title="Sunting" onClick="location.href=`{{route('admin.faq.edit')}}?id={{$f->id}}`"><i class="bx bxs-edit-alt"></i></button>
                                                            <button type="button" class="btn btn-icon rounded-circle btn-danger glow mr-1 mb-1" title="Sunting" onClick="popModalDelete('{{ $f->id }}')"><i class="bx bxs-trash-alt"></i></button>
                                                        </td>
                                                        @php
                                                            $index++;
                                                        @endphp
                                                    </tr> 
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
    </div>

<!-- Modal Delete -->
<div class="modal text-left" id="modal_delete" role="dialog" aria-labelledby="myModalLabel140" aria-modal="true" style="padding-right: 16px;">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header bg-danger">
            <h5 class="modal-title white" id="myModalLabel140">Delete FAQ</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
            <i class="bx bx-x"></i>
            </button>
        </div>
        <div class="modal-body">
            <form id="edt_form">
                <div class="row">
                    <div class="col-12">
                        <fieldset class="form-group">
                            <label for="pertanyaan">Apakah ingin menghapus data ?</label>
                        </fieldset>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
            <i class="bx bx-x d-block d-sm-none"></i>
            <span class="d-none d-sm-block">Tutup</span>
            </button>

            <button type="button" class="btn btn-danger ml-1" data-dismiss="modal" onClick="submitDelete()">
            <i class="bx bx-check d-block d-sm-none"></i>
            <span class="d-none d-sm-block">Hapus</span>
            </button>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$("#faq_table").DataTable({
    "oLanguage": {
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sSearch": "Cari:",
        "sInfo": "Tampilkan _START_ Sampai _END_ dari _TOTAL_ Entri",
        "sInfoEmpty": "Tampilkan 0 Sampai 0 dari 0 Entri",
        "sZeroRecords": "Tidak ada data yang ditemukan",
        "oPaginate": {
            "sFirst":    "Awal",
            "sLast":     "Akhir",
            "sNext":     "Selanjutkan",
            "sPrevious": "Sebelumnya"
        },
    }
});
</script>

<script>
    @if(Session::get('status'))
        @if(Session::get('status') ==  1)         
            $("#toast-success-content").html("{{Session::get('status_message')}}");
            $("#toast-success").toast('show');
        @elseif (Session::get('status') ==  -1)  
            $("#toast-error-content").html("{{Session::get('status_message')}}");
            $("#toast-error").toast('show');   
        @endif
    @endif
</script>

<script>

var idTarget="";
@if(Auth::user()->role =='1')

    function popModalDelete(id){
        idTarget = id
        $('#modal_delete').modal('show');
    }
    
    function submitDelete(){
        $.post( "{{route('admin.faq.delete')}}", { 
            "_token": "{{ csrf_token() }}",
            "id": idTarget, 
        }).done(function( res ) {
            if (res==1){
                $("#toast-success-content").html('Berhasil delete data');
                $("#toast-success").toast('show');
                setTimeout(function(){ 
                    $(".cover-spin").fadeOut(500)
                    window.location.href=window.location.href;
                }, 1000);
            } else {
                $("#toast-error-content").html("Gagal delete data");
                $("#toast-error").toast('show');
                $(".cover-spin").fadeOut(500)
            }
        }).fail(function (jqXHR, textStatus) {
            $("#toast-error-content").html("Server Error");
            $("#toast-error").toast('show');
            $(".cover-spin").fadeOut(500)
        });
    }
@endif
</script>
@endsection