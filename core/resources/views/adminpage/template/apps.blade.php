<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="{{ asset('admin_template/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('admin_template/app-assets/images/ico/favicon.ico')}}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/vendors/css/charts/apexcharts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/vendors/css/extensions/dragula.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/css/themes/dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/css/themes/semi-dark-layout.css')}}">
    <!-- END: Theme CSS-->
    
    @yield('custom_css')
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_template/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_template/app-assets/vendors/css/pickers/daterange/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_template/app-assets/css/pages/dashboard-analytics.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_template/app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_template/app-assets/vendors/css/editors/quill/quill.snow.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_template/assets/css/style.css')}}">
    <!-- END: Custom CSS-->
    
    <style>
        .chart-container .highcharts-container {
            max-height: 220px;
        }
        .cover-spin {
            position:fixed;
            width:100%;
            left:0;right:0;top:0;bottom:0;
            background-color: rgba(255,255,255,0.7);
            z-index:99999;
        }
        .loader {
            width: 30px;
            height: 30px;
            margin:auto;
            left:0;
            right:0;
            top:0;
            bottom:0;
            position:fixed;
            z-index:9999;
        }
        .error {
            color:#d3514d;
        }
    </style>
</head>
<!-- END: Head-->

<!-- Authentication Links -->
@if (Auth::check())
    <!-- BEGIN: Body-->
    <div class="cover-spin">
        <div class="loader">
            <div class="spinner-grow text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

        @include('adminpage.template.navbar')

        <!-- BEGIN: Content-->
        @yield('content')
        
        <!-- END: Content-->

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0"><span class="float-left d-inline-block">2021 &copy; Polres Barito Timur</span>
                <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
            </p>
        </footer>
        <!-- END: Footer-->

        <!-- modal broadcast -->

        <!-- BEGIN: Vendor JS-->
        <script src="{{ asset('admin_template/app-assets/vendors/js/vendors.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('admin_template/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/extensions/dragula.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>

        <script src="{{ asset('admin_template/app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/pickers/pickadate/picker.time.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/pickers/pickadate/legacy.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/pickers/daterange/moment.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/vendors/js/editors/quill/quill.min.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/ckeditor/ckeditor.js')}}"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="{{ asset('admin_template/app-assets/js/core/app-menu.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/js/core/app.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/js/scripts/components.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/js/scripts/footer.js')}}"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="{{ asset('admin_template/app-assets/js/scripts/pages/dashboard-analytics.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/js/scripts/datatables/datatable.js')}}"></script>
        <!-- <script src="{{ asset('admin_template/app-assets/js/scripts/forms/validation/form-validation.js')}}"></script> -->
        <script src="{{ asset('admin_template/app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
        <script src="{{ asset('admin_template/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js')}}"></script>
        <!-- END: Page JS-->


        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        
        <script>
        function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        }

        </script>
        <!-- money separator -->
        <script>
        

        function rem_moneydot(money) {
            return parseInt(money.split(".").join(""));
        }

        function get_moneydot(money) {
            if (isNaN(parseInt(money))) {
                var convertmoney = "";
            } else {
                money = rem_moneydot(money);
                var convertmoney = money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
            }
            return convertmoney;
        }
        $(document).ready(function() {
            $(".cover-spin").fadeOut(250);
        });
        
        </script>
        @yield('script')
    </body>
    <!-- END: Body-->
@else
    <script>
        window.location="{{ route('login') }}"
    </script>
@endif

</html>