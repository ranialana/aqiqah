<!-- BEGIN: Header-->
<div class="header-navbar-shadow"></div>
    <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs is-active" href="#"><i class="ficon bx bx-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon bx bx-fullscreen"></i></a></li>
                        <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon bx bx-search"></i></a>
                            <div class="search-input">
                                <div class="search-input-icon"><i class="bx bx-search primary"></i></div>
                                <input class="input" type="text" placeholder="Explore Frest..." tabindex="-1" data-search="template-search">
                                <div class="search-input-close"><i class="bx bx-x"></i></div>
                                <ul class="search-list"></ul>
                            </div>
                        </li>
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            @if(Auth::user()->role==1)
                            <div class="user-nav d-sm-flex d-none"><span class="user-name">{{ Auth::user()->name }}</span><span class="user-status text-muted">admin</span>
                            </div><span><img class="round" src="{{ asset('admin_template/app-assets/images/portrait/small/avatar-s-11.jpg')}}" alt="avatar" height="40" width="40"></span>
                            @elseif(Auth::user()->role==2)
                            <div class="user-nav d-sm-flex d-none"><span class="user-name">{{ Auth::user()->name }}</span><span class="user-status text-muted">admin_scanner</span>
                            </div><span><img class="round" src="{{ asset('admin_template/app-assets/images/portrait/small/avatar-s-11.jpg')}}" alt="avatar" height="40" width="40"></span>
                            @elseif(Auth::user()->role==3)
                            <div class="user-nav d-sm-flex d-none"><span class="user-name">{{ Auth::user()->name }}</span><span class="user-status text-muted">admin_wilayah</span>
                            </div><span><img class="round" src="{{ asset('admin_template/app-assets/images/portrait/small/avatar-s-11.jpg')}}" alt="avatar" height="40" width="40"></span>
                            @endif
                            <div class="dropdown-menu dropdown-menu-right pb-0">
                                <div class="dropdown-divider mb-0"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    <i class="bx bx-power-off mr-50"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->

    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header" style="margin-bottom:20px">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto" style="margin: auto!important;"><a class="navbar-brand" href="{{route('admin.pendaftaran.index')}}">
                        <div class="brand-logo"><img class="logo" src="{{ asset('assets/img/logo_polres_bartim.png')}}" style="width: 42px; height: unset"></div>
                        <!-- <h2 class="brand-text mb-0"><img src="{{ asset('admin_template/app-assets/images/logo/text-logo.png')}}" style="width: 120px"></h2> -->
                    </a>
                    <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx-x d-block d-xl-none font-medium-4 primary toggle-icon bx bx-disc"></i><i class="toggle-icon bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary bx" data-ticon="bx-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">

                @if(Auth::user()->role =='2')  
                <li class="{{ Request::is('admin-page/scan') || Request::is('admin-page/scan/*') ? 'active' : '' }} nav-item"><a href="{{route('admin.scan.index')}}"><i class="bx bxs-search" data-icon="list"></i><span class="menu-title" data-i18n="Data Pendaftaran">Scan</span></a>
                </li>
                @endif

                @if(Auth::user()->role =='1' || Auth::user()->role =='2' || Auth::user()->role =='3')  
                <li class="{{ Request::is('admin-page/data-pendaftaran') || Request::is('admin-page/data-pendaftaran/*') ? 'active' : '' }} nav-item"><a href="{{route('admin.pendaftaran_aqiqah.index')}}"><i class="bx bxs-book-content" data-icon="list"></i><span class="menu-title" data-i18n="Data Pendaftaran">Data Pendaftaran</span></a>
                </li>
                @endif

                @if(Auth::user()->role =='1' || Auth::user()->role =='3')
                <li class="{{ Request::is('admin-page/faq') || Request::is('admin-page/faq/*') ? 'active' : '' }} nav-item"><a href="{{route('admin.faq.index')}}"><i class="bx bxs-note" data-icon="list"></i><span class="menu-title" data-i18n="Setting Terms">FAQ</span></a>
                </li>
                @endif

                @if(Auth::user()->role =='1' || Auth::user()->role =='3')
                <li class="{{ Request::is('admin-page/syarat-ketentuan') || Request::is('admin-page/syarat-ketentuan/*') ? 'active' : '' }} nav-item"><a href="{{route('admin.syarat.index')}}"><i class="bx bxs-notepad" data-icon="list"></i><span class="menu-title" data-i18n="Log">Syarat Ketentuan</span></a>
                </li>
                @endif

                @if(Auth::user()->role =='1' || Auth::user()->role =='3')
                <li class="{{ Request::is('admin-page/setting') || Request::is('admin-page/setting/*') ? 'open' : '' }} has-sub nav-item"><a href="#"><i class="bx bxs-data" data-icon="share"></i><span class="menu-title" data-i18n="Setting">Setting</span></a>
                    <ul class="menu-content">
                        <li class="{{ Request::is('admin-page/setting/user-management') || Request::is('admin-page/setting/user-management') ? 'active' : '' }}"><a href="{{route('admin.user_management.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="User Management">User Management</span></a>
                        <li class="{{ Request::is('admin-page/setting/jadwal-vaksinasi') || Request::is('admin-page/setting/jadwal-vaksinasi/*') ? 'active' : '' }}"><a href="{{route('admin.jadwal_vaksinasi.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Jadwal Vaksinasi">Jadwal Vaksinasi</span></a>
                        @if(Auth::user()->role =='1')
                            <li class="{{ Request::is('admin-page/setting/setting-sub-judul') || Request::is('admin-page/setting/setting-form/*') ? 'active' : '' }}"><a href="{{route('admin.setting_subjudul.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Setting Sub Judul">Setting Sub Judul</span></a>
                            <li class="{{ Request::is('admin-page/setting/setting-form') || Request::is('admin-page/setting/setting-form/*') ? 'active' : '' }}"><a href="{{route('admin.setting_form.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Setting Form">Setting Form Pendaftaran</span></a>
                        @endif
                    </ul>
                </li>
                @endif

            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->