@extends('adminpage.template.apps')

@section('title')
Setting Form - Admin
@endsection

@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/tagsinput.css')}}">
@endsection

@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="{{route('admin.pendaftaran.index')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active"> Setting Sub Judul
                            </li>
                        </ol>
                    </div>
                    <div class="col-12">
                        <h5>Setting Sub Judul</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="basic-tabs-components">
                <div class="card col-lg-9">
                    <div class="card-header">
                        <div class="card-title">
                            <h4>Setting Sub Judul</h4>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="tab-content">
                                <form id="form_create" action="{{route('admin.setting_subjudul.store')}}" method="post" accept-charset="utf-8">
                                    {{ csrf_field() }}
                                    <div class="tab-pane active" id="id" aria-labelledby="id-tab" role="tabpanel">
                                        <!-- <div class="row">
                                                <div class="col-12">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Tempat</label>
                                                        <select class="form-control" id="tempat" name="tempat" required>
                                                            @foreach ($jadwal as $j)
                                                                @if ($data->tempat==$j->id)
                                                                    <option value="{{$j->id}}" data-id="{{$j->id}}" selected>{{$j->lokasi}}</option>
                                                                @else 
                                                                    <option value="{{$j->id}}" data-id="{{$j->id}}">{{$j->lokasi}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                </div>
                                            </div> -->
                                        <div class="row">
                                            <div class="col-12 jenis_vaksin">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <fieldset class="form-group">
                                                            <label for="basicInput">Sub Judul</label>
                                                            <!-- <input name="judul" type="text" class="form-control" value="{{$data->judul}}" placeholder="Masukan Sub Judul" required> -->
                                                            <textarea name="judul" type="text" class="form-control" value="" placeholder="Masukan Sub Judul" required>{{$data->judul}}</textarea>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" >
                                            <div class="col-12" style="display: none">
                                                <fieldset class="form-group">
                                                    <label for="basicInput">Jenis Vaksin</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row" id="jenis_vaksin" style="display: none">
                                            <?php
                                            $jenis_vaksin = json_decode($data->jenis_vaksin);

                                            if (is_Array($jenis_vaksin)) {
                                                for ($i = 0; $i < count($jenis_vaksin); $i++) {
                                            ?>
                                                    <div class="col-12 jenis_vaksin" data-id="{{$i}}">
                                                        <div class="row">
                                                            <?php
                                                            if ($i > 0) {
                                                            ?>
                                                                <div class="col-10">
                                                                <?php
                                                            } else {
                                                                ?>
                                                                    <div class="col-12">
                                                                    <?php
                                                                }
                                                                    ?>
                                                                    <fieldset class="form-group">
                                                                        <input name="jenis_vaksin[]" type="text" class="form-control" value="{{$jenis_vaksin[$i]}}" placeholder="Masukan Jenis Vaksin" required>
                                                                    </fieldset>
                                                                    </div>
                                                                    <?php
                                                                    if ($i > 0) {
                                                                    ?>
                                                                        <div class="col-2">
                                                                            <button class="remove_jv btn btn-danger" data-id="{{$i}}"><i class="bx bx-trash"></i></button>
                                                                        </div>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                        </div>
                                                    <?php
                                                }
                                            } else {
                                                $i = 0;
                                                    ?>
                                                    <div class="col-12 jenis_vaksin" data-id="{{$i}}">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <fieldset class="form-group">
                                                                    <input name="jenis_vaksin[]" type="text" data-id="{{$i}}" data-role="tagsinput" class="form-control" value="" placeholder="Masukan Jenis Vaksin" required>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                            }
                                                ?>
                                                    </div>
                                                    <div class="row" >
                                                        <div class="col-12" style="display: none">
                                                            <button id="add_jv" class="btn btn-primary">Tambah Jenis Vaksin</button>
                                                        </div>

                                                        <!-- Submit Button -->
                                                        <div class="col-12">
                                                            <a id="submit" href="javascript:void(0)" class="btn btn-success round glow" style="float:right">
                                                                <span class="align-middle ml-25">Tambah</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('assets/js/tagsinput.js')}}"></script>
<script>
    @if(Session::get('status'))
    @if(Session::get('status') == 1)
    $("#toast-success-content").html("{{Session::get('status_message')}}");
    $("#toast-success").toast('show');
    @elseif(Session::get('status') == -1)
    $("#toast-error-content").html("{{Session::get('status_message')}}");
    $("#toast-error").toast('show');
    @endif
    @endif
</script>

<script>
    index_jv = parseInt('{{$i}}');
    // var index = index_jv;
    $('#add_jv').on("click", function() {
        index_jv = index_jv + 1;
        var input_jv = '<div class="col-12 jenis_vaksin" data-id="' + index_jv + '">' +
            '<div class="row">' +
            '<div class="col-10">' +
            '<fieldset class="form-group">' +
            '<input name="jenis_vaksin[]" type="text" data-role="tagsinput" class="form-control" value="" placeholder="Masukan Jenis Vaksin" required>' +
            '</fieldset>' +
            '</div>' +
            '<div class="col-2">' +
            '<button class="remove_jv btn btn-danger" data-id="' + index_jv + '"><i class="bx bx-trash"></i></button>' +
            '</div>' +
            '</div>' +
            '</div>';
        $("#jenis_vaksin").append(input_jv);

        $('.remove_jv').on("click", function() {
            var id_jv = $(this).data("id");
            $('.jenis_vaksin[data-id="' + id_jv + '"]').remove();
        });
    });

    $('.remove_jv').on("click", function() {
        var id_jv = $(this).data("id");
        $('.jenis_vaksin[data-id="' + id_jv + '"]').remove();
    });

    $('#submit').click(function() {
        $('#form_create').validate();
        $('#form_create').submit();
    });
    $("#kota").select2();
</script>
@endsection