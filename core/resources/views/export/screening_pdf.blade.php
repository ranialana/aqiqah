<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>SKRENING - Polres Barito Timur</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Place favicon.ico in the root directory -->

  <!-- Vendors -->

  <meta name="theme-color" content="#fafafa">
  <!-- Bootstrap CSS -->
  <link href="{{public_path('assets/css/bootstrap.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{public_path('assets/css/main_pdf.css')}}">
</head>
<body>
<style>
	.card-voucher{
		height: 0.53cm;
		font-size: 11px;
	}

	#booking_list{
		line-height: 0.1px;
	}

	.text-center{
		text-align: center;
	}
	.text-right{
		text-align: right;
	}

	th, tr, td {
		border-color: black !important;
		vertical-align: middle !important;
		word-wrap: break-word !important;
		padding: 10px !important;
	}

</style>
  <main>
  	<section>
	    <div>

	    	<div class="card card-voucher">

	    		<div class="card-body">
				<div class="row">

						<div class="table-responsive">
							<table id="booking_list" class="table table-bordered table-hover">
								<thead>
									<tr class="text-center">
										@php
										$gender = $data_pendaftar->jenis_kelamin;
										$cek = strftime("%Y-%m-%d", strtotime($data_pendaftar->tanggal_lahir));
										$umur = floor((time() - strtotime($cek)) / 31556926);
											$is_pregnant = $data_pendaftar->is_pregnant;
											if ($is_pregnant==1 && $gender==1){
												$is_pregnant = "Ibu hamil/menyusui";
											} else if ($is_pregnant==0 && $umur >= 18){
												$is_pregnant = "Dewasa";
											} else if ($is_pregnant==0 && $umur < 18){
												$is_pregnant = "Anak-anak";
											}
										@endphp
										@if ($is_pregnant == 'Ibu hamil/menyusui')
											<th colspan="6">KARTU KENDALI PELAYANAN VAKSINASI COVID-19 POLRES BARTIM</th>
											<th style="line-height: 10px;" >Form Ibu hamil/menyusui &nbsp - &nbsp {{$data_pendaftar->dosis}}</th>
										@endif

										@if ($is_pregnant == 'Anak-anak')
											<th colspan="6">KARTU KENDALI PELAYANAN VAKSINASI COVID-19 POLRES BARTIM</th>
											<th style="line-height: 10px;" >Form Anak &nbsp - &nbsp {{$data_pendaftar->dosis}}</th>
										@endif

										@if ($is_pregnant == 'Dewasa')
											<th colspan="6">KARTU KENDALI PELAYANAN VAKSINASI COVID-19 POLRES BARTIM</th>
											<th style="line-height: 10px;" >Form Umum &nbsp - &nbsp {{$data_pendaftar->dosis}}</th>
										@endif
									</tr>
									<tr>
										<th colspan="7">A. MEJA PRA REGISTRASI</th>
									</tr>
									<tr>
										<td colspan="2" width="20%">NAMA</td>
										<td colspan="5">{{$data_pendaftar->nama}}</td>
									</tr>
									<tr>
										<td colspan="2" width="20%">NIK</td>
										<td colspan="5">{{$data_pendaftar->nik}}</td>
									</tr>
									<tr>
										<td colspan="2" width="20%">TANGGAL LAHIR</td>
										<td colspan="5">{{$data_pendaftar->tanggal_lahir}}</td>
									</tr>
									<tr>
										<td colspan="2" width="20%">NO HP</td>
										<td colspan="5">{{$data_pendaftar->no_telp}}</td>
									</tr>
									<tr>
										<td colspan="2" width="20%">ALAMAT</td>
										<td colspan="5">{{$data_pendaftar->alamat_tinggal}}</td>
									</tr>
									<tr>
										<td colspan="2" width="20%">TANGGAL PENDAFTARAN</td>
										<td colspan="5">{{$data_pendaftar->created_at}}</td>
									</tr>

									<tr>
										<th colspan="7">B. MEJA 1 (SKRINING DAN VAKSINASI)</th>
									</tr>
									<tr class="text-center">
										<th colspan="7">SKRINING</th>
									</tr>	
									<tr class="text-center">
										<th>NO</th>
										<th width="40%" colspan="2">PEMERIKSAAN</th>
										<th width="20%" colspan="2">HASIL</th>
										<th width="20%" colspan="2">TINDAK LANJUT</th>
									</tr>

									
									
									<tr>
										<td width="10%" class="text-center">1</td>
										<td width="50%" colspan="2">SUHU</td>
										<td width="20%" colspan="2" class="text-right"><span>&#8451;</span></td>
										<td width="40%" colspan="2" style="line-height: 10px;" >Suhu > 37,5 <span>&#8451;</span> vaksinasi ditunda sampai sasaran sembuh</td>
									</tr>
									<tr>
										<td width="10%" class="text-center">2</td>
										<td width="30%" colspan="2">TEKANAN DARAH</td>
										<td width="20%" colspan="2" class="text-right">MmHg</td>
										<td width="40%" colspan="2" style="line-height: 10px;">Jika tekanan darah > 180/110 mmHg pengukuran tekanan darah diulang 5 (lima) sampai 10 (sepuluh) menit kemudian. Jika masih tinggi maka vaksinasi ditunda sampai terkontrol</td>
									</tr>

									<!-- START FORM UMUM/DEWASA -->
									@php
										$gender = $data_pendaftar->jenis_kelamin;
										$cek = strftime("%Y-%m-%d", strtotime($data_pendaftar->tanggal_lahir));
										$umur = floor((time() - strtotime($cek)) / 31556926);
										$is_pregnant = $data_pendaftar->is_pregnant;
									@endphp
									@if ($is_pregnant==0 && $umur >= 18)
									<tr>
										<th width="10%" class="text-center"></th>
										<th width="40%" colspan="2">PERTANYAAN</th>
										<th width="10%" class="text-center">YA</th>
										<th width="10%" class="text-center">TIDAK</th>
										<th width="10%"></th>
										<th width="10%"></th>
									</tr>

									<tr>
										<td width="10%" class="text-center" rowspan="2">1</td>
										<th width="30%" colspan="2">Pertanyaan untuk vaksinasi ke-1</th>
										<th width="10%" class="text-center"></th>
										<th width="10%" class="text-center"></th>
										<td width="40%" colspan="2" style="line-height: 10px;" >Jika Ya: vaksinasi diberikan di Rumah Sakit</td>
									</tr>

									<tr>
										<th width="30%" colspan="2">Pertanyaan untuk vaksinasi ke-2</th>
										<th width="10%" class="text-center"></th>
										<th width="10%" class="text-center"></th>
										<td width="40%" colspan="2" style="line-height: 10px;">Jika Ya: merupakan kontraindikasi untuk vaksinasi ke-2.</td>
									</tr>

									<tr>
										<td width="10%" class="text-center">2</td>
										<td width="30%" colspan="2">Apakah Anda sedang hamil?</td>
										
										
										@if($data_pendaftar->is_pregnant == 0)
											<th width="10%" class="text-center"></th>
											<th width="10%" class="text-center">
												<img class="me-auto" src="{{public_path('assets/img/checked.png')}}" alt="" width="10">
											</th>
										@endif
										@if($data_pendaftar->is_pregnant == 1 && $data_pendaftar->jenis_kelamin == 1)
											<th width="10%" class="text-center">
												<img class="me-auto" src="{{public_path('assets/img/checked.png')}}" alt="" width="10">
											</th>
											<th width="10%" class="text-center"></th>
										@endif
										
										<td width="40%" colspan="2" style="line-height: 10px;">Jika sedang hamil vaksinasi ditunda sampai melahirkan.</td>
									</tr>

									<tr>
										<td width="10%" class="text-center">3</td>
										<td width="30%" colspan="2">Apakah Anda mengidap penyakit autoimun seperti asma, lupus?</td>
										<th width="10%" class="text-center"></th>
										<th width="10%" class="text-center"></th>
										<td width="50%" colspan="2" style="line-height: 10px;">Jika Ya, maka vaksinasi ditunda jika sedang dalam kondisi akut atau belum terkendali.</td>
									</tr>

									<tr>
										<td width="10%" class="text-center">4</td>
										<td width="30%" colspan="2" style="line-height: 10px;">Apakah Anda sedang mendapat pengobatan untuk gangguan pembekuan darah, kelainan darah, defisiensi imun dan penerima produk darah/transfusi?</td>
										<th width="10%" class="text-center"></th>
										<th width="10%" class="text-center"></th>
										<td width="40%" colspan="2">Jika Ya: vaksinasi ditunda dan dirujuk</td>
									</tr>

									<tr>
										<td width="10%" class="text-center">5</td>
										<td width="30%" colspan="2" style="line-height: 10px;">Apakah Anda sedang mendapat pengobatan immunosupressant seperti kortikosteroid dan kemoterapi?</td>
										<th width="10%" class="text-center"></th>
										<th width="10%" class="text-center"></th>
										<td width="40%" colspan="2">Jika Ya: vaksinasi ditunda dan dirujuk</td>
									</tr>

									<tr>
										<td width="10%" class="text-center">6</td>
										<td width="30%" colspan="2" style="line-height: 10px;">Apakah Anda memiliki penyakit jantung berat dalam keadaan sesak?</td>
										<th width="10%" class="text-center"></th>
										<th width="10%" class="text-center"></th>
										<td width="40%" colspan="2">Jika Ya: vaksinasi ditunda dan dirujuk</td>
									</tr>

									<tr>
										<th colspan="7" class="text-center">Pertanyaan Nomor 7 dilanjutkan apabila terdapat penilaian kelemahan fisik pada sasaran vaksinasi.</th>
									</tr>

									<tr>
										<td width="10%" class="text-center" rowspan="6">7</td>
										<td width="30%" colspan="2">Pertanyaan tambahan bagi sasaran lansia (≥60 tahun):</td>
										<td width="10%" class="text-center"></td>
										<td width="10%" class="text-center"></td>
										<td width="40%" colspan="2" rowspan="6" style="line-height: 10px;">Jika terdapat 3 atau lebih jawaban Ya maka vaksin tidak dapat diberikan.</td>
									</tr>

									<tr>
										<td width="10%" class="text-center">1</td>
										<td width="20%" style="line-height: 10px;"> Apakah Anda mengalami kesulitan untuk naik 10 anak tangga?</td>
										<td width="10%" class="text-center"></td>
										<td width="10%" class="text-center"></td>
									</tr>

									<tr>
										<td width="10%" class="text-center">2</td>
										<td width="20%">Apakah Anda sering merasa kelelahan?</td>
										<td width="10%" class="text-center"></td>
										<td width="10%" class="text-center"></td>
									</tr>

									<tr>
										<td width="10%" class="text-center">3</td>
										<td width="25%" style="line-height: 10px;">Apakah Anda memiliki paling sedikit 5 dari 11 penyakit (Hipertensi, diabetes, kanker, penyakit paru kronis, serangan jantung, gagal jantung kongestif, nyeri dada, asma, nyeri sendi, stroke dan penyakit ginjal)?</td>
										<td width="10%" class="text-center"></td>
										<td width="10%" class="text-center"></td>
									</tr>

									<tr>
										<td width="10%" class="text-center">4</td>
										<td width="20%" style="line-height: 10px;">Apakah Anda mengalami kesulitan berjalan kira-kira 100 sampai 200 meter?</td>
										<td width="10%" class="text-center"></td>
										<td width="10%" class="text-center"></td>
									</tr>

									<tr>
										<td width="10%" class="text-center">5</td>
										<td width="20%" style="line-height: 10px;">Apakah Anda mengalami penurunan berat badan yang bermakna dalam setahun terakhir?</td>
										<td width="10%" class="text-center"></td>
										<td width="10%" class="text-center"></td>
									</tr>

									@endif
									<!-- END FORM UMUM -->

									<!-- START FORM Anak-anak -->
									@php
										$gender = $data_pendaftar->jenis_kelamin;
										$cek = strftime("%Y-%m-%d", strtotime($data_pendaftar->tanggal_lahir));
										$umur = floor((time() - strtotime($cek)) / 31556926);
										$is_pregnant = $data_pendaftar->is_pregnant;
									@endphp
									@if ($is_pregnant==0 && $umur < 18)
									
											<tr>
												<th width="10%" class="text-center"></th>
												<th width="30%" colspan="2">PERTANYAAN</th>
												<td class="text-center">YA</td>
												<td class="text-center">TIDAK</td>
												<td></td>
												<td></td>
											</tr>

											<tr>
												<td width="10%" class="text-center">1</td>
												<td width="40%" style="line-height: 10px;" colspan="2">Apakah anak mendapat vaksin lain kurang dari 1 bulan sebelumnya?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2">Jika Ya: vaksinasi ditunda</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">2</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah anak pernah sakit <br>COVID-19?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2" style="line-height: 10px;" >Jika Ya: vaksinasi ditunda sampai 3 bulan setelah sembuh</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">3</td>
												<td width="40%" style="line-height: 10px;" colspan="2">Apakah dalam keluarga terdapat kontak dengan pasien COVID-19?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="40%" colspan="2" style="line-height: 10px;">Jika ada kontak vaksinasi ditunda 2 minggu</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">4</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah dalam 7 hari terakhir anak menderita demam atau batuk pilek atau nyeri menelan atau muntah atau diare?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="40%" colspan="2" style="line-height: 10px;" >Jika Ya, vaksinasi ditunda, dianjurkan untuk berobat</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">5</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah dalam 7 hari terakhir anak perlu perawatan di RS atau menderita kedaruratan medis seperti sesak nafas, kejang, tidak sadar, berdebar-debar, pendarahan, hipertensi, tremor hebat?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="40%" colspan="2" style="line-height: 10px;" >Jika Ya, vaksinasi ditunda, dianjurkan untuk berobat</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">6</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah anak sedang menderita gangguan imunitas (hiperium, autoimun, alergi berat, dan defisiensi imun, gizi buruk, HIV berat, keganasan?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="40%" colspan="2" style="line-height: 10px;" >Jika Ya, vaksinasi ditunda, sampai dinyatakan boleh oleh dokter yang merawat</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">7</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah saat ini anak sedang menjalani pengobatan imunosupresan jangka panjang (steroid lebih dari 2 minggu, sitostatika)?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="40%" colspan="2" style="line-height: 10px;" >Jika Ya, vaksinasi ditunda, sampai dinyatakan boleh oleh dokter yang merawat.</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">8</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah anak mempunyai riwayat alergi berat seperti sesak nafas, bengkak, urtikaria di seluruh tubuh atau gejala syok anafilaksis (tidak sadar) setelah vaksinasi sebelumnya?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="40%" colspan="2">Jika ya, vaksinasi di rumah sakit</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">9</td>
												<td width="40%" colspan="2" style="line-height: 10px;" >Apakah anak penyandang penyakit hemofilia / kelainan darah?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="40%" colspan="2">Jika ya, vaksinasi di rumah sakit</td>
											</tr>
										@endif
									<!-- END FORM ANAK-ANAK -->

									<!-- START FORM IBU HAMIL/MENYUSUI -->
									@php
										$gender = $data_pendaftar->jenis_kelamin;
										$cek = strftime("%Y-%m-%d", strtotime($data_pendaftar->tanggal_lahir));
										$umur = floor((time() - strtotime($cek)) / 31556926);
										$is_pregnant = $data_pendaftar->is_pregnant;
									@endphp
									@if ($is_pregnant==1 && $gender == 1)
									

											<tr>
												<td width="10%" class="text-center">3</td>
												<td width="40%" style="line-height: 10px;" colspan="2">Berapa usia kehamilan anda? a. Trisemester 1 (0 s/d 13 minggu); b. Trisemester 2 (14 s/d 28 minggu); c. Trisemester 3 (29 minggu s/d aterm)</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2" style="line-height: 10px;">jika kurang dari 13 minggu vaksinasi ditunda</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">4</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah ibu memiliki keluhan dan tanda preeklampsia? (Kaki bengkak, Sakit kepal, Nyeri ulu hati, Pandangan kabur, Tekanan darah >140/90 mmHg) </td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2" style="line-height: 10px;" >Jika Ya: vaksinasi ditunda sampai 3 bulan setelah sembuh</td>
											</tr>

											<tr>
												<td width="10%" class="text-center" rowspan="4">5</td>
												<th width="40%" colspan="2" style="line-height: 10px;">Pertanyaan untuk vaksinasi ke-1</th>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="4%" rowspan="2" colspan="2" style="line-height: 10px;">Jika Ya vaksinasi ditunda dan dirujuk ke RS</td>
											</tr>

											<tr>
												<td width="40%" style="line-height: 10px;" colspan="2">Apakah anda memiliki riwayat alergi berat seperti sesak nafas, bengkak, dan urtikaria seluruh badan atau reaksi berat lainnya karena vaksin?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
											</tr>

											<tr>
												<th width="40%" style="line-height: 10px;" colspan="2">Pertanyaan untuk vaksinasi ke-2</th>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="4%" rowspan="2" colspan="2"></td>
											</tr>

											<tr>
												<td width="40%" style="line-height: 10px;" colspan="2">Apakah anda memiliki riwayat alergi berat setelah divaksin COVID-19 sebelumnya?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
											</tr>

											<tr>
												<td width="10%" class="text-center">6</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah anda mempunyai penyakit penyerta, seperti: Jantung, DM, Asma, Penyakit paru-paru, HIV, Hiperteroid/Hipoteroid,Penyakit ginjal kronik, Penyakit hati</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2" style="line-height: 10px;" >Jika dalam kondisi terkontrol dan tidak ada komplikasi akut maka vaksin dapat diberikan</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">7</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah anda mengidap penyakit autoimun seperti lupus</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2" style="line-height: 10px;" >Jika dalam kondisi terkontrol dan tidak ada komplikasi akut maka vaksin dapat diberikan</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">8</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah anda sedang mendapat pengobatan untuk gangguan pembekuan darah, kelainan darah, defisiensi imun, dan penerima produk darah/transfusi?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2" style="line-height: 10px;">Jika Ya vaksinasi ditunda dan dirujuk</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">9</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah anda sedang mendapat pengobatan immunosupressant seperti kortikosteroid dan kemoterapi?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2" style="line-height: 10px;" >Jika Ya vaksinasi ditunda dan dirujuk</td>
											</tr>

											<tr>
												<td width="10%" class="text-center">10</td>
												<td width="40%" colspan="2" style="line-height: 10px;">Apakah anda pernah terkonfirmasi menderita COVID-19?</td>
												<th width="4%" class="text-center"></th>
												<th width="5%" class="text-center"></th>
												<td width="20%" colspan="2" style="line-height: 10px;" >Jika Ya vaksinasi ditunda sampai 3 (tiga) bulan setelah sembuh.  </td>
											</tr>
										@endif
									<!-- END FORM IBU HAMIL/MENYUSUI -->

									<tr>
										<th width="10%" colspan="3">HASIL SKRINING :</th>
										<th width="20%" rowspan="2" colspan="2" class="text-center" style="line-height: 10px;">SETUJU MENERIMA <br>VAKSIN COVID-19</th>
										<th width="10%" class="text-center" colspan="2">PARAF PETUGAS</th>
									</tr>

									<tr>
										<td width="10%" class="text-center">	
											<img class="me-auto" src="{{public_path('assets/img/empty-checkbox.png')}}" alt="" width="10">
										</td>
										<td width="10%" colspan="2">LANJUT VAKSIN</td>
										<td width="10%" colspan="2" rowspan="3"></td>
										
									</tr>

									<tr>
										<td width="10%" class="text-center">	<img class="me-auto" src="{{public_path('assets/img/empty-checkbox.png')}}" alt="logo" width="10"></td>
										<td width="10%" colspan="2">TUNDA</td>
										<td width="10%" class="text-center">YA</td>
										<td width="10%" class="text-center">TIDAK</td>
									</tr>

									<tr>
										<td width="10%" class="text-center">	<img class="me-auto" src="{{public_path('assets/img/empty-checkbox.png')}}" alt="logo" width="10"></td>
										<td width="10%" colspan="2">TIDAK DIBERIKAN</td>
										<td width="10%" class="text-center"></td>
										<td width="10%" class="text-center"></td>
									</tr>

									<tr>
										<th colspan="7">HASIL VAKSINASI:</th>
									</tr>

									<tr>
										<th width="10%" colspan="5">JENIS VAKSIN :</th>
										<th width="20%" class="text-center" colspan="2">PARAF PETUGAS</th>
									</tr>

									<tr>
										<td width="10%" colspan="5">NO. BATCH :</td>
										<td width="20%" colspan="2" rowspan="3"></td>
									</tr>

									<tr>
										<td width="10%" colspan="5">TANGGAL VAKSIN :</td>
									</tr>

									<tr>
										<td width="10%" colspan="5">JAM VAKSIN :</td>
									</tr>

									<tr>
										<th colspan="7">C. MEJA 2 (PENCATATAN DAN OBSERVASI)</th>
									</tr>

									<tr>
										<th colspan="7">HASIL OBSERVASI</th>
									</tr>

									<tr>
										<td width="10%" class="text-center" rowspan="2"><img class="me-auto" src="{{public_path('assets/img/empty-checkbox.png')}}" alt="logo" width="10"></td>
										<td width="10%" colspan="4" rowspan="2">ADA KELUHAN</td>
										<th width="10%" class="text-center" colspan="2">PARAF PETUGAS</th>
									</tr>

									<tr>
										<td width="10%" colspan="2" rowspan="4"></td>
									</tr>

									<tr>
										<td width="10%" class="text-center" rowspan="2"><img class="me-auto" src="{{public_path('assets/img/empty-checkbox.png')}}" alt="logo" width="10"></td>
										<td width="10%"  colspan="4" rowspan="2">SEBUTKAN KELUHAN JIKA ADA, ............................</td>
									</tr>
								</thead>
							</table>
						</div>
	    			</div>
	    		</div>


	    	</div>

	    </div>

  	</section>
  </main>
</body>

</html>
