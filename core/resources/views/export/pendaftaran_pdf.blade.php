<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Print PDF Data Vaksinasi COVID-19 - Polres Barito Timur</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Place favicon.ico in the root directory -->

  <!-- Vendors -->

  <meta name="theme-color" content="#fafafa">
  <!-- Bootstrap CSS -->
  <link href="{{public_path('assets/css/bootstrap.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{public_path('assets/css/main_pdf.css')}}">
</head>
<body>
<style>
	.card-voucher{
		height: 371mm;
	}
</style>
  <main>
  	<section>
	    <div>

	    	<div class="card card-voucher">
	    		<div class="card-header">
	    			&nbsp;
	    		</div>

	    		<div class="card-body">
	    			<!-- Logo -->
	    			<div class="row">
	    				<div class="col-6 col-print-12 d-flex flex-row">
							<img class="me-auto" src="{{public_path('assets/img/logo_dokkes_polri.png')}}" alt="logo" width="70" style="margin-left:15px">
							<img class="me-auto" src="{{public_path('assets/img/polri.png')}}" alt="logo" width="70" style="margin-left:15px">
							<img class="me-auto" src="{{public_path('assets/img/logo_polres_bartim.png')}}" alt="logo" width="70" style="margin-left:15px">
	    				</div>
						<div class="col-6">
							<img class="ms-4 me-0 object-fit-contain" src="{{public_path('assets/img/logo-kabayan.png')}}" alt="logo" width="80" style="float:right;margin-right:5px">
							<span style="float:right;margin-top:20px;margin-right:5px">Supported By</span>
						</div>
	    			</div>

	    			<hr>

	    			<!-- Heading label -->
	    			<div class="row">
	    				<div class="col-8 col-print-8">
	    					<h1 class="no-line mt-0 mb-2">Voucher Vaksinasi</h1>
	    					<span>
	    						<strong>{{$jadwal->lokasi}}</strong><br>
	    					</span>
	    					<ul class="text-small list-unstyled">
	    						<li>{{ HariIdn(strftime('%A', strtotime($jadwal->tanggal_vaksin)))}}, {{strftime('%d', strtotime($jadwal->tanggal_vaksin))}} {{BulanIdn(strftime('%m',strtotime($jadwal->tanggal_vaksin)))}} {{strftime('%Y', strtotime($jadwal->tanggal_vaksin))}} {{$sesi_nama}} {{$sesi_vaksin}}</li>
	    					</ul>
	    				</div>
	    				<div class="col-4">
	    					<div class="qr-container" style="float:right;margin-right:10px">
	    						<img src="data:image/png;base64, {{$qr}}" alt="QR Code" height="150">
	    					</div>
	    				</div>
	    			</div>

	    			<!-- Biodata Peserta -->
	    			<div class="row">
	    				<div class="col-12">
							<b>Nama</b></br>{{$data_pendaftar->nama}}
						</div>
					</div>
					<br>
					<div class="row">
	    				<div class="col-4"><b>No. Registrasi</b></br>{{$unique_id}}</div>
	    				<div class="col-4"><b>No. Telepon</b></br>{{$data_pendaftar->no_telp}}</div>
	    				<div class="col-4"><b>NIK</b></br>{{$data_pendaftar->nik}}</div>
					</div>

	    			<!-- Syarat & Ketentuan -->
	    			<hr>

	    			<h5 class="no-line mb-2">Syarat dan Ketentuan</h5>
					<br>
					@if ($syarat)
						{!! $syarat->data !!}
					@endif
	    		</div>

	    		<div class="card-footer">
	    			&nbsp;
	    		</div>
	    	</div>

	    </div>

  	</section>
  </main>
</body>

</html>
