<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Print PDF Data Aqiqah</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Place favicon.ico in the root directory -->

  <!-- Vendors -->

  <meta name="theme-color" content="#fafafa">
  <!-- Bootstrap CSS -->
  <link href="{{public_path('assets/css/bootstrap.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{public_path('assets/css/main_pdf.css')}}">
</head>
<body>
<style>
	.card-voucher{
		height: 371mm;
	}
</style>
  <main>
  	<section>
	    <div>

	    	<div class="card card-voucher">
	    		<div class="card-header">
	    			&nbsp;
	    		</div>

	    		<div class="card-body">
	    			<!-- Logo -->
	    			<div class="row">
						<div class="col-6">
							<img class="ms-4 me-0 object-fit-contain" src="{{public_path('assets/img/logo-kabayan.png')}}" alt="logo" width="80" style="float:right;margin-right:5px">
							<span style="float:right;margin-top:20px;margin-right:5px">Supported By</span>
						</div>
	    			</div>

	    			<hr>

	    			<!-- Heading label -->
	    			<div class="row">
	    				<div class="col-8 col-print-8">
	    					<h1 class="no-line mt-0 mb-2">Pendaftaran</h1>
	    					<ul class="text-small list-unstyled">
	    						<li></li>
	    					</ul>
	    				</div>
	    				<div class="col-4">
	    					<div class="qr-container" style="float:right;margin-right:10px">
	    						<img src="data:image/png;base64, {{$qr}}" alt="QR Code" height="150">
	    					</div>
	    				</div>
	    			</div>

	    			<!-- Biodata Peserta -->
	    			<div class="row">
	    				<div class="col-12">
							<b>Nama</b></br>
						</div>
					</div>
					<br>
	    		</div>

	    		<div class="card-footer">
	    			&nbsp;
	    		</div>
	    	</div>

	    </div>

  	</section>
  </main>
</body>

</html>
