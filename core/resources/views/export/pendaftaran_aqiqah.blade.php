<style>
.page-break {
    page-break-after: always;
}
table {
  border-collapse:separate; 
  border-spacing: 0 2em;
}
footer {
                position: fixed; 
                bottom: 30px; 
                left: 0px; 
                right: 0px;
                height: 80px; 

                /** Extra personal styles **/
                background-color: #ffffff;
                color: black;
                text-align: left;
                line-height: 35px;
            }
			.brxsmall {
            display: block;
            margin-bottom: -.4em;
        }

		grid-container {
  display: grid;
  grid-template-columns: auto auto auto;
  background-color: #2196F3;
  padding: 10px;
}
.grid-item {
  background-color: rgba(255, 255, 255, 0.8);
  border: 1px solid rgba(0, 0, 0, 0.8);
  padding: 20px;
  font-size: 30px;
  text-align: center;
}
</style>
<p style="text-align:center"><img class="me-auto" src="{{ public_path("images/elemen_rspv_1.png") }}" alt="logo" width="300" style="margin-left:15px"></p>
<p style="font-size: 30px;text-align:center;">Pendaftaran Peserta Aqiqah
</p>
<p style="font-size: 20px;text-align:center;">Selamat, pendaftaran peserta aqiqah atas nama @if ($query) {{$query->nama}} @endif berhasil!
</p>
<table style="width:100%">
	<tr>
		<td></td>
		<td style="text-align: center;">
			<img src="data:image/png;base64, {{$qr_code}}" alt="QR Code" width="30%">
		</td>
		<td></td>
	</tr>
</table>
<table>
	<tr>
		<td colspan="3"><strong>  </strong></td>
	</tr>
	<tr>
		<td>Kode Pendaftaran</td>
		<td> : </td>
		<td>@if ($query) {{$query->nik}} @endif</td>
	</tr>
	<tr>
		<td>Nama Lengkap Pendaftar</td>
		<td> : </td>
		<td>@if ($query) {{$query->nama}} @endif</td>
	</tr>
    @php 
	$anggota = array();
    if($query) {
        $anggota = json_decode($query->anggota_keluarga);
    } 
    @endphp

	@if($anggota != NULL)
    @if(count($anggota) > 0)
        @foreach($anggota as $key => $row)
        @php $keys = $key + 1; @endphp
        <tr>
		    <td>Nama Lengkap Anggota Keluarga Ke- {{$keys}}</td>
		    <td> : </td>
		    <td>{{$row}}</td>
	    </tr>
        @endforeach
    @endif
	@endif
    <tr>
		<td>Total yang Hadir</td>
		<td> : </td>
		<td>@if ($query) {{$query->jumlah_anggota + 1}} @endif</td>
	</tr>
	<tr>
		<td colspan="3"></td>
	</tr>
	<tr>
		<td colspan="3"></td>
	</tr>
	<tr>
		<td colspan="3"></td>
	</tr>
</table>
<footer>
	<hr>
	<div style="clear:both; position:relative;">
    <div style="position:absolute; left:0pt; width:480pt;">
		<small>
		Ruko D'Bali Pasteur No. A11 Jl. Ranca Bali Pasirkaliki Cimahi Utara Kota Cimahi Jawa Barat 40514
		<br>Ika Iqlima - 087821968369 <br>Zulfikar Hakim - 08981911551</small>
    </div>
    <div style="margin-left:430pt;">
		<img src="{{ public_path("images/qr_edit.png") }}" width="140">
    </div>
</div>
</footer>
