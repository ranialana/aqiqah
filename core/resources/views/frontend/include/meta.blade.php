<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta property="og:title" content="Polres Barito Timur">
<meta property="og:type" content="place">
<meta property="og:url" content="polresbaritotimur.id/">
<meta property="og:image" content="../assets/img/logo_polres_bartim.png">
<meta property="og:description" content="Vaksinasi Polres Barito Timur" />

<link rel="apple-touch-icon" sizes="180x180" href="../apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
<link rel="manifest" href="../site.webmanifest">
<link rel="mask-icon" href="../safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffc40d">
<meta name="theme-color" content="#ffffff">
<!-- Place favicon.ico in the root directory -->

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<!-- Vendors -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<style>
    .swal2-title:after{
        background-color:rgba(0,0,0,0) !important
    }
</style>
<meta name="theme-color" content="#fafafa">