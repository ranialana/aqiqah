@extends('frontend.layouts.apps')

@section('title','Admin Page - Pendaftaran Aqiqah')

@section('meta')
    @include('frontend.include.meta')
@endsection

@section('header')
    @include('frontend.include.header')
    
    <style>
    .grecaptcha-badge {
        z-index: 9999;
    }
    </style>
@endsection

@section('content')
<div class="container table-content">
    <div class="text-center">
        <h1>
            Admin Registrasi Pendaftaran Aqiqah<br>
        </h1>
    </div>
    <div class="row">
        <form id="form_checking" action="{{route('login_post')}}" method="post" accept-charset="utf-8">
            {{ csrf_field() }}
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">

                        <div class="form-group mb-3">
                            <label for="username">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required> 
                            <div class="invalid-feedback">Tidak boleh kosong</div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group mb-3">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" minlength="6" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="row mt-4">
                            <div class="col-12 col-md-8 text-center d-flex">
                                <!--
                                <div class="d-flex w-100 h-100 justify-content-center align-items-center" style="background-color: #cacaca;">
                                    Google reCAPTCHA field
                                </div>
                                -->
                            </div>
                            <div class="col-12 col-md-4 d-grid gap-2">
                                <button type="submit" class="btn btn-primary g-recaptcha" 
                                data-callback='onSubmit' 
                                data-action='submit'><i class="bi bi-login"></i> Log in</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </form>
    </div>

</div>

@endsection
@section('footer')
    @include('frontend.include.footer')
@endsection

@section('custom-script')
    @include('frontend.include.custom-script')
    
<script>
    @if (Session::get('message'))
        Swal.fire(
            'Gagal Login',
            "{{Session::get('message')}}",
            'error'
        )
    @endisset
    function onSubmit(token) {
        document.getElementById("form_checking").submit();
    }
</script>

@endsection
