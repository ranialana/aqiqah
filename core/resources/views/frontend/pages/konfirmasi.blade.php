@extends('frontend.layouts.apps')

@section('title','Konfirmasi - Pendaftaran Vaksinasi Polres Barito Timur')

@section('meta')
    @include('frontend.include.meta')
@endsection

@section('header')
    @include('frontend.include.header')
    
@endsection

@section('content')
<div class="container table-content">
    <div class="text-center">
        <h1>
            Pendaftaran Vaksinasi COVID-19
        </h1>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <!-- Data Peserta -->
                <div class="card mb-5">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="no-line mb-4 text-center">Selamat, pendaftaran vaksinasi COVID-19 di {{(is_null(Session::get('jadwal')) ? 'Data Tidak Valid' : Session::get('jadwal')->lokasi)}} atas nama {{Session::get('data_pendaftar')->nama}} berhasil!</h5>
                            </div>
                        </div>
                        

                        <div class="form-group text-center mb-3">
                            <div class="d-flex flex-column">
                                <div id="qrcode" class="qr-container" style="margin:0px auto; text-align:center;">
                                    
                                </div>
                                <div class="text-center" style="margin-top:20px">
                                    <a href="{{route('konfirmasi_pdf')}}/?code={{Session::get('unique_id')}}" type="button" class="btn btn-primary download-pdf"><i class="bi bi-download"></i> Unduh PDF</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group mb-3">
                            <label for="nama">Nama Lengkap</label>
                            <input type="text" class="form-control" id="nama" value="{{Session::get('data_pendaftar')->nama}}" readonly>
                        </div>
                        <div class="form-group mb-3">
                            <label for="nik">NIK</label>
                            <input type="text" class="form-control" id="nik" maxlength="16" value="{{Session::get('data_pendaftar')->nik}}" readonly>
                        </div>
                        <div class="form-group mb-3">
                            <label for="tgl_lahir">Tanggal Lahir</label>
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="tgl_lahir" value="{{Session::get('data_pendaftar')->tanggal_lahir}}" readonly>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="umur">Umur</label>
                            <input type="text" class="form-control" id="umur" value="@if(count(explode('-',Session::get('data_pendaftar')->tanggal_lahir))>2){{date('Y')-explode('-',Session::get('data_pendaftar')->tanggal_lahir)[2]}} Tahun @endif" readonly>
                        </div>
                        <div class="form-group mb-3">
                            <label for="umur">Jenis Kelamin</label>
                            <div class="input-group">
                                @php
                                    $gender = Session::get('data_pendaftar')->jenis_kelamin;

                                    if($gender==0){
                                        $gender = "Laki - laki";
                                    } else if ($gender==1){
                                        $gender = "Perempuan";
                                    } else {
                                        $gender = "Other";
                                    }
                                @endphp
                                <input type="text" class="form-control datepicker" id="umur" value="{{$gender}}" readonly>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="alamat">Alamat Tinggal</label>
                            <textarea name="alamat" rows="3" class="form-control" readonly>{{Session::get('data_pendaftar')->alamat_tinggal}}</textarea>
                        </div>
                        <div class="form-group mb-3">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <label for="no_telp">Nomor Telepon (WhatsApp)</label>
                                    <input type="text" class="form-control" id="no_telp" maxlength="16" value="{{Session::get('data_pendaftar')->no_telp}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <label for="email">Alamat Email</label>
                                    <input type="email" class="form-control" id="email" value="{{Session::get('data_pendaftar')->email}}" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-3" style="display:none;">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    @php
                                        setlocale(LC_ALL, 'IND');
                                    @endphp
                                    <label for="tgl_vaksin">Lokasi - Tanggal Vaksin</label>
                                    @if(is_null(Session::get('jadwal')))
                                    <input type="tgl_vaksin" class="form-control" id="tgl_vaksin" value="Data Tidak Valid" readonly>
                                    
                                    @else
                                    <input type="tgl_vaksin" class="form-control" id="tgl_vaksin" value="{{Session::get('jadwal')->lokasi}} - {{strftime('%A, %d %B %Y',strtotime(Session::get('jadwal')->tanggal_vaksin))}}" readonly>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-3" style="display:none;">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <label for="tgl_vaksin">Sesi Vaksin</label>
                                    @if(Session::get('jadwal') == 'Data tidak valid')
                                    <input type="tgl_vaksin" class="form-control" id="tgl_vaksin" value="Data Tidak Valid" readonly>
                                    @else
                                    <input type="tgl_vaksin" class="form-control" id="tgl_vaksin" value="{{Session::get('sesi_nama')}} ({{Session::get('sesi_vaksin')}})" readonly>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="mb-5">
                    <a href="{{route('konfirmasi_pdf')}}/?code={{Session::get('unique_id')}}" type="button" class="btn btn-lg btn-block btn-primary download-pdf"><i class="bi bi-download"></i> Unduh Bukti Pendaftaran Vaksin</a>
                </div>

                <!-- syarat & ketentuan -->
                <div class="card mb-5">
                    <div class="card-body">
                        <h2 class="no-line text-center mb-4">Syarat dan Ketentuan</h2>
                        @if ($syarat)
                            {!! $syarat->data !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        </div>
</div>
@endsection

@section('faq')
    @include('frontend.include.faq')
@endsection

@section('footer')
    @include('frontend.include.footer')
@endsection

@section('custom-script')
    @include('frontend.include.custom-script')
<script src="{{asset('admin_template/custom-js/qrcode.min.js')}}" ></script>
<script src="{{asset('admin_template/custom-js/html2canvas.js')}}" ></script>
<script>
new QRCode(document.getElementById("qrcode"), "{{Session::get('unique_id')}}");
</script>
@endsection
