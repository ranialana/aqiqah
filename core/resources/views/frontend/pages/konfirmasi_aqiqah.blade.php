@extends('frontend.layouts.apps')

@section('title','Konfirmasi - Pendaftaran Aqiqah')

@section('meta')
    @include('frontend.include.meta')
@endsection

@section('header')
    @include('frontend.include.header')
    
@endsection

@section('content')
<div class="container table-content">
    <div class="text-center">
        <h1>
            Pendaftaran Peserta Aqiqah
        </h1>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <!-- Data Peserta -->
                <div class="card mb-5">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="no-line mb-4 text-center">Selamat, pendaftaran peserta aqiqah atas nama {{Session::get('data_aqiqah')->nama}} berhasil!</h5>
                            </div>
                        </div>
                        

                        <div class="form-group text-center mb-3">
                            <div class="d-flex flex-column">
                                <div id="qrcode" class="qr-container" style="margin:0px auto; text-align:center;">
                                </div>
                                <div class="text-center" style="margin-top:20px">
                                    <a href="{{route('konfirmasi_pdf_aqiqah')}}/?code={{Session::get('unique_id')}}" type="button" class="btn btn-primary download-pdf"><i class="bi bi-download"></i> Unduh PDF</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="nama">Kode Pendaftaran</label>
                            <input type="text" class="form-control" id="kode" value="{{Session::get('data_aqiqah')->nik}}" readonly>
                        </div>
                        <div class="form-group mb-3">
                            <label for="nama">Nama Lengkap Pendaftar</label>
                            <input type="text" class="form-control" id="nama" value="{{Session::get('data_aqiqah')->nama}}" readonly>
                        </div>
                        @if(Session::get('data_aqiqah')->anggota_keluarga)
                            @php $data_aqiqah = json_decode(Session::get('data_aqiqah')->anggota_keluarga); @endphp
                            @if(count($data_aqiqah) > 0)
                                @foreach($data_aqiqah as $key =>$row)
                                @php $angg = $key + 1; @endphp
                                <div class="form-group mb-3">
                                    <label for="nama">Nama Lengkap Anggota Keluarga Ke- {{$angg}} </label>
                                    <input type="text" class="form-control" id="nama" value="{{$row}}" readonly>
                                </div>
                                @endforeach
                            @endif
                        @endif

                        @php 
                            $total_hadir = 0;
                            $total_hadir = Session::get('data_aqiqah')->jumlah_anggota + 1;
                        @endphp
                        <div class="form-group mb-3">
                            <label for="nama">Total Yang Hadir</label>
                            <input type="text" class="form-control" id="nama" value="{{$total_hadir}}" readonly>
                        </div>
                    </div>
                </div>

                <div class="mb-5">
                    <!--
                        <a href="{{route('konfirmasi_pdf')}}/?code={{Session::get('unique_id')}}" type="button" class="btn btn-lg btn-block btn-primary download-pdf"><i class="bi bi-download"></i> Unduh Bukti Pendaftaran</a>
                    -->
                </div>
            </div>
        </div>
        </div>
</div>
@endsection

@section('footer')
    @include('frontend.include.footer')
@endsection

@section('custom-script')
    @include('frontend.include.custom-script')
<script src="{{asset('admin_template/custom-js/qrcode.min.js')}}" ></script>
<script src="{{asset('admin_template/custom-js/html2canvas.js')}}" ></script>
<script>
new QRCode(document.getElementById("qrcode"), "{{Session::get('unique_id')}}");
</script>
@endsection
