@extends('frontend.layouts.apps')

@section('title','FAQ - Pendaftaran Vaksinasi Polres Barito Timur')

@section('meta')
    @include('frontend.include.meta')
@endsection

@section('header')
    @include('frontend.include.header')
@endsection

@section('content')
<div class="container table-content">
    <div class="text-center">
        <h1>
            Pertanyaan Umum Seputar Pendaftaran Vaksin COVID-19<br>
        </h1>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">

                    <div class="accordion" id="accordionExample">

                        @if($faq) 
                            @php
                                $i=0
                            @endphp
                            @foreach ($faq as $s)
                                @if ($i==0)
                                    <div class="accordion-item">
                                        <span class="accordion-header" id="heading-faq{{$i}}">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-faq{{$i}}" aria-expanded="true" aria-controls="collapse-faq{{$i}}">
                                                {{$s->pertanyaan}}
                                            </button>
                                        </span>
                                        <div id="collapse-faq{{$i}}" class="accordion-collapse collapse show" aria-labelledby="heading-faq{{$i}}" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                {!! $s->jawaban !!}
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="accordion-item">
                                        <span class="accordion-header" id="heading-faq{{$i}}">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-faq{{$i}}" aria-expanded="true" aria-controls="collapse-faq{{$i}}">
                                                {{$s->pertanyaan}}
                                            </button>
                                        </span>
                                        <div id="collapse-faq{{$i}}" class="accordion-collapse collapse" aria-labelledby="heading-faq{{$i}}" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                {!! $s->jawaban !!}
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @php
                                    $i++
                                @endphp
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('faq')
    <!-- Aside -->
    <aside id="faq">
        <div class="container">
            <span></span>
            <a href="{{route('home')}}" class="btn btn-primary">Beranda</a>
        </div>
    </aside>
@endsection

@section('footer')
    @include('frontend.include.footer')
@endsection

@section('custom-script')
    @include('frontend.include.custom-script')
@endsection

@section('custom-js')
    @include('frontend.include.custom-js')
@endsection