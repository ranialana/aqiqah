@extends('frontend.layouts.apps')

@section('title','Pendaftaran Vaksinasi Polres Barito Timur')

@section('meta')
    @include('frontend.include.meta')
@endsection

@section('header')
    @include('frontend.include.header')
    
<script src="https://www.google.com/recaptcha/api.js?render=6Ld16uscAAAAAH2ZKczzmFVn_gpAjSLagyB9qznH"></script>
<style>
.btn-primary:disabled {
    color: #fff;
    background-color: #58606c !important;
    border-color: #58606c!important;
}
.grecaptcha-badge {
    z-index: 9999;
}
.cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:99999;
}
.loader {
    width: 30px;
    height: 30px;
    margin:auto;
    left:0;
    right:0;
    top:0;
    bottom:0;
    position:fixed;
    z-index:9999;
}

.jadwal_vaksin_widget{
    display: none !important;
}

.sub_judul {
    margin-top: -10px;
}
.sub_judul::after {
content: none;
}
</style>
@endsection

@section('content')
    <div class="cover-spin">
        <div class="loader">
            <div class="spinner-grow text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <div class="container table-content">
        <form id="form_checking" action="{{route('pendaftaran_post')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            {{ csrf_field() }}    
            <div class="text-center">
                <h1>
                    Formulir Pendaftaran Vaksinasi COVID-19
                </h1>
                <h5 class="sub_judul">
				{{$setting_sub_judul->judul}}
                <?php
                $jenis_vaksin = json_decode($setting_sub_judul->jenis_vaksin);
                $jenis = '';
                for($i=0; $i<count($jenis_vaksin); $i++) {
                    if($i == 0){
                        $jenis .= $jenis_vaksin[$i];
                    }
                    elseif($i > 0 && $i < count($jenis_vaksin) && count($jenis_vaksin) - $i > 1) {
                        $jenis .= ', '.$jenis_vaksin[$i];
                    }
                    elseif($i < count($jenis_vaksin) && count($jenis_vaksin)-$i == 1) {
                        $jenis .= ' dan '.$jenis_vaksin[$i];
                    }
                }
                ?>
                <!-- {{$jenis}} untuk Masyarakat Umum dan Anak Sekolah Bertempat di {{$setting_sub_judul->tempat}} -->

				</h5>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Data Peserta -->
                        <div class="card mb-5">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="no-line mb-4">Identitas Penerima Vaksin</h5>
                                    </div>
                                    <div class="col text-end">
                                        <small>)<b><span class="text-danger">*</span> = Wajib diisi</b></small>
                                    </div>
                                </div>
                                
                                <div class="form-group mb-3">
                                    <label for="nama">Nama Lengkap<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama lengkap Anda" required >
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="nik">NIK<span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" id="nik" name="nik" maxlength="16" placeholder="NIK KTP @if($setting->is_kota==1){{$kota->nama}}@endif" required>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="not_manggarai" name="not_manggarai">
                                        <label class="form-check-label" for="flexCheckDefault" style="margin-top:5px">
                                            Centang jika tidak ber-KTP Barito Timur
                                            <!-- @if($setting->is_kota==1){{$kota->nama}}@endif -->
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group mb-3">
                                    <label for="upload">Foto KTP/KK<span class="text-danger">*</span></label>
                                    <input type="file" name="file" class="form-control" id="file" accept="image/png, image/gif, image/jpeg" >
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <div class="form-group mb-3 domisili">
                                    <label for="provinsi">Provinsi<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="provinsi" name="provinsi" placeholder="Provinsi" >
                                    <!-- <select id="provinsi" name="provinsi" class="form-control form-select" disabled="disabled">
                                        <option value="">(Pilih)</option>
                                        @foreach ($provinsi as $r)
                                            @if ($r->kode==53)
                                                <option value="{{$r->nama}}" selected>{{$r->nama}}</option>
                                            @else
                                                <option value="{{$r->nama}}" style="display:none">{{$r->nama}}</option>
                                            @endif
                                        @endforeach
                                    </select> -->
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <div class="form-group mb-3 domisili">
                                    <label for="kabupaten">Kabupaten/Kota<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="kabupaten" name="kabupaten" placeholder="Kabupaten" >
                                    <!-- <select id="kabupaten" name="kabupaten" class="form-control form-select" disabled="disabled">
                                        <option value="">(Pilih)</option>
                                        <option value="{{$kota->nama}}" selected>{{$kota->nama}}</option>
                                    </select> -->
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <!-- <div class="form-group mb-3 domisili">
                                    <label for="kecamatan">Kecamatan<span class="text-danger">*</span></label>
                                    <select id="kecamatan" name="kecamatan" class="form-control form-select">
                                        <option value="">(Pilih)</option>
                                        @foreach ($kecamatan as $k)
                                            <option value="{{$k->kode}}">{{$k->nama}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>

                                <div class="form-group mb-3 domisili">
                                    <label for="kelurahan">Kelurahan<span class="text-danger">*</span></label>
                                    <select id="kelurahan" name="kelurahan" class="form-control form-select" disabled>
                                        <option value="">(Pilih)</option>
                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div> -->

                                <div id="section_keterangan_tambahan" class="form-group mb-3" style="display:none">
                                    <label id="label_keterangan_tambahan" for="keterangan_tambahan"><span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="keterangan_tambahan" name="keterangan_tambahan">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="tgl_lahir">Tanggal Lahir<span class="text-danger">*</span></label>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <input autocomplete="off" type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="DD-MM-YYYY" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <div class="form-group mb-3" id="gender">
                                    <label for="gender">Jenis Kelamin<span class="text-danger">*</span></label>
                                    <select  name="gender" class="form-control form-select" required>
                                        <option value="">(Pilih)</option>
                                        <option value="0">Laki-laki</option>
                                        <option value="1">Perempuan</option>
                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>

                                <div class="form-group mb-3" id="is_pregnant">
                                    <label for="riwayat_covid">Apakah Anda sedang hamil?<span class="text-danger">*</span></label>
				                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="is_pregnant" value="1" id="is_pregnant1">
                                        <label class="form-check-label" for="is_pregnant">
                                            Ya
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="is_pregnant"  value="0" id="is_pregnant2">
                                        <label class="form-check-label" for="is_pregnant">
                                           Tidak
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group mb-3">
                                    <label for="alamat">Alamat Sesuai KTP<span class="text-danger">*</span></label>
                                    <textarea name="alamat" rows="3" class="form-control" required></textarea>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                
                                <div class="form-group mb-3">
                                    <label for="riwayat_penyakit">Pekerjaan<span class="text-danger">*</span></label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status_pekerjaan" id="status_bekerja" value="Bekerja">
                                        <label class="form-check-label" for="status_profesi">
                                            Bekerja
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status_pekerjaan" id="" value="Pelajar/Mahasiswa">
                                        <label class="form-check-label" for="status_pekerjaan">
                                            Pelajar/Mahasiswa
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status_pekerjaan" id="" value="Tidak Bekerja">
                                        <label class="form-check-label" for="status_pekerjaan">
                                            Tidak Bekerja
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group mb-3" id="status_profesi">
                                    <label for="nama_instansi">Status Profesi<span class="text-danger">*</span></label>
                                    <select name="status_profesi" id="statusprofesi" class="form-control form-select">
                                        <option value="">(Pilih)</option>
                                        <option value="PNS">PNS</option>
                                        <option value="Honorer">Honorer</option>
                                        <option value="TNI">TNI</option>
                                        <option value="POLRI">POLRI</option>
                                        <option value="Swasta">Swasta</option>
                                        <option value="Lainnya">Lainnya</option>	
                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>

				<!-- <div class="form-group mb-3" id="keterangan_profesi">
                                    <label for="keterangan_profesi">Keterangan Profesi<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="keterangan_profesi" placeholder="Keterangan profesi">
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div> -->


                                <div class="form-group mb-3" id="nama_instansi">
                                    <label for="nama_instansi" id="label_nama_instansi"></label>
                                    <input type="text" class="form-control" name="nama_instansi" id="nama_instansi" placeholder="">
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>

				                <div class="form-group mb-3">
                                    <label for="riwayat_covid">Dosis<span class="text-danger">*</span></label>
				                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dosis" value="Dosis 1" id="dosis1">
                                        <label class="form-check-label" for="dosis">
                                            Dosis 1
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dosis"  value="Dosis 2" id="dosis2">
                                        <label class="form-check-label" for="dosis">
                                            Dosis 2
                                        </label>
                                    </div>
                                </div>

				                <div class="form-group mb-3" id="form_tgl_dosis1">
                                    <label for="tgl_dosis1">Tanggal Pemberian Dosis 1<span class="text-danger">*</span></label>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <input autocomplete="off" type="text" class="form-control" id="tgl_dosis1" name="tgl_dosis1" placeholder="DD-MM-YYYY" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>

                                <div class="form-group mb-3" id="form_jenis_vaksin">
                                    <label for="jenis_vaksin">Jenis Vaksin<span class="text-danger">*</span></label>
                                    <select id="jenis_vaksin" name="jenis_vaksin" class="form-control form-select" required>
                                        <option value="">(Pilih)</option>
                                        <option value="Sinovac/Coronavac">Sinovac/Coronavac</option>
                                        <option value="Sinofarm">Sinofarm</option>
                                        <option value="Astrazaneca">Astrazaneca</option>
                                        <option value="Moderna">Moderna</option>
                                        <!-- @for($jv=0; $jv<count($jenis_vaksin); $jv++)
                                        <option value="{{$jenis_vaksin[$jv]}}">{{$jenis_vaksin[$jv]}}</option>
                                        @endfor -->

                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>

				                <div class="form-group mb-3" id="form_tgl_dosis2">
                                    <label for="tgl_dosis2">Tanggal Akan Diberikan Dosis 2<span class="text-danger">*</span></label>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <input autocomplete="off" type="text" class="form-control" id="tgl_dosis2" name="tgl_dosis2" placeholder="DD-MM-YYYY" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>

                                <div class="form-group mb-3" style="display:none"> 
                                    <label for="riwayat_penyakit">Riwayat Penyakit<span class="text-danger">*</span></label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="tidakada_riwayat" name="riwayat_penyakit[]" value="tidak ada">
                                        <label class="form-check-label" for="flexCheckDefault" style="margin-top:5px">
                                            Tidak Ada Penyakit
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input riwayatpenyakit" type="checkbox" id="" name="riwayat_penyakit[]" value="diabetes">
                                        <label class="form-check-label" for="flexCheckDefault" style="margin-top:5px">
                                            Diabetes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input riwayatpenyakit" type="checkbox" id="" name="riwayat_penyakit[]" value="hipertensi">
                                        <label class="form-check-label" for="flexCheckDefault" style="margin-top:5px">
                                            Hipertensi
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input riwayatpenyakit" type="checkbox" id="" name="riwayat_penyakit[]" value="asma">
                                        <label class="form-check-label" for="flexCheckDefault" style="margin-top:5px">
                                            Asma
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input riwayatpenyakit" type="checkbox" id="" name="riwayat_penyakit[]" value="alergi berat">
                                        <label class="form-check-label" for="flexCheckDefault" style="margin-top:5px">
                                            Alergi Berat
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input riwayatpenyakit" type="checkbox" id="riwayatpenyakit" name="riwayat_penyakit[]" value="Lainnya">
                                        <label class="form-check-label" for="flexCheckDefault" style="margin-top:5px">
                                            Yang Lain :
                                        </label>
                                    </div>
                                    <input type="text" class="form-control" id="riwayatpenyakit_lainnya" name="riwayatpenyakit_lainnya" placeholder="Riwayat Penyakit Kronis Lainnya" style="margin-top: 10px;">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="riwayat_covid">Riwayat Terkonfirmasi Covid-19<span class="text-danger">*</span></label>
                                    <!-- <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="riwayatcovid" name="riwayat_covid" value="riwayat_covid">
                                        <label class="form-check-label" for="flexSwitchCheckDefault">Pernah/Tidak Pernah</label>
                                    </div> -->
				                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="riwayat_covid" value="Pernah">
                                        <label class="form-check-label" for="riwayat_covid">
                                            Pernah
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="riwayat_covid"  value="Tidak Pernah">
                                        <label class="form-check-label" for="riwayat_covid">
                                            Tidak Pernah
                                        </label>
                                    </div>

                                </div>
                                <div class="form-group mb-3" id="riwayat_covid">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <input autocomplete="off" type="text" class="form-control" id="tgl_riwayat_covid" name="tgl_riwayat_covid" placeholder="DD-MM-YYYY">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <label for="ket" id="ket_tambahan" style="text-align: left; font-size: 14px;"><span class="text-danger">*Informasi :
                                                <br>- Mohon menyertakan surat hasil SWAB/PCR saat Vaksinasi
                                                <br>- Jika Pernah Terkonfirmasi Gejala Ringan Atau Sedang Maka Butuh Waktu 1 Bulan Setelah Dinyatakan Sembuh Untuk Mendapatkan Vaksinasi;
                                                <br>- Jika Pernah Terkonfirmasi Gejala Berat Atau Pernah Dirawat Inap Maka Butuh Waktu 3 Bulan Setelah Dinyatakan Sembuh Untuk Mendapatkan Vaksin.</span></label>					    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                
                                <div class="form-group mb-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <label for="no_telp">Nomor Telepon (WhatsApp)<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="no_telp" name="no_telp" maxlength="16" placeholder="Nomor HP Anda" required>
                                            <div class="invalid-feedback">Tidak boleh kosong</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <label for="email">Alamat email</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Alamat Email Anda">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Jadwal Vaksin -->
                        <div class="card mb-5 jadwal_vaksin_widget">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="no-line mb-4">Jadwal Vaksinasi</h5>
                                    </div>
                                    <div class="col text-end">
                                        <small>)<b><span class="text-danger">*</span> = Wajib diisi</b></small>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="nama">Lokasi Vaksin<span class="text-danger">*</span></label>
                                    <select id="lokasi_vaksin" name="lokasi_vaksin" class="form-control form-select" required>
                                        <!-- <option value="Gedung Tenis Indoor Adaro Tamiang Layang">Gedung Tenis Indoor Adaro Tamiang Layang</option> -->
                                      @foreach ($jadwal as $k)
                                            <option value="{{$k->lokasi}}">{{$k->lokasi}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="nama">Tanggal Vaksinasi<span class="text-danger">*</span></label>
                                    <select disabled id="tgl_vaksin" name="tgl_vaksin" class="form-control form-select" required>
                                        @foreach ($jadwal as $k)
                                            <option value="{{$k->tanggal_vaksin}}">{{$k->tanggal_vaksin}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="nama">Sesi Vaksinasi<span class="text-danger">*</span></label>
                                    <select disabled id="sesi_vaksin" name="sesi_vaksin" class="form-control form-select" required>
                                        <option value="118"></option>
                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                            </div>
                        </div>

                        <!-- syarat & ketentuan -->
                        <div class="card mb-5">
                            <div class="card-body">
                                <h2 class="no-line text-center mb-4">Syarat dan Ketentuan</h2>

                                    @if ($syarat)
                                        {!! $syarat->data !!}
                                    @endif

                            </div>
                        </div>

                        <div class="mb-5">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="checkbox" value="" id="check-valid">
                                <label class="form-check-label" for="check-valid">
                                    Data yang saya masukkan benar dan dapat dipertanggungjawabkan
                                </label>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="check-terms">
                                <label class="form-check-label" for="check-terms">
                                    Saya menerima syarat dan ketentuan yang berlaku
                                </label>
                            </div>
                        </div>

                        <div class="d-grid gap-2">
                            <button id="submit" class="btn btn-lg btn-block btn-primary"
                                data-sitekey="6Lc3ISIcAAAAAMpqLlFCIeZb4cvfVuEdCemg89Y1" 
                                data-callback='onSubmit' 
                                data-action='submit' disabled>Submit</button>
                        </div>
                    </div>
                </div>
        </form>
    </div>
@endsection

@section('faq')
    @include('frontend.include.faq')
@endsection

@section('footer')
    @include('frontend.include.footer')
@endsection

@section('custom-script')
    @include('frontend.include.custom-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.css"  />

<script>
    @if (Session::get('status_message'))
        Swal.fire(
            'Gagal Mendaftar',
            "{{Session::get('status_message')}}",
            'error'
        )
    @endisset

    $(".cover-spin").fadeOut(250);
    
    $('#form_checking').submit(function() {
        $(this).find(':input').prop('disabled', false);
        $(".cover-spin").fadeIn(250);
    });

    $(function () {
        $("#tgl_lahir").datepicker({ 
            startView : 2,
            minView : 2,
            changeYear:true,
            startDate: '-69y',
            endDate: '@if ($setting->is_batasan_umur==1){{"-".$setting->batasan_umur."y"}}@else{{"-5y"}}@endif',
            format: 'dd-mm-yyyy'
        });

        $("#tgl_riwayat_covid").datepicker({ 
            startView : 2,
            minView : 2,
            changeYear:true,
            format: 'dd-mm-yyyy'
        });

	 $("#tgl_dosis1").datepicker({ 
            startView : 2,
            minView : 2,
            changeYear:true,
            format: 'dd-mm-yyyy'
        });

        $("#tgl_dosis2").datepicker({ 
            startView : 2,
            minView : 2,
            changeYear:true,
            format: 'dd-mm-yyyy'
        });

    });

    @if($setting->is_nik==1)
    var check_nik = 0;
    $("#nik").on("keyup", function(e) {
        var value_now = $("#nik").val();
        var nik_prefix = '@if($setting->is_nik==1){{$setting->nik}}@endif';
        if (nik_prefix!=""){
            nik_prefix = nik_prefix.split(','); ///array
            for (var i=0;i<nik_prefix.length;i++){
                if (value_now.length >= nik_prefix[i].length){
                    if (value_now.substring(0, nik_prefix[i].length)==nik_prefix[i]){
                        check_nik = 1; 
                        break;
                    } else {
                        check_nik = 0; 
                    }
                } else {
                    check_nik = 1; 
                    break;
                }
            }
        }

        if (check_nik==0){
            $("#nik").val("");
        }
    });
    @endif

    $("#provinsi").change(function() {
        //check if manggarai is checked
        var m_cek = $("#not_manggarai").is(':checked');
        // do stuff!
        if(m_cek){
            $.get( "{{route('pendaftaran.get_kab_kot')}}", { "_token": "{{ csrf_token() }}", "nama": $(this).val() })
            .done(function( res ) {
                $("#kabupaten").html(`<option value="">(Pilih)</option>`)
                for (const val of res) {
                    $("#kabupaten").append(`<option value='`+val.nama+`'>`+val.nama+`</option>`)
                }
            }).fail(function (jqXHR, textStatus) {
                $("#kabupaten").html(`<option value="">(Pilih)</option>`)
            });
            $("#kecamatan").val("");
            $("#kecamatan").attr("disabled","");
            $("#kelurahan").val("");
            $("#kelurahan").attr("disabled","");
        }
        
    });

    $("#kabupaten").change(function() {
        // do stuff!
        if ($(this).val()==""){
            $("#kecamatan").attr("disabled","");
        } else {
            $("#kecamatan").removeAttr("disabled");
            
            $("#kelurahan").attr("disabled","");
            $("#kecamatan").html(`<option value="">(Pilih)</option>`)
            $.get( "{{route('pendaftaran.get_kecamatan')}}", { "_token": "{{ csrf_token() }}", "nama": $(this).val() })
            .done(function( res ) {
                for (const val of res) {
                    $("#kecamatan").append(`<option value='`+val.kode+`'>`+val.nama+`</option>`)
                }
            }).fail(function (jqXHR, textStatus) {
                $("#kecamatan").html(`<option value="">(Pilih)</option>`)
            });
        }
    });

    $("#not_manggarai").change(function() {
        if(this.checked) {
            $(".domisili").show();
            $("#provinsi").attr('required');
            $("#kabupaten").attr('required');
            $("#kecamatan").attr('required');
            $("#kelurahan").attr('required');

            $("#section_keterangan").show();
            $("#keterangan").val("").change();
            $("#keterangan").attr('required');
            $('#nik').off('keyup');
            $("#provinsi > option").show();
            $("#provinsi").removeAttr("disabled");
            // $("#provinsi").val('KALIMANTAN TENGAH').change();

            $("#kabupaten").removeAttr("disabled");
            $("#kabupaten").val("").change();
        } else {
            $(".domisili").hide();
            $("#provinsi").removeAttr('required', true);
            $("#kabupaten").removeAttr('required', true);
            $("#kecamatan").removeAttr('required', true);
            $("#kelurahan").removeAttr('required', true);

            $("#section_keterangan").hide();
            $("#keterangan").val("").change();
            $("#provinsi > option").hide();
            $("#provinsi > option[value='KALIMANTAN TENGAH']").show();
            // $("#provinsi").val('KALIMATAN TENGAH').change();
            $("#provinsi").attr("disabled",'true');

            $("#kabupaten").attr("disabled",'true');
            $("#kabupaten > option[value='BARITO TIMUR']").show();
            $("#kabupaten").val('BARITO TIMUR').change();
            
            @if($setting->is_nik==1)
            var check_nik = 0;
            $("#nik").on("keyup", function(e) {
                var value_now = $("#nik").val();
                var nik_prefix = '@if($setting->is_nik==1){{$setting->nik}}@endif';
                if (nik_prefix!=""){
                    nik_prefix = nik_prefix.split(','); ///array
                    for (var i=0;i<nik_prefix.length;i++){
                        if (value_now.length >= nik_prefix[i].length){
                            if (value_now.substring(0, nik_prefix[i].length)==nik_prefix[i]){
                                check_nik = 1; 
                                break;
                            } else {
                                check_nik = 0; 
                            }
                        } else {
                            check_nik = 1; 
                            break;
                        }
                    }
                }

                if (check_nik==0){
                    $("#nik").val("");
                }
            });
            $("#nik").keyup();
            @endif
        }
        $("#section_keterangan_tambahan").hide();
        $("#keterangan_tambahan").removeAttr('required');
    });

    $("#keterangan").change(function() {
        if ($(this).val()==1){
            $("#section_keterangan_tambahan").show();
            $("#label_keterangan_tambahan").html('Tuliskan tempat anda bekerja');
            $("#keterangan").attr('required');
        } else if ($(this).val()==2){
            $("#section_keterangan_tambahan").show();
            $("#label_keterangan_tambahan").html('Tuliskan nama Kementrian/Lembaga');
            $("#keterangan").attr('required');
        } else if ($(this).val()==3){
            $("#section_keterangan_tambahan").hide();
            $("#label_keterangan_tambahan").html('');
            $("#keterangan").removeAttr('required');
        } else {
            $("#section_keterangan_tambahan").hide();
            $("#label_keterangan_tambahan").html('');
            $("#keterangan").removeAttr('required');
        }
        
        $("#keterangan_tambahan").val('');
        $("#keterangan_tambahan").attr('required');
    });

    var array_jadwal = @json($jadwal);

    $("#kecamatan").on("change", function(e) {
        // do stuff!
        $.get( "{{route('pendaftaran.get_kelurahan')}}", { "_token": "{{ csrf_token() }}", "id": $(this).val() })
        .done(function( res ) {
            $("#kelurahan").html(`<option value="">(Pilih)</option>`)
            for (const val of res) {
                $("#kelurahan").append(`<option value='`+val.kode+`'>`+val.nama+`</option>`)
            }
            if (res.length>0){
                $("#kelurahan").removeAttr('disabled')
            } else {
                $("#kelurahan").attr('disabled','')
            }
        }).fail(function (jqXHR, textStatus) {
            alert("Data Not Found")
        });
    })    
    
    $("#lokasi_vaksin").on("change", function(e) {
        if ($(this).val()==""){
            $("#tgl_vaksin").html(`<option value="">(Pilih)</option>`)
            $("#sesi_vaksin").html(`<option value="">(Pilih)</option>`)
            $("#sesi_vaksin").attr('disabled',true)
            $("#tgl_vaksin").attr('disabled',true)
        } else {
            var selected_lokasi = $(this).val();
            $("#tgl_vaksin").html(`<option value="">(Pilih)</option>`)
            $("#tgl_vaksin").removeAttr('disabled')
            $("#sesi_vaksin").attr('disabled',true)
            $("#sesi_vaksin").html(`<option value="">(Pilih)</option>`)
            for (const val of array_jadwal) {
                if (val.lokasi==selected_lokasi){
                    $("#tgl_vaksin").append(`<option value="`+val.id+`">`+val.tanggal_vaksin+`</option>`);
                }
            }
        }
    });

    $("#tgl_vaksin").on("change", function(e) {
        var selected_tgl = $(this).val();
        $.get( "{{route('pendaftaran.get_sesi')}}", { "_token": "{{ csrf_token() }}", "id": selected_tgl })
        .done(function( res ) {
            $("#sesi_vaksin").html(`<option value="">(Pilih)</option>`)
            var i = 1;
            for (const val of res) {
                if (val.kuota-val.pendaftar_count>0){
                    $("#sesi_vaksin").append(`<option value='`+val.id+`'>`+val.nama+` (`+val.sesi+`) (Sisa kuota: `+(val.kuota-val.pendaftar_count)+`)</option>`)
                    i++;
                } else {
                    $("#sesi_vaksin").append(`<option disabled value='`+val.id+`'>`+val.nama+` (`+val.sesi+`) (Sisa kuota: Habis)</option>`)
                }
            }
            if (res.length>0){
                $("#sesi_vaksin").removeAttr('disabled')
            } else {
                $("#sesi_vaksin").attr('disabled','')
            }
        }).fail(function (jqXHR, textStatus) {
            alert("Data Not Found")
        });
    });

    function ValidateDate(dtValue) {
        var dtRegex = new RegExp('^(0[1-9]|[1-9]|[12][0-9]|3[01])-(0[1-9]|[1-9]|1[012])-(19|20)\\d\\d$');
        return dtRegex.test(dtValue);
    }
    
    //checking terms
    var checked = 0;
    $('#check-valid').change(function() {
        if (ValidateDate($("#tgl_lahir").val())){
            if ($(this).is(':checked')) {
                checked = checked + 1
            } else {
                checked = checked - 1
            }

            if (checked==2){
                $("#submit").removeAttr("disabled");
            } else {
                $("#submit").attr("disabled","");
            }
        } else {
            $("#tgl_lahir").focus();
            $(window).scrollTop($('#tgl_lahir').position().top);
            $('#check-valid').prop('checked', false);
            Swal.fire(
                'Gagal Mendaftar',
                "Format tanggal lahir yang benar adalah DD-MM-YYYY. Contoh 29-08-1996",
                'error'
            );
        }

    });


    $('#check-terms').change(function() {

        if (ValidateDate($("#tgl_lahir").val())){
            if ($(this).is(':checked')) {
                checked = checked + 1
            } else {
                checked = checked - 1
            }

            if (checked==2){
                $("#submit").removeAttr("disabled");
            } else {
                $("#submit").attr("disabled","");
            }
        } else {
            $("#tgl_lahir").focus();
            $(window).scrollTop($('#tgl_lahir').position().top);
            $('#check-terms').prop('checked', false);
            Swal.fire(
                'Gagal Mendaftar',
                "Format tanggal lahir yang benar adalah DD-MM-YYYY. Contoh 29-08-1996",
                'error'
            );
        }
    });

    $(document).ready(function() {
        $('#riwayatpenyakit_lainnya').hide();
        $('#riwayat_covid').hide();

        $('#status_profesi').hide();
        $('#nama_instansi').hide();
        $("#keterangan_profesi").hide();

        $('.domisili').hide();
        $("#is_pregnant").hide();

        $("#form_tgl_dosis1").hide();
        $("#form_tgl_dosis2").hide();
        $("#form_jenis_vaksin").hide();
        });

    $('#riwayatpenyakit').on("click", function() {
        if(!$('#riwayatpenyakit_lainnya').is(':visible')) {
            $('#riwayatpenyakit_lainnya').show();
            $('#riwayatpenyakit_lainnya').attr("required", true);
        }
        else {
            $('#riwayatpenyakit_lainnya').hide();
            $('#riwayatpenyakit_lainnya').attr("required", false);
        }
    });

    $('#riwayatcovid').on("click", function() {
        if(!$('#riwayat_covid').is(':visible')) {
            $('#riwayat_covid').show();
            $('#tgl_riwayat_covid').attr("required", true);
        }
        else {
            $('#riwayat_covid').hide();
            $('#tgl_riwayat_covid').attr("required", false);
        }
    });

    $('#tidakada_riwayat').on('click', function() {
        if (this.checked) {
            $("input.riwayatpenyakit").attr("disabled", true);
        } else {
            $("input.riwayatpenyakit").removeAttr("disabled");
        }
    });

    $('input[type=radio][name=status_pekerjaan]').change(function() {
        if (this.value == 'Bekerja') {
            $("#status_profesi").show();
            $('#statusprofesi').attr("required", true);
	        $("#nama_instansi").hide();
            $("#status_profesi").show();
	    
            //$('#nama_instansi input').attr("required", true);
        // if ($('#status_profesi').value == 'Lainnya') {
        //     $("#field_status_profesi").show();
        // }
        } 
        else if(this.value == 'Pelajar/Mahasiswa') {
            $("#nama_instansi").show();
            $('#nama_instansi input').attr("required", true);
            $( "#label_nama_instansi" ).html('Nama Sekolah/Universitas<span class="text-danger">*</span>');
            $("#status_profesi").hide();
        }
        else {
            $("#status_profesi").hide();
            $('#statusprofesi').attr("required", false);
            $("#nama_instansi").hide();
            $('#nama_instansi input').attr("required", false);
        }
    });

    $('#status_profesi select').on('change', function() {
	if(this.value == 'PNS' || this.value == 'Honorer') {
	    $("#nama_instansi").show();
        $('#nama_instansi input').attr("required", true);
	    $( "#label_nama_instansi" ).html('Nama Instansi<span class="text-danger">*</span>');
        $("#keterangan_profesi").show();
        }
	else if(this.value == 'TNI' || this.value == 'POLRI' ) {
	    $("#nama_instansi").show();
        $('#nama_instansi input').attr("required", true);
	    $( "#label_nama_instansi" ).html('Kesatuan<span class="text-danger">*</span>');
        $("#keterangan_profesi").show();
        }
	else if(this.value == 'Pelajar/Mahasiswa') {
	    $("#nama_instansi").show();
        $('#nama_instansi input').attr("required", true);
	    $( "#label_nama_instansi" ).html('Nama Sekolah/Universitas<span class="text-danger">*</span>');
        $("#keterangan_profesi").show();
        }
	else if(this.value == 'Swasta') {
	    $("#nama_instansi").show();
        $('#nama_instansi input').attr("required", true);
	    $( "#label_nama_instansi" ).html('Nama Tempat Bekerja<span class="text-danger">*</span>');
            $("#keterangan_profesi").show();
        }
    else if(this.value == 'Lainnya') {
	    $("#nama_instansi").show();
        $('#nama_instansi input').attr("required", true);
	    $( "#label_nama_instansi" ).html('Keterangan<span class="text-danger">*</span>');
        $("#keterangan_profesi").show();
        }
    else {
	    $("#nama_instansi").hide();
        $('#nama_instansi input').attr("required", false);
        $("#keterangan_profesi").hide();
        }
    });

    $('input[type=radio][name=riwayat_covid]').change(function() {
        if (this.value == 'Pernah') {
            $("#riwayat_covid").show();
            $('#riwayat_covid').attr("required", true);
        } else {
            $("#riwayat_covid").hide();
            $('#riwayat_covid').attr("required", false);
        }
    });

    $('input[type=radio][name=dosis]').change(function() {
        if (this.value == 'Dosis 2') {
            $("#form_tgl_dosis1").show();
            $('#tgl_dosis1').attr("required", true);
            $("#form_tgl_dosis2").show();
            $('#tgl_dosis2').attr("required", true);
            $("#form_jenis_vaksin").show();
            $('#jenis_vaksin').attr("required", true);
        } else {
            $("#form_tgl_dosis1").hide();
            $('#tgl_dosis1').attr("required", false);
            $("#form_tgl_dosis2").hide();
            $('#tgl_dosis2').attr("required", false);
            $("#form_jenis_vaksin").hide();
            $('#jenis_vaksin').attr("required", false);
        }
    });

    $('#gender select').on('change', function() {
	if(this.value == '1') {
	    $("#is_pregnant").show();
        $('input[type=radio][name=is_pregnant]').attr("required", true);
        }
    else {
	    $("#is_pregnant").hide();
        $('input[type=radio][name=is_pregnant]').attr("required", false);
        }
    });
</script>
@endsection