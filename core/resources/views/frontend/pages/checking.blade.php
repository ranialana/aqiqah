@extends('frontend.layouts.apps')

@section('title','Checking - Pendaftaran Vaksinasi Polres Barito Timur')

@section('meta')
    @include('frontend.include.meta')
@endsection

@section('header')
    @include('frontend.include.header')
    
<style>
.grecaptcha-badge {
    z-index: 9999;
}
</style>
@endsection

@section('content')
<div class="container table-content">
    <div class="text-center">
        <h1>
            Cek Data Pendaftaran Aqiqah<br>
        </h1>
    </div>
    <div class="row">
        <form id="form_checking" action="{{route('checking_post')}}" method="post" accept-charset="utf-8">
            {{ csrf_field() }}  
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group mb-3">
                            <label for="username">Kode Pendaftaran<span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-lg" id="code" name="code" placeholder="Kode Pendaftaran" maxlength="16">
                            <div class="invalid-feedback">Tidak boleh kosong</div>
                        </div>
                        <div class="row mt-3">
                            <div class="col d-grid gap-2">
                                <button type="submit" class="btn btn-primary g-recaptcha"
                                data-callback='onSubmit' 
                                data-action='submit'><i class="bi bi-search"></i> Cek</button>
                            </div>
                        </div>

                        <!-- Search Result simpen di bawah -->

                    </div>
                </div>
            </div>

        </form>

        <div class="col-sm-12">
            <div class="card">
                 <div class="card-body">

                 <div class="row mt-3">
                        <div class="col d-grid gap-2">
                            <img class="d-block mx-auto mb-4" src="{{asset('assets/img/cs.png')}}" alt="logo" height="200">
                            <!-- <img class="ms-4 me-0 object-fit-contain" src="{{asset('assets/img/cs.png')}}" alt="" width="350" style="float:right;margin-right:5px"> -->
                            </div>
                            <div class="col d-grid gap-2">
                                <label for="username">Untuk pembatalan, peserta dapat menghubungi Panitia pada : <br>Ika Iqlima - 087821968369 (WA Text Only)<br>Zulfikar Hakim - 08981911551 (WA Text Only)<span class="text-danger"></span></label>
                                <a target="_blank" href="https://api.whatsapp.com/send/?phone=6287821968369&text=Halo%2C+%0ANIK+%3A+%0ANama+%3A+%0AIngin+membatalkan+pendaftaran+registrasi+saya&app_absent=0" class="btn btn-primary" style="background-color:#25D366; height:50px !important"><i class="bi bi-whatsapp"></i>Hubungi Kami</a>
                            </div>
                            
                        </div>

                        <!-- Search Result simpen di bawah -->

                    </div>
                </div>
</div>
    </div>

</div>
@endsection

@section('footer')
    @include('frontend.include.footer')
@endsection

@section('custom-script')
    @include('frontend.include.custom-script')
    
<script>
    @if (Session::get('status_message'))
        Swal.fire(
            'Gagal Mendaftar',
            "{{Session::get('status_message')}}",
            'error'
        )
    @endisset
    
    function onSubmit(token) {
        document.getElementById("form_checking").submit();
    }
</script>
@endsection
