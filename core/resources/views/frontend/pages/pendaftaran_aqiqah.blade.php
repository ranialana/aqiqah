@extends('frontend.layouts.apps')

@section('title','Pendaftaran Peserta Aqiqah')

@section('meta')
    @include('frontend.include.meta')
@endsection

@section('header')
    @include('frontend.include.header')
<style>
.btn-primary:disabled {
    color: #fff;
    background-color: #58606c !important;
    border-color: #58606c!important;
}
.grecaptcha-badge {
    z-index: 9999;
}
.cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:99999;
}
.loader {
    width: 30px;
    height: 30px;
    margin:auto;
    left:0;
    right:0;
    top:0;
    bottom:0;
    position:fixed;
    z-index:9999;
}

.jadwal_vaksin_widget{
    display: none !important;
}

.sub_judul {
    margin-top: -10px;
}
.sub_judul::after {
content: none;
}

</style>

@endsection

@section('content')
    <div class="cover-spin">
        <div class="loader">
            <div class="spinner-grow text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <div class="container table-content">
        
        <form id="form_checking" action="{{route('pendaftaran_post_aqiqah')}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            {{ csrf_field() }}    
            <div class="text-center">
                <h1>
                    Formulir Pendaftaran Peserta Aqiqah
                </h1>
            </div>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Data Peserta -->
                        <div class="card mb-5">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="no-line mb-4">Identitas Peserta Aqiqah</h5>
                                    </div>
                                    <div class="col text-end">
                                        <small>)<b><span class="text-danger">*</span> = Wajib diisi</b></small>
                                    </div>
                                </div>
                                
                                <div class="form-group mb-3">
                                    <label for="nama">Nama Lengkap Pendaftar<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama lengkap Pendaftar" required maxlength="50" onkeyup="cekNamaLengkap(this.maxLength, this.value, this)">
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <div class="form-group mb-3 div_jumlah">
                                    <label for="nama">Jumlah Anggota Keluarga<span class="text-danger">*</span></label>
                                    <p><small>(Pilih Jumlah Anggota Keluarga/Rekan yang akan ikut didaftarkan)</small></p>
                                    <select class="form-control" id="jumlah_peserta" onchange="addForm()" required name="jumlah_anggota">
                                        <option value="">Pilih</option>
                                        <option value="0" selected>Tidak Ada</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <!--
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        -->
                                    </select>
                                    <div class="invalid-feedback">Tidak boleh kosong</div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="nama">Sisa Kuota<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" readonly value="{{$sisa_kuota}}">
                                </div>
                                <div id="form-member">	
                                </div>
                            </div>
                        </div>
                        <!-- syarat & ketentuan -->
                        <div class="d-grid gap-2">
                            <button id="submit" class="btn btn-lg btn-block btn-primary"
                                data-sitekey="6Lc3ISIcAAAAAMpqLlFCIeZb4cvfVuEdCemg89Y1" 
                                data-callback='onSubmit' 
                                data-action='submit'>Submit</button>
                        </div>
                    </div>
                </div>
        </form>
    </div>
    



@endsection

@section('footer')
    @include('frontend.include.footer')
@endsection

@section('custom-script')
    @include('frontend.include.custom-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.css"  />

<script>
    @if (Session::get('status_message'))
        Swal.fire(
            'Gagal Mendaftar',
            "{{Session::get('status_message')}}",
            'error'
        )
    @endisset

    $(".cover-spin").fadeOut(250);
    function addForm(){
        var i = 1;
		var jumlah = document.getElementById('jumlah_peserta').value;
		var mydiv = document.getElementById("form-member");
		mydiv.innerHTML = "";
            <?php $j = 1; ?>
			for(i=1; i<=jumlah; i++){
				var div = document.createElement("div");
				mydiv.appendChild(div);
				div.className ="dynamicDiv";
				div.innerHTML = '<div class="the-member">'+
									'<div class="row">'+
										'<div class="form-group mb-3"><label>Nama Lengkap Anggota Keluarga Ke- '+i+' *</label>'+
                                            '<input type="text" class="form-control" id="nama_anggota" name="nama_anggota[]" placeholder="Nama Anggota Keluarga" required maxlength="50" onkeyup="cekNamaLengkap(this.maxLength, this.value, this)">'+
                                            '<div class="invalid-feedback">Tidak boleh kosong</div>'+
                                        '</div>'+
									'</div>'+
								'</div>';
			}
		}

        function cekNamaLengkap(maxLength, value, element){
            if(value.length == maxLength){
                $(element.parentElement).children('.invalid-feedback').show().text('Maksimum 50 karakter');
            }else{
                $(element.parentElement).children('.invalid-feedback').text('').hide();
            }
        }

       
    function getValueForm(){

        var tot_nama_pendaftar = 0;
        var total = 0;

        var anggota = [];
        var nama_pendaftar = $('#nama').val();
        var count_anggota  = $('#nama_anggota').length;
        tot_nama_pendaftar = $('#nama').length;

        var html2 = '<table>'+
        '<tr><td style="text-align:left;">Nama Pendaftar</td><td style="text-align:left;">'+ nama_pendaftar +'</td></tr>';
        if(count_anggota > 0){
            var arr = $('input[name="nama_anggota[]"]').map(function () {
                anggota.push(this.value);
            }).get();
        }

        total = tot_nama_pendaftar + anggota.length;
        if(anggota.length > 0){
            for (let i = 0; i < anggota.length; i++) {
                html2 += '<tr><td style="text-align:left;">Nama Anggota Keluarga</td><td style="text-align:left;">'+ anggota[i] +'</td></tr>';
            }
        }

        html2 += '<tr><td>&nbsp;&nbsp;&nbsp;</td></tr>';
        html2 += '<tr><td><strong>Total Yang Hadir</strong></td> <td>'+ total +'</td></tr>';
        html2 += '</table>';
        return html2;
    }

    $("#form_checking").submit(function (e) {
        var html_form = getValueForm();
        e.preventDefault();
        Swal.fire({  
            title: "<h3>Konfirmasi? </h3>", 
            html: html_form,   
            showCancelButton: true,  
            confirmButtonText: 'Simpan',  
            denyButtonText: 'Batal',
            confirmButtonColor: "#FFBF00",
        }).then((result) => {  
        if (result.isConfirmed) {  
            $("#form_checking").off("submit").submit();
            $("#submit").click();
            $(".cover-spin").fadeIn(250);  
        } else if (result.isDenied) {    
    	    return false;
 	    }
    });
});
</script>
@endsection