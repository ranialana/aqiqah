<html>
<head>
    <title>@yield('title')</title>
    @yield('meta')
</head>
<body>
    <main>
        <section id="list">
            @yield('header')
            @yield('content')           
        </section>
    </main>
</div>

@yield('faq')
@yield('footer')
</body>
@yield('custom-script')
</html>