<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegisterVaksin extends Model
{
    //
    use SoftDeletes;
    protected $table = 't_register_vaksin';
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    public function sesi()
    {
        return $this->hasOne(SesiJadwal::class,'id','t_sesi_vaksin_id');
    }

    public function penerima()
    {
        return $this->hasOne(PenerimaVaksin::class,'nik','penerima_vaksin_nik');
    }
}
