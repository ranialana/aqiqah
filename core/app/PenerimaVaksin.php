<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenerimaVaksin extends Model
{
    //
    protected $table = 'penerima_vaksin';
    protected $guarded = [];

    public function register()
    {
        return $this->hasOne(RegisterVaksin::class,'penerima_vaksin_nik','nik');
    }
}
