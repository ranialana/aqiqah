<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Syarat;

class SyaratController extends Controller
{
    //
    public function index()
    {
        $syarat = Syarat::where("kategori",1)->first();
        return view('adminpage.syarat.index',[ 'syarat' => $syarat]);
    }

    public function store(Request $request)
    {
        if (Syarat::where("kategori",1)->first()){
            $query = Syarat::where("kategori",1)->update([
                'data' => $request->data,
                'created_at' => now()
            ]);
        } else {
            $query = Syarat::create([
                'data' => $request->data,
                'kategori' => 1,
                'created_at' => now()
            ]);
        }

        if ($query){
            return redirect()->route('admin.syarat.index')->with(['status' => 1 ,'status_message' => "Berhasil Menambahkan"]);
        } else {
            return redirect()->route('admin.syarat.index')->with(['status' => -1 ,'status_message' => "Gagal Menambahkan"]);
        }
    }
}
