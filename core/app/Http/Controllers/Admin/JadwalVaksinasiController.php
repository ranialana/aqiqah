<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JadwalVaksin;
use App\SesiJadwal;
use App\KabKot;
use Illuminate\Support\Facades\Auth;

class JadwalVaksinasiController extends Controller
{
    public function index()
    {
        if (Auth::user()->role==3){
            $jadwal = JadwalVaksin::with(['sesi','kota'])->orderBy('id',"DESC")->where("kota_id",Auth::user()->kota_id)->get();

            $kota = KabKot::where("kode_prov",62)->get();
        } else {
            $jadwal = JadwalVaksin::with(['sesi','kota'])->orderBy('id',"DESC")->get();
            $kota = KabKot::where("kode_prov",62)->get();
        }
        return view('adminpage.jadwal_vaksinasi.index',[ 'jadwal' => $jadwal,'kota' => $kota]);
    }

    public function add()
    {
        $kota = KabKot::where("kode_prov",62)->get();
        return view('adminpage.jadwal_vaksinasi.add',['kota' => $kota]);
    }

    public function store(Request $request)
    {
        
        if (Auth::user()->role==3){
            $jadwal = JadwalVaksin::create([
                'lokasi' => $request->lokasi,
                'tanggal_vaksin' => $request->tanggal,
                'kota_id' => Auth::user()->kota_id,
                'created_at' => now()
            ]);

        } else {
            $jadwal = JadwalVaksin::create([
                'lokasi' => $request->lokasi,
                'tanggal_vaksin' => $request->tanggal,
                'kota_id' => $request->kota_id,
                'created_at' => now()
            ]);
        }
        
    
        $i=0;
        for ($i=0;$i<count($request->timeAwal);$i++){
            SesiJadwal::create([
                'id_m_jadwal_vaksin' => $jadwal->id,
                'nama' => "Sesi ".($i+1),
                'sesi' => $request->timeAwal[$i]."-".$request->timeAkhir[$i],
                'kuota' => $request->kuota[$i],
                'created_at' => now()
            ]);
        }
        
        if ($jadwal){
            return redirect()->route('admin.jadwal_vaksinasi.index')->with(['status' => 1 ,'status_message' => "Berhasil Menambahkan"]);
        } else {
            return redirect()->route('admin.jadwal_vaksinasi.index')->with(['status' => -1 ,'status_message' => "Gagal Menambahkan"]);
        }
    }

    public function update(Request $request)
    {
        if (Auth::user()->role==3){
            $jadwal = JadwalVaksin::where('id',$request->id)->update([
                'lokasi' => $request->lokasi,
                'tanggal_vaksin' => $request->tanggal,
                'updated_at' => now()
            ]);
        } else{
            $jadwal = JadwalVaksin::where('id',$request->id)->update([
                'lokasi' => $request->lokasi,
                'tanggal_vaksin' => $request->tanggal,
                'kota_id' => $request->kota_id,
                'updated_at' => now()
            ]);
        }

        $i=0;
        if (is_array($request->idDelete)){
            for ($i=0;$i<count($request->idDelete);$i++){
                SesiJadwal::where('id',$request->idDelete[$i])->delete();
            }
        }

        $i=0;
        if (is_array($request->idExist)){
            for ($i=0;$i<count($request->idExist);$i++){
                $id_sesi = $request->idExist[$i];
                SesiJadwal::where('id',$id_sesi)->update([
                    'sesi' => $request->timeAwal[$i]."-".$request->timeAkhir[$i],
                    'kuota' => $request->kuota[$i],
                    'updated_at' => now()
                ]);
            }
        }

        $i=0;
        if (is_array($request->kuotaNew)){
            $last_sesi = explode("Sesi ",SesiJadwal::where('id_m_jadwal_vaksin',$request->id)->orderBy('id', 'desc')->first()["nama"])[1]+1;
            for ($i=0;$i<count($request->kuotaNew);$i++){
                SesiJadwal::create([
                    'id_m_jadwal_vaksin' => $request->id,
                    'nama' => "Sesi". $last_sesi,
                    'sesi' => $request->timeAwalNew[$i]."-".$request->timeAkhirNew[$i],
                    'kuota' => $request->kuotaNew[$i],
                    'created_at' => now()
                ]);
                $last_sesi++;
            }
        }

        if ($jadwal){
            return 1;
        } else {
            return 0;
        }
    }

    public function delete(Request $request)
    {
        $query = JadwalVaksin::where('id',$request->id)->delete();
        SesiJadwal::where('id_m_jadwal_vaksin',$request->id)->delete();
        if ($query){
            return 1;
        } else {
            return 0;
        }
    }
}
