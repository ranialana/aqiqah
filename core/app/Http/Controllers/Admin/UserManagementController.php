<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\KabKot;
use Illuminate\Support\Facades\Auth;

class UserManagementController extends Controller
{
    public function index()
    {
        if (Auth::user()->role==3){
            $resultQuery = User::join('role', 'users.role', '=', 'role.id')
            ->select('users.*', 'role.nama_role as nama_role', 'role.id as role_id')
            ->where("kota_id",Auth::user()->kota_id)
            ->get();
        } else {
            $resultQuery = User::join('role', 'users.role', '=', 'role.id')
            ->select('users.*', 'role.nama_role as nama_role', 'role.id as role_id')
            ->get();
        }

        $kota = KabKot::where("kode_prov",53)->get();

    	return view('adminpage.user_management.index',['resultQuery' => $resultQuery,'kota' => $kota]);
    }

    public function add()
    {
        $roles = Role::get();

        $kota = KabKot::where("kode_prov",53)->get();
    	return view('adminpage.user_management.add',['roles' => $roles,'kota' => $kota]);
    }

    public function store(Request $request)
    {
        try {
            if (Auth::user()->role==3){
                $insertQueryUser = User::insert([
                    'name' => $request->nama,
                    'username' => $request->email,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'role' => 3,
                    'kota_id' => Auth::user()->kota_id,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            } else {
                $insertQueryUser = User::insert([
                    'name' => $request->nama,
                    'username' => $request->email,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'role' => $request->role,
                    'kota_id' => $request->kota_id,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }

        } catch (\Exception $e) {
            return redirect()->route('admin.user_management.index')->with(['status' => -1 ,'status_message' => "Email yang digunakan sudah terdaftar"]);
        }

        return redirect()->route('admin.user_management.index')->with(['status' => 1 ,'status_message' => "Berhasil Menambahkan"]);
    }

    public function update(Request $request)
    {
        if ($request->password){
            // update data
            try {
                $updateQuery = User::where('id',$request->id)->update([
                    'name' => $request->nama,
                    'username' => $request->email,
                    'email' => $request->email,
                    'kota_id' => $request->kota_id,
                    'password' => bcrypt($request->password),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                return 1;
            } catch (\Exception $e) {
                error_log($e);
                return 0;
            }
        } else {

            // update data
            try {
                $updateQuery = User::where('id',$request->id)->update([
                    'name' => $request->nama,
                    'username' => $request->email,
                    'email' => $request->email,
                    'kota_id' => $request->kota_id,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                return 1;
            } catch (\Exception $e) {
                error_log($e);
                return 0;
            }
        }
    }

    public function delete(Request $request)
    {
        $deleteQuery = User::where('id', $request->id)->delete();
        if ($deleteQuery) {
            // returns true
    	    return 1;
        }
        else {
            // returns false
    	    return 0;
        }
    }
}
