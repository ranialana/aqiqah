<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\KabKot;
use App\SettingForm;
use Illuminate\Support\Facades\Auth;

class SettingFormController extends Controller
{
    //
    public function index()
    {
        if (Auth::user()->role==1){
            $kota = KabKot::where('kode_prov','62')->orderBy('id',"DESC")->get();
            $setting = SettingForm::first();
            return view('adminpage.setting_form.index',[ 'kota' => $kota, 'data' => $setting]);
        } else {
            return redirect()->route('admin.pendaftaran.index');
        }
    }

    public function store(Request $request)
    {
        if (Auth::user()->role==1){
            if ($request->is_nik=="on"){
                $is_nik = 1;
            } else {
                $is_nik = 0;
            }

            if ($request->is_kota=="on"){
                $is_kota = 1;
            } else {
                $is_kota = 0;
            }

            if ($request->is_batasan_umur=="on"){
                $is_batasan_umur = 1;
            } else {
                $is_batasan_umur = 0;
            }
            $query = SettingForm::whereId('1')->update([
                'batasan_umur' => $request->tgl_lahir,
                'is_batasan_umur' => $is_batasan_umur,
                'kota_id' => $request->kota,
                'is_kota' => $is_kota,
                'nik' => $request->nik,
                'is_nik' => $is_nik,
                'updated_at' => now()
            ]);
            if ($query){
                return redirect()->route('admin.setting_form.index')->with(['status' => 1 ,'status_message' => "Berhasil Mensunting Data"]);
            } else {
                return redirect()->route('admin.setting_form.index')->with(['status' => -1 ,'status_message' => "Gagal Mensunting Data"]);
            }
        } else {
            return redirect()->route('admin.pendaftaran.index');
        }
    }
}
