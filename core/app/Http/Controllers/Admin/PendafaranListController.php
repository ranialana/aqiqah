<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Syarat;
use App\Kecamatan;
use App\Kelurahan;
use App\JadwalVaksin;
use App\SesiJadwal;
use App\PenerimaVaksin;
use App\RegisterVaksin;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use App\PendaftarAqiqah;

class PendafaranListController extends Controller
{
    //

    public function index_aqiqah(Request $request){
       $data['aqiqah'] = PendaftarAqiqah::get();
       return view('adminpage.pendaftaran.index_aqiqah', $data);
    }

    public function index(Request $request)
    {
        $textFilter = "";

        if($request->has('location')) {
            $location = $request->location;
            $sesi = $request->sesi;
            if($request->sesi !='null' && $request->location !='null'){
                $data = RegisterVaksin::where('is_deleted',0)->where('t_sesi_vaksin_id',$sesi)->with(['sesi' => function($query) {
                    $query->with(['jadwal' => function($query) {
                        $query->withTrashed();
                    }]);
                    $query->withTrashed();
                }])->whereHas('penerima',function($q){
                    $q->where('is_deleted',0);
                })->orderBy('id','desc');

                if (Auth::user()->role==3){
                    $data = $data->whereHas('sesi', function($q) {
                        $q->whereHas('jadwal', function($q) {
                            $q->where('kota_id',Auth::user()->kota_id);
                            $q->withTrashed();
                        });
                        $q->withTrashed();
                    })->get();
                } else {
                    $data = $data->get();
                }

                if(count($data)>0){
                    $textFilter = 'Lokasi '.$data[0]->sesi->jadwal->lokasi.' Tanggal '.strftime("%d-%m-%Y", strtotime($data[0]->sesi->jadwal->tanggal_vaksin)).' '.$data[0]->sesi->nama.' ('.$data[0]->sesi->sesi.')';
                }
            }elseif($request->sesi =='null' && $request->location !='null' ){
                
                $data = RegisterVaksin::where('is_deleted',0)->whereHas('sesi.jadwal', function($q) use($location){
                    $q->where('id',$location);
                })->whereHas('penerima',function($q){
                    $q->where('is_deleted',0);
                })->orderBy('id','desc');

                if (Auth::user()->role==3){
                    $data = $data->whereHas('sesi', function($q) {
                        $q->whereHas('jadwal', function($q) {
                            $q->where('kota_id',Auth::user()->kota_id);
                            $q->withTrashed();
                        });
                        $q->withTrashed();
                    })->get();
                } else {
                    $data = $data->get();
                }

                if(count($data)>0){
                    $textFilter = 'Lokasi '.$data[0]->sesi->jadwal->lokasi.' Tanggal '.strftime("%d-%m-%Y", strtotime($data[0]->sesi->jadwal->tanggal_vaksin));
                }
                
            }else{
                $data = RegisterVaksin::where('is_deleted',0)->with(['sesi' => function($query) {
                    $query->with(['jadwal' => function($query) {
                        $query->withTrashed();
                    }]);
                    $query->withTrashed();
                }])->whereHas('penerima',function($q){
                    $q->where('is_deleted',0);
                })->orderBy('id','desc')->limit(5000);

                if (Auth::user()->role==3){
                    $data = $data->whereHas('sesi', function($q) {
                        $q->whereHas('jadwal', function($q) {
                            $q->where('kota_id',Auth::user()->kota_id);
                            $q->withTrashed();
                        });
                        $q->withTrashed();
                    })->get();
                } else {
                    $data = $data->get();
                }
            }

        }
        elseif($request->has('pekerjaan')) {
            $pekerjaan = $_GET['pekerjaan'];
            $data = RegisterVaksin::where('is_deleted',0)->with(['sesi' => function($query){
                $query->with(['jadwal' => function($query){
                    $query->withTrashed();
                }]);
                $query->withTrashed();
            }])->whereHas('penerima',function($q) use($pekerjaan){
                $q->where('is_deleted',0)->where('status_profesi', $pekerjaan)->orWhere('status_pekerjaan', $pekerjaan);
            })->orderBy('id','desc')->limit(5000);

            if (Auth::user()->role==3){
                $data = $data->whereHas('sesi', function($q) use($pekerjaan) {
                    $q->whereHas('jadwal', function($q) use($pekerjaan) {
                        $q->where('kota_id',Auth::user()->kota_id);
                        $q->withTrashed();
                    });
                    $q->withTrashed();
                })->get();
            } else {
                $data = $data->get();
            }
        }
        else {
            $data = RegisterVaksin::where('is_deleted',0)->with(['sesi' => function($query){
                $query->with(['jadwal' => function($query){
                    $query->withTrashed();
                }]);
                $query->withTrashed();
            }])->whereHas('penerima',function($q){
                $q->where('is_deleted',0);
            })->orderBy('id','desc')->limit(5000);

            if (Auth::user()->role==3){
                $data = $data->whereHas('sesi', function($q) {
                    $q->whereHas('jadwal', function($q) {
                        $q->where('kota_id',Auth::user()->kota_id);
                        $q->withTrashed();
                    });
                    $q->withTrashed();
                })->get();
            } else {
                $data = $data->get();
            }
        }
        

        //if role user admin_wilayah
        if (Auth::user()->role==3){
            $jadwal = JadwalVaksin::with(['sesi' => function($query) {
                $query->with(['jadwal' => function($query){
                    $query->withTrashed();
                }]);
                $query->withTrashed();
            }])->where('kota_id',Auth::user()->kota_id)->orderBy('id',"DESC")->get(); 

        } else {
            $jadwal = JadwalVaksin::with(['sesi' => function($query) {
                $query->with(['jadwal' => function($query){
                    $query->withTrashed();
                }]);
                $query->withTrashed();
            }])->orderBy('id',"DESC")->get(); 
        }
        // echo dd($jadwal[0]->tanggal_vaksin);


        return view('adminpage.pendaftaran.index',['data' => $data, 'jadwal' => $jadwal, 'text' => $textFilter]);
    }

    public function approve(Request $request)
    {
        $data = RegisterVaksin::where("id",$request->id)->update([
            'is_verified' => 1
        ]);

        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function cancel(Request $request)
    {
        $data = RegisterVaksin::where("id",$request->id)->update([
            'is_verified' => -1
        ]);

        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function check_in(Request $request)
    {
        $data = RegisterVaksin::where("id",$request->id)->update([
            'is_verified' => 1
        ]);
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function detail(Request $request)
    {
        $data = PendaftarAqiqah::where("id",$request->id)->first();
        if ($data){
            return view('adminpage.pendaftaran.detail_aqiqah',['data' => $data]);
        } else {
            return redirect()->route('admin.pendaftaran_aqiqah.index');
        }
    }

    public function detail_qr_aqiqah(Request $request)
    {
        
        $data = RegisterVaksin::where("code",$request->id)->first();
        if ($data) {
            $nik  = $data->penerima_vaksin_nik;
            $data = PendaftarAqiqah::where('nik', $nik)->first();
        } else {
            $data = PendaftarAqiqah::where('nik', $request->id)->first();
            if ($data){
                $nik = $request->id;
                $data = $data;
            } else {
                return redirect()->route('admin.pendaftaran.index');
            }
        }
        if ($data){
            return view('adminpage.pendaftaran.detail_qr_aqiqah',['data' => $data]);
        } else {
            return redirect()->route('admin.pendaftaran.index');
        }
        
    }

    public function detail_qr(Request $request)
    {
        $data = RegisterVaksin::where("code",$request->id)->first();
        if ($data) {
            $nik = $data->penerima_vaksin_nik;
        } else {
            $data = RegisterVaksin::where("penerima_vaksin_nik",$request->id)->whereHas('penerima',function($q){
                $q->where('is_deleted',0);
            })->first();
            if ($data){
                $nik = $request->id;
            } else {
                return redirect()->route('admin.pendaftaran.index');
            }
        }

        $data = PenerimaVaksin::with(['register' => function($query) {
            $query->with(['sesi' => function($query) {
                $query->with(['jadwal' => function($query) {
                    $query->withTrashed();
                }]);
                $query->withTrashed();
            }]);
        }])->where("nik",$nik)->first();
        
        if ($data){
            return view('adminpage.pendaftaran.detail_qr',['data' => $data]);
        } else {

            return redirect()->route('admin.pendaftaran.index');
        }
    }

    public function screening_pdf(Request $request)
    {
        $register = RegisterVaksin::where("code",$request->code)->first();
        if ($register) {
            $query = PenerimaVaksin::where("nik",$register->penerima_vaksin_nik)->first();

            $sesi = SesiJadwal::where("id",$register->t_sesi_vaksin_id)->withTrashed()->first();
            $jadwal = JadwalVaksin::where("id",$sesi["id_m_jadwal_vaksin"])->withTrashed()->first();
            $unique_id = $register->code;

            $qr_code = new DNS2D();
            $qr_code = $qr_code->getBarcodePNG($unique_id, 'QRCODE');

            $syarat = Syarat::where("kategori",1)->first();

            $html = view('export.pendaftaran_pdf',['data_pendaftar' => $query,"sesi_vaksin" => $sesi->sesi,"sesi_nama" => $sesi->nama,"jadwal" => $jadwal,"unique_id" => $unique_id,"qr" => $qr_code,"syarat" => $syarat])->render();

            $pdf = PDF::loadHTML($html)->setOption('enable-local-file-access', true)->setPaper('a4');
            $pdf->setOption("encoding","UTF-8");
            $pdf->setOption('margin-left', '0mm');
            $pdf->setOption('margin-right', '0mm');
            $pdf->setOption('margin-top', '0mm');
            $pdf->setOption('margin-bottom', '0mm');
            $pdf->save(public_path("/pdf/".$unique_id.'.pdf'),$overwrite = true);

            $file = public_path("/pdf/".$unique_id.'.pdf');

            $headers = array(
                'Content-Type: application/pdf',
            );

           return Response::download($file, $unique_id.'.pdf', $headers);

        } else {
            // return redirect()->route('home')->with(['status' => -1 ,'status_message' => "Anda belum melakukan pendaftaran"]);
            return redirect()->route('admin.pendaftaran.index');
        }
    }

    public function export_print(Request $request)
    {
        $data = PendaftarAqiqah::get();
    	return view('adminpage.pendaftaran.export_print_aqiqah',['data' => $data]);
    }
    
    public function export_excel(Request $request)
    {
        try {

            $data = PendaftarAqiqah::get();
            $textFilter = "";
            $title = WriterEntityFactory::createRowFromArray(["No","Kode Pendaftaran","Nama Pendaftar","Jumlah Anggota Keluarga","Total Hadir","Anggota"]);
            $fileName = date('Y-m-d').'-PendaftaranDaftar.xls';

            $writer = WriterEntityFactory::createXLSXWriter(); // for XLSX files

            $writer->openToBrowser($fileName); // stream data directly to the browser

            $writer->addRow($title);

            $jumlah_anggota_total = 0;
            $total_hadir          = 0;
            foreach ($data as $key => $d) {
                $jumlah_anggota_total = $d->jumlah_anggota+1;
                $angg         = array();
                $string_array = "";
                if($d->anggota_keluarga !=null || $d->anggota_keluarga !=NULL || $d->anggota_keluarga !=""){
                    $angg = json_decode($d->anggota_keluarga);
                    $string_array =  implode(",",$angg);
                }
                $row_data = WriterEntityFactory::createRowFromArray([$key+1,$d->nik,$d->nama, $d->jumlah_anggota, $jumlah_anggota_total, $string_array]);
                $writer->addRow($row_data);

                $total_hadir += $jumlah_anggota_total;
            }
            $row_data2 = WriterEntityFactory::createRowFromArray(['Total','', '', '', $total_hadir,'']);
            $writer->addRow($row_data2);
            $writer->close();
        } catch (\Exception $e) {
            error_log($e);
        }
    }

    public function scan(Request $request)
    {
        return view('adminpage.scan.index');
    }

    public function qr(Request $request)
    {
        $data = RegisterVaksin::where("code",$request->code)->first();
        if ($data) {
            return $data->penerima_vaksin_nik;
        } else {
            $data = RegisterVaksin::where("penerima_vaksin_nik",$request->code)->first();
            if ($data){
                return $request->code;
            } else {
                return 0;
            }
        }
    }

    public function sesiPendaftaran($id)
    {
        $sesi = SesiJadwal::where('id_m_jadwal_vaksin',$id)->orderBy('id','ASC')->get();
        return $sesi;
    }

    public function HapusPendaftaran($id)
    {
        $tmp = '';
        $data = PendaftarAqiqah::where('id',$id)->first();
        $tmp = $data->nik;
        $data->delete();

        $data_register = RegisterVaksin::where('penerima_vaksin_nik',$tmp)->first();
        $data_register->delete();
        return redirect()->back()->with('success', 'Data Berhasil dihapus');
    }
}