<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;

class FaqController extends Controller
{
    //
    public function index()
    {
        $faq = Faq::orderBy('id',"DESC")->get();
        return view('adminpage.faq.index',[ 'faq' => $faq]);
    }

    public function add()
    {
        return view('adminpage.faq.add');
    }

    public function store(Request $request)
    {
        $query = Faq::insert([
            'pertanyaan' => $request->pertanyaan,
            'jawaban' => $request->jawaban,
            'created_at' => now()
        ]);
        if ($query){
            return redirect()->route('admin.faq.index')->with(['status' => 1 ,'status_message' => "Berhasil Menambahkan"]);
        } else {
            return redirect()->route('admin.faq.index')->with(['status' => -1 ,'status_message' => "Gagal Menambahkan"]);
        }
    }

    public function edit(Request $request)
    {
        $faq = Faq::where('id',$request->id)->first();
        if ($faq){
            return view('adminpage.faq.edit',['faq' => $faq]);
        } else {
            return redirect()->route('admin.faq.index');
        }
    }

    public function update(Request $request)
    {
        $query = Faq::where('id',$request->id)->update([
            'pertanyaan' => $request->pertanyaan,
            'jawaban' => $request->jawaban,
            'updated_at' => now()
        ]);
        if ($query){
            return redirect()->route('admin.faq.index')->with(['status' => 1 ,'status_message' => "Berhasil Edit"]);
        } else {
            return redirect()->route('admin.faq.index')->with(['status' => -1 ,'status_message' => "Gagal Edit"]);
        }
    }

    public function delete(Request $request)
    {
        $query = Faq::where('id',$request->id)->delete();
        if ($query){
            return 1;
        } else {
            return 0;
        }
    }
}
