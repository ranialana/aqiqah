<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\KabKot;
use App\SettingSubJudul;
use App\JadwalVaksin;
use Illuminate\Support\Facades\Auth;

class SettingSubJudulController extends Controller
{
    public function index()
    {
        if (Auth::user()->role==1){
            $jadwal = JadwalVaksin::with(['sesi','kota'])->orderBy('id',"DESC")->get();
            $kota = KabKot::where("kode_prov",53)->get();
            $setting = SettingSubJudul::first();
            return view('adminpage.setting_subjudul.index',[ 'kota' => $kota, 'data' => $setting, 'jadwal' => $jadwal]);
        } else {
            return redirect()->route('admin.pendaftaran.index');
        }
    }

    public function store(Request $request)
    {
        if (Auth::user()->role==1){
            $data = [];
            $data['judul'] = $request->judul;
            $data['jenis_vaksin'] = json_encode($request->jenis_vaksin);
            
            // if($request->tempat) {
            //     $getJadwal = JadwalVaksin::find($request->tempat);

            //     if($getJadwal) {
            //         $data['tempat'] = $getJadwal->tempat;
            //         $data['tanggal'] = $getJadwal->tanggal;
            //     }
            // }
            $query = SettingSubJudul::whereId('1')->update($data);
            if ($query){
                return redirect()->route('admin.setting_subjudul.index')->with(['status' => 1 ,'status_message' => "Berhasil Mensunting Data"]);
            } else {
                return redirect()->route('admin.setting_subjudul.index')->with(['status' => -1 ,'status_message' => "Gagal Mensunting Data"]);
            }
        } else {
            return redirect()->route('admin.pendaftaran.index');
        }
    }
}
