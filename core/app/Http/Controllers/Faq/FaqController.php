<?php

namespace App\Http\Controllers\Faq;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;

class FaqController extends Controller
{
    //
    public function index()
    {
        $faq = Faq::orderBy('id',"DESC")->get();
        return view('frontend.pages.faq', ['faq' => $faq]);
    }
}
