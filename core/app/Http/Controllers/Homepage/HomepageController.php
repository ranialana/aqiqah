<?php

namespace App\Http\Controllers\Homepage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Syarat;
use App\SettingSubJudul;

class HomepageController extends Controller
{
    //
    public function index()
    {
        $syarat = Syarat::where("kategori",1)->first();
        $setting_sub_judul = SettingSubJudul::find(1);

        return view('welcome',[ 'syarat' => $syarat, 'setting_sub_judul' => $setting_sub_judul]);
    }
}
