<?php

namespace App\Http\Controllers\Checking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Syarat;
use App\Kecamatan;
use App\Kelurahan;
use App\JadwalVaksin;
use App\SesiJadwal;
use App\PenerimaVaksin;
use App\RegisterVaksin;
use Illuminate\Support\Str;
use Session;
use App\PendaftarAqiqah;

class CheckingController extends Controller
{
    //
    public function index()
    {
        return view('frontend.pages.checking');
    }

    
    public function qr(Request $request){
        $data = PendaftarAqiqah::where("nik",$request->code)->first();
        if($data){
            $nik = $request->code;
        } else {
            return redirect()->route('checking')->with(['status' => -1 ,'status_message' => "Kode Pendaftaran tidak valid"]);
        }
        $query = PendaftarAqiqah::where("nik",$nik)->first();
        Session::put('data_aqiqah', $query);
        
        return redirect()->route('konfirmasi_aqiqah')->with(['status' => 1]);
    }
}
