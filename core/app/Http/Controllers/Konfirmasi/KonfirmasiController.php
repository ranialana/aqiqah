<?php

namespace App\Http\Controllers\Konfirmasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Syarat;
use App\JadwalVaksin;
use App\PendaftarAqiqah;
use App\SesiJadwal;
use App\PenerimaVaksin;
use App\RegisterVaksin;
use \Milon\Barcode\DNS1D;
use \Milon\Barcode\DNS2D;
use PDF;
use Session;
use Response;
use PDF2;

class KonfirmasiController extends Controller
{
    //
    public function index()
    {
        if (Session::get('data_pendaftar')){
            $syarat = Syarat::where("kategori",1)->first();
            return view('frontend.pages.konfirmasi',[ 'syarat' => $syarat]);
        } else {
            return redirect()->route('home')->with(['status' => -1 ,'status_message' => "Anda belum melakukan pendaftaran"]);
        }
    }

    public function konfirmasi_pdf(Request $request)
    {
        $register = RegisterVaksin::where("code",$request->code)->first();
        if ($register) {
            $query = PenerimaVaksin::where("nik",$register->penerima_vaksin_nik)->first();

            $sesi = SesiJadwal::where("id",$register->t_sesi_vaksin_id)->withTrashed()->first();
            $jadwal = JadwalVaksin::where("id",$sesi["id_m_jadwal_vaksin"])->withTrashed()->first();
            $unique_id = $register->code;

            $qr_code = new DNS2D();
            $qr_code = $qr_code->getBarcodePNG($unique_id, 'QRCODE');

            $syarat = Syarat::where("kategori",1)->first();

            $html = view('export.pendaftaran_pdf',['data_pendaftar' => $query,"sesi_vaksin" => $sesi->sesi,"sesi_nama" => $sesi->nama,"jadwal" => $jadwal,"unique_id" => $unique_id,"qr" => $qr_code,"syarat" => $syarat])->render();

            $pdf = PDF::loadHTML($html)->setOption('enable-local-file-access', true)->setPaper('a4');
            $pdf->setOption("encoding","UTF-8");
            $pdf->setOption('margin-left', '0mm');
            $pdf->setOption('margin-right', '0mm');
            $pdf->setOption('margin-top', '0mm');
            $pdf->setOption('margin-bottom', '0mm');
            $pdf->save(public_path("/pdf/".$unique_id.'.pdf'),$overwrite = true);

            $file = public_path("/pdf/".$unique_id.'.pdf');

            $headers = array(
                'Content-Type: application/pdf',
            );

           return Response::download($file, $unique_id.'.pdf', $headers);

        } else {
            return redirect()->route('home')->with(['status' => -1 ,'status_message' => "Anda belum melakukan pendaftaran"]);
        }
    }

    

    public function format_screening_pdf(Request $request)
    {
        $register = RegisterVaksin::where("code",$request->code)->first();
        if ($register) {
            $query = PenerimaVaksin::where("nik",$register->penerima_vaksin_nik)->first();

            $sesi = SesiJadwal::where("id",$register->t_sesi_vaksin_id)->withTrashed()->first();
            $jadwal = JadwalVaksin::where("id",$sesi["id_m_jadwal_vaksin"])->withTrashed()->first();
            $unique_id = $register->code;

            $qr_code = new DNS2D();
            $qr_code = $qr_code->getBarcodePNG($unique_id, 'QRCODE');

            $syarat = Syarat::where("kategori",1)->first();

            $html = view('export.screening_pdf',['data_pendaftar' => $query,"sesi_vaksin" => $sesi->sesi,"sesi_nama" => $sesi->nama,"jadwal" => $jadwal,"unique_id" => $unique_id,"qr" => $qr_code,"syarat" => $syarat])->render();

            $pdf = PDF::loadHTML($html)->setOption('enable-local-file-access', true)->setPaper('a4');
            $pdf->setOption("encoding","UTF-8");
            $pdf->setOption('margin-left', '0.6cm');
            $pdf->setOption('margin-right', '0.6cm');
            $pdf->setOption('margin-top', '0.2cm');
            $pdf->setOption('margin-bottom', '0.2cm');
            $pdf->save(public_path("/pdf/".$unique_id.'.pdf'),$overwrite = true);

            $file = public_path("/pdf/".$unique_id.'.pdf');

            $headers = array(
                'Content-Type: application/pdf',
            );

           return Response::download($file, $unique_id.'.pdf', $headers);

        } else {
            // return redirect()->route('home')->with(['status' => -1 ,'status_message' => "Anda belum melakukan pendaftaran"]);
        }
    }

    public function konfirmasi_aqiqah()
    {
        $tes = Session::get('data_aqiqah');
        if (Session::get('data_aqiqah')){
            return view('frontend.pages.konfirmasi_aqiqah');
        } else {
            return redirect()->route('home')->with(['status' => -1 ,'status_message' => "Anda belum melakukan pendaftaran"]);
        }
    }

    public function konfirmasi_pdf_aqiqah(Request $request)
    {

        $register = RegisterVaksin::where("code",$request->code)->first();
        if ($register) {
            $data['query'] = PendaftarAqiqah::where("nik",$register->penerima_vaksin_nik)->first();
            $unique_id = $register->code;
            $qr_code = new DNS2D();
            $data['qr_code'] = $qr_code->getBarcodePNG($unique_id, 'QRCODE');
            $data['syarat'] = Syarat::where("kategori",1)->first();
            $headers = array(
                'Content-Type: application/pdf',
            );
          $pdf = PDF2::loadview('export.pendaftaran_aqiqah', $data);
    	  return $pdf->download($register->penerima_vaksin_nik.'.pdf', $headers);

        } else {
            return redirect()->route('home')->with(['status' => -1 ,'status_message' => "Anda belum melakukan pendaftaran"]);
        }
    }
}
