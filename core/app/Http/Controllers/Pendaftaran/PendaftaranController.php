<?php

namespace App\Http\Controllers\Pendaftaran;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Syarat;
use App\Kecamatan;
use App\Kelurahan;
use App\JadwalVaksin;
use App\SesiJadwal;
use App\PenerimaVaksin;
use App\RegisterVaksin;
use App\SettingForm;
use App\KabKot;
use App\Provinsi;
use App\SettingSubJudul;
use Illuminate\Support\Str;
use Session;
use Illuminate\Support\Facades\Mail;
use Config;
use Illuminate\Support\Facades\DB;
use App\PendaftarAqiqah;


class PendaftaranController extends Controller
{
    //
    public function aqiqah(){
        $sisa_kuota = 0;
        $sisa_kuota = $this->sisa_kuota();
        $data['sisa_kuota'] = $sisa_kuota;
        return view('frontend.pages.pendaftaran_aqiqah', $data);
    }

    public function sisa_kuota(){
        $t_seksi_vaksin = 120;
        $kuotaAwal = 0;
        $cek_count_pendaftar        = 0;
        $cek_count_anggota_keluarga = 0;
        $count_total = 0;
        $sisa = 0;
        $jum_anggota = 0;
        $cekKuota = DB::table('t_sesi_vaksinasi')->where('id', $t_seksi_vaksin)->first();
        if($cekKuota){
            $kuotaAwal = $cekKuota->kuota;
            $cek_count_pendaftar = RegisterVaksin::where('t_sesi_vaksin_id', $t_seksi_vaksin)->count();
            $cek_count_anggota_keluarga = PendaftarAqiqah::join('t_register_vaksin', 'pendaftar_aqiqah.nik', '=', 't_register_vaksin.penerima_vaksin_nik')->where('t_sesi_vaksin_id',$t_seksi_vaksin)->sum('pendaftar_aqiqah.jumlah_anggota');

            $count_total = $cek_count_pendaftar + $cek_count_anggota_keluarga;
            $sisa = $kuotaAwal - $count_total; 
        }
        return $sisa;
    }
    public function index()
    {
        $syarat = Syarat::where("kategori",1)->first();
        $jadwal = JadwalVaksin::with('sesi')->orderBy('id',"DESC")->get();
        $setting = SettingForm::first();
        $setting_sub_judul = SettingSubJudul::find(1);
        
        if ($setting->is_kota==1){
            $kota = KabKot::where("kode",$setting->kota_id)->first();
            $kecamatan = Kecamatan::where("kode_kabkot",$setting->kota_id)->get();
        } else {
            $kota = KabKot::where("kode",6212)->first();
            $kecamatan = Kecamatan::where("kode_kabkot",5315)->get();
        }

        $provinsi = Provinsi::get();
        
        setlocale(LC_ALL, 'IND');
        foreach ($jadwal as $j) {
            $j->tanggal_vaksin = strftime('%A, %d %B %Y',strtotime($j->tanggal_vaksin));
        }
        return view('frontend.pages.pendaftaran',['syarat' => $syarat,'kecamatan' => $kecamatan,'kota' => $kota,'jadwal' => $jadwal,'setting' => $setting,'provinsi' => $provinsi, 'setting_sub_judul' => $setting_sub_judul]);
    }

    public function getKabKot(Request $request)
    {
        $provinsi = Provinsi::where("nama",$request->nama)->first()->kode;
        $kota = KabKot::where("kode_prov",$provinsi)->get();
        return $kota;
    }


    public function getKecamatan(Request $request)
    {
        $kota = KabKot::where("nama",$request->nama)->first()->kode;
        $kecamatan = Kecamatan::where("kode_kabkot",$kota)->get();
        return $kecamatan;
    }


    public function getKelurahan(Request $request)
    {
        $kelurahan = Kelurahan::where("kode_kec",$request->id)->get();
        return $kelurahan;
    }

    public function getSesi(Request $request)
    {
        $jadwal = SesiJadwal::withCount('pendaftar')->where("id_m_jadwal_vaksin",$request->id)->get();
        return $jadwal;
    }

    public function store(Request $request)
    {
        // try{
            $jadwal = SesiJadwal::withCount('pendaftar')->where("id",$request->sesi_vaksin)->first();
            //check kuota
            if ($jadwal->kuota-$jadwal->pendaftar_count>0){
                $checking = PenerimaVaksin::Where('nik',$request->nik)->where('is_deleted',0)->first();
                if (!$checking){
                    $kecamatan = '';
                    $kelurahan = '';
                    if($request->kecamatan) {
                        $kecamatan = Kecamatan::where("kode",$request->kecamatan)->first()["nama"];
                    }
                    if($request->kelurahan) {
                        $kelurahan = Kelurahan::where("kode",$request->kelurahan)->first()["nama"];
                    }

                    
                    $keterangan = "";
                    if ($request->keterangan==1){
                        $keterangan = "Pelaku usaha wisata di Barito Timur";
                    } else if ($request->keterangan==2){
                        $keterangan = "Perjalanan Dinas Pegawai Aparatur Sipil Negara/ASN";
                    } else if ($request->keterangan==3){
                        $keterangan = "Pelaku perjalan wisata, sertakan surat keterangan domisili Barito Timur";
                    } 

                    $data_riwayat_penyakit = [];
                    for($i=0; $i<count($request->riwayat_penyakit); $i++) {
                        if($request->riwayat_penyakit[$i] == 'Lainnya') {
                            $data_riwayat_penyakit['lainnya'] = $request->riwayatpenyakit_lainnya;
                        }
                        else {
                            $data_riwayat_penyakit[$request->riwayat_penyakit[$i]] = $request->riwayat_penyakit[$i];

                        }
                    }

                    $data_riwayat_covid = [
                        'status' => 0,
                        'tanggal' => ''
                    ];

                    if($request->riwayat_covid == "Pernah") {
                        $data_riwayat_covid['status'] = 1;
                        $data_riwayat_covid['tanggal'] = $request->tgl_riwayat_covid;
                    }

                    $status_pekerjaan = '';
                    $status_profesi = '';
                    $nama_instansi = '';
                    $keterangan_profesi = '';

                    if($request->status_pekerjaan == 'Bekerja' || $request->status_pekerjaan == 'Pelajar/Mahasiswa') {
                        $status_pekerjaan = $request->status_pekerjaan;
                        $status_profesi = $request->status_profesi;
                        $nama_instansi = $request->nama_instansi;
                        if($status_profesi == 'Lainnya'){
                            $keterangan_profesi = $request->$keterangan_profesi;
                        }
                    }
                    else {
                        $status_pekerjaan = $request->status_pekerjaan;
                    }

                    $data_domisili = [
                        'provinsi' => '',
                        'kabupaten' => '',
                        'kecamatan' => '',
                        'kelurahan' => ''
                    ];

                    if($request->provinsi) {
                        $data_domisili['provinsi'] = $request->provinsi;
                    }
                    if($request->kabupaten) {
                        $data_domisili['kabupaten'] = $request->kabupaten;
                    }
                    if($request->kecamatan) {
                        $data_domisili['kecamatan'] = $kecamatan;
                    }
                    if($request->kelurahan) {
                        $data_domisili['kelurahan'] = $kelurahan;
                    }

                    $nama_file = '';

                    if($request->file) {
                        $file = $request->file('file');
 
                        $nama_file = time()."_".$file->getClientOriginalName();
                
                                // isi dengan nama folder tempat kemana file diupload
                        $tujuan_upload = 'KK_KTP/';
                        $file->move($tujuan_upload,$nama_file);
                    }

                    $query = PenerimaVaksin::create([
                        'nik' => $request->nik,
                        'nama' => $request->nama,
                        'tanggal_lahir' => $request->tgl_lahir,
                        'jenis_kelamin' => $request->gender,
                        'is_pregnant' => $request->is_pregnant,
                        'alamat_tinggal' => $request->alamat,
                        'domisili' => json_encode($data_domisili),
                        'status_pekerjaan' => $status_pekerjaan,
                        'status_profesi' => $status_profesi,
			            //'keterangan_profesi' => $keterangan_profesi,
                        'nama_instansi' => $request->nama_instansi,
                        'dosis' => $request->dosis,
                        'tgl_dosis1' => $request->tgl_dosis1,
                        'tgl_dosis2' => $request->tgl_dosis2,
                        'jenis_vaksin' => $request->jenis_vaksin,
                        'riwayat_penyakit' => json_encode($data_riwayat_penyakit),
                        'riwayat_covid' => json_encode($data_riwayat_covid),
                        // 'ref_prov' => $request->provinsi,
                        // 'ref_kabkot' => $request->kabupaten,
                        // 'ref_kec' => $kecamatan,
                        // 'ref_kel' => $kelurahan,
                        'no_telp' => $request->no_telp,
                        'email' => $request->email,
                        'keterangan' => $keterangan,
                        'keterangan_tambahan' => $request->keterangan_tambahan,
                        'file' => $nama_file,
                        'created_at' => now()
                    ]);
    
                    $unique_id = Str::random(10);
                    RegisterVaksin::create([
                        'code' => $unique_id,
                        'penerima_vaksin_nik' => $request->nik,
                        't_sesi_vaksin_id' => $request->sesi_vaksin,
                        'is_verified' => 0
                    ]);
    
                    $sesi = SesiJadwal::where("id",$request->sesi_vaksin)->first();
                    $jadwal = JadwalVaksin::where("id",$sesi["id_m_jadwal_vaksin"])->first();
                    Session::put('data_pendaftar', $query);
                    Session::put('sesi_vaksin', $sesi->sesi);
                    Session::put('sesi_nama', $sesi->nama);
                    Session::put('jadwal', $jadwal);
                    Session::put('unique_id', $unique_id);
    
                    try{
                        $this->send_email($request->email,$request->nama,$unique_id);
                    } catch (\Exception $e) {
                    }
                    return redirect()->route('konfirmasi')->with(['status' => 1]);
                } else {
                    return redirect()->route('pendaftaran')->with(['status' => -1 ,'status_message' => "Nomor NIK sudah terdaftar silahkan cek registrasi anda"]);
                }
            } else {
                return redirect()->route('pendaftaran')->with(['status' => -1 ,'status_message' => "Kuota yang anda pilih sudah habis, Silahkan ganti jadwal"]);
            }
        // } catch (\Exception $e) {
        //     return redirect()->route('pendaftaran')->with(['status' => -1 ,'status_message' => "Internal Server Error"]);
        // }
    }

    private $mail_app = "Support Vaksinasi Polres Barito Timur";

    public function send_email($email,$name,$code){
        $mail_app = $this->mail_app;
        $subject= "Informasi Pendaftaran Vaksinasi Polres Barito Timur";

        $to_email = $email;
        $data = array('name'=>$name,'code'=>$code);
        $template= 'export.pendaftaran_email';
        $from_email = Config::get('value.username');
        
        Mail::send($template, $data, function($message) use ($to_email, $subject, $from_email ,$mail_app) {
            $message->to($to_email)->subject($subject);
            $message->from($from_email, $mail_app);
        });
        return 1;
    }

    public function store_aqiqah(Request $request)
    {
        $t_seksi_vaksin = 120;
        $kuotaAwal = 0;
        $cek_count_pendaftar        = 0;
        $cek_count_anggota_keluarga = 0;
        $count_total = 0;
        $sisa = 0;
        $jum_anggota = 0;
        $cekKuota = DB::table('t_sesi_vaksinasi')->where('id', $t_seksi_vaksin)->first();
        if($cekKuota){
            $kuotaAwal = $cekKuota->kuota;
            $cek_count_pendaftar = RegisterVaksin::where('t_sesi_vaksin_id', $t_seksi_vaksin)->count();
            $cek_count_anggota_keluarga = PendaftarAqiqah::join('t_register_vaksin', 'pendaftar_aqiqah.nik', '=', 't_register_vaksin.penerima_vaksin_nik')->where('t_sesi_vaksin_id',$t_seksi_vaksin)->sum('pendaftar_aqiqah.jumlah_anggota');

            $count_total = $cek_count_pendaftar + $cek_count_anggota_keluarga;
            $sisa = $kuotaAwal - $count_total; 
            if($request->jumlah_anggota){
                $jum_anggota = $request->jumlah_anggota + 1;
            }       
        if ($sisa>0 && (($sisa - $jum_anggota) >= 0)){
            
        try {
            DB::beginTransaction();
            $uniqid = "";
            $unique_id = Str::random(10);
            $pendaftar = new PendaftarAqiqah;
            $pendaftar->nama = $request->nama;
            $pendaftar->nik  = $unique_id;
            $pendaftar->jumlah_anggota   = $request->jumlah_anggota;
            if($request->nama_anggota){
                if(count($request->nama_anggota) > 0){
                    $pendaftar->anggota_keluarga = json_encode($request->nama_anggota);
                }
            } 
            $pendaftar->save();
            RegisterVaksin::create([
                'code' => $unique_id,
                'penerima_vaksin_nik' => $unique_id,
                't_sesi_vaksin_id' => $t_seksi_vaksin,
                'is_verified' => 0
            ]);

            Session::put('data_aqiqah', $pendaftar);
            Session::put('unique_id', $unique_id);
            DB::commit();
            return redirect()->route('konfirmasi_aqiqah')->with(['status' => 1]);
        } catch (Exception $e) {
            //dd($e);
            DB::rollBack();
            return redirect()->back()->with('flash_notification', 'Registrasi Gagal, Data yang anda masukan tidak lengkap atau email yang anda daftarkan sudah terdaftar');
        }
    }else{
        return redirect()->route('pendaftaran-aqiqah')->with(['status' => -1 ,'status_message' => "Maaf kuota yang anda pilih melebihi dari kuota yang tersedia"]);
    }
}else{
    return redirect()->route('pendaftaran-aqiqah')->with(['status' => -1 ,'status_message' => "Maaf kuota yang anda pilih melebihi dari kuota yang tersedia"]);
}
        
    }
}


