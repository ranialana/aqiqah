<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) // I included this check because you have it, but it really should be part of your 'auth' middleware, most likely added as part of a route group.
            return redirect('admin.pendaftaran.index');

        $user = Auth::user();

        if ($user->role==1 || $user->role==3){
            return $next($request);
        } else {
            return redirect('admin.pendaftaran.index');
        }
    }
}
