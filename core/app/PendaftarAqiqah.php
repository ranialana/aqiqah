<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PendaftarAqiqah extends Model
{
    use SoftDeletes;
    protected $table = 'pendaftar_aqiqah';
    protected $dates = ['deleted_at'];
}
