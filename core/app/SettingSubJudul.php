<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingSubJudul extends Model
{
    protected $table = 'setting_subjudul';
    protected $guarded = [];
}
