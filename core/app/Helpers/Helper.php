<?php
function HariIdn($hari) {
    $hari_bulan = '';
    $hariIndo = [
        'Sunday' => 'Minggu',
        'Monday' => 'Senin',
        'Tuesday' => 'Selasa',
        'Wednesday' => 'Rabu',
        'Thursday' => 'Kamis',
        'Friday' => 'Jumat',
        'Saturday' => 'Sabtu'
    ];

    if(array_key_exists($hari, $hariIndo)) {
        return $hariIndo[$hari];
    }
    else {
        return 'Tidak Ada';
    }
}

function BulanIdn($bulan) {
    $bulanIndo = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
    if(array_key_exists($bulan, $bulanIndo)) {
        return $bulanIndo[$bulan];
    }
    else {
        return 'Tidak Ada';
    }
}
?>