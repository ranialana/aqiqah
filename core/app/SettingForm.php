<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingForm extends Model
{
    //
    protected $table = 'setting_form';
    protected $guarded = [];
}
