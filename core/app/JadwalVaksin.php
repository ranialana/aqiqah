<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JadwalVaksin extends Model
{
    //
    use SoftDeletes;
    
    protected $table = 'm_jadwal_vaksin';
    protected $guarded = [];

    public function sesi()
    {
        return $this->hasMany(SesiJadwal::class,'id_m_jadwal_vaksin','id');
    }

    public function kota()
    {
        return $this->hasOne(KabKot::class,'kode','kota_id');
    }
}
