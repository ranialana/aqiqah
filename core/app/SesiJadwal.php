<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SesiJadwal extends Model
{
    //
    use SoftDeletes;
    protected $table = 't_sesi_vaksinasi';
    protected $guarded = [];

    public function jadwal()
    {
        return $this->hasOne(JadwalVaksin::class,'id','id_m_jadwal_vaksin');
    }

    public function pendaftar()
    {
        return $this->hasMany(RegisterVaksin::class,'t_sesi_vaksin_id','id');
    }
}
