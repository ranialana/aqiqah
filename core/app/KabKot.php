<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KabKot extends Model
{
    //
    protected $table = 'ref_kabkot';
    protected $guarded = [];
}
