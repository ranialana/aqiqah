Highcharts.chart('visitors-chart', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        'Padar',
        'Loh Liang',
        'Loh Buaya',
        'Karang Makasar',
        'Batu Bolong',
        'Mawan',
        'Kelimutu'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
        text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
        pointPadding: 0.2,
        borderWidth: 0
        }
    },
    series: [{
        name: 'Visitors',
        data: [120, 300, 550, 450, 300, 210]

    }]
});

var gaugeOptions = {
    chart: {
        type: 'solidgauge'
    },

    title: null,

    pane: {
        center: ['50%', '35%'],
        size: '100%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
        }
    },

    exporting: {
        enabled: false
    },

    tooltip: {
        enabled: false
    },

    // the value axis
    yAxis: {
        stops: [
        [0.1, '#55BF3B'], // green
        [0.5, '#DDDF0D'], // yellow
        [0.9, '#DF5353'] // red
        ],
        lineWidth: 0,
        tickWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
        y: -70
        },
        labels: {
        y: 16
        }
    },

    plotOptions: {
        solidgauge: {
            // innerRadius: '100%',
            dataLabels: {
                y: 0,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

// The speed gauge
var chartSpeed = Highcharts.chart('gauge-1', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 200,
        title: {
        text: ''
        }
    },

    credits: {
        enabled: false
    },

    series: [{
    name: 'Speed',
    data: [80],
    dataLabels: {
    format:
        '<div style="text-align:center">' +
        '<span style="font-size:25px">{y}</span><br/>' +
        '<span style="font-size:12px;opacity:0.4"></span>' +
        '</div>'
    },
    tooltip: {
    valueSuffix: ' Visitors'
    }
}]

}));

var chartSpeed = Highcharts.chart('gauge-2', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 200,
        title: {
        text: ''
        }
    },

    credits: {
        enabled: false
    },

    series: [{
        name: 'Speed',
        data: [80],
        dataLabels: {
        format:
            '<div style="text-align:center">' +
            '<span style="font-size:25px">{y}</span><br/>' +
            '<span style="font-size:12px;opacity:0.4"></span>' +
            '</div>'
        },
        tooltip: {
        valueSuffix: ' Visitors'
        }
    }]

}));

var chartSpeed = Highcharts.chart('gauge-3', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 200,
        title: {
        text: ''
        }
    },

    credits: {
        enabled: false
    },

    series: [{
        name: 'Speed',
        data: [80],
        dataLabels: {
        format:
            '<div style="text-align:center">' +
            '<span style="font-size:25px">{y}</span><br/>' +
            '<span style="font-size:12px;opacity:0.4"></span>' +
            '</div>'
        },
        tooltip: {
        valueSuffix: ' Visitors'
        }
    }]

}));

var chartSpeed = Highcharts.chart('gauge-4', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 200,
        title: {
        text: ''
        }
    },

    credits: {
        enabled: false
    },

    series: [{
        name: 'Speed',
        data: [80],
        dataLabels: {
        format:
            '<div style="text-align:center">' +
            '<span style="font-size:25px">{y}</span><br/>' +
            '<span style="font-size:12px;opacity:0.4"></span>' +
            '</div>'
        },
        tooltip: {
        valueSuffix: ' Visitors'
        }
    }]

}));

var chartSpeed = Highcharts.chart('gauge-5', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 200,
        title: {
        text: ''
        }
    },

    credits: {
        enabled: false
    },

    series: [{
        name: 'Speed',
        data: [80],
        dataLabels: {
        format:
            '<div style="text-align:center">' +
            '<span style="font-size:25px">{y}</span><br/>' +
            '<span style="font-size:12px;opacity:0.4"></span>' +
            '</div>'
        },
        tooltip: {
        valueSuffix: ' Visitors'
        }
    }]

}));

var chartSpeed = Highcharts.chart('gauge-6', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 200,
        title: {
        text: ''
        }
    },

    credits: {
        enabled: false
    },

    series: [{
        name: 'Speed',
        data: [80],
        dataLabels: {
        format:
            '<div style="text-align:center">' +
            '<span style="font-size:25px">{y}</span><br/>' +
            '<span style="font-size:12px;opacity:0.4"></span>' +
            '</div>'
        },
        tooltip: {
        valueSuffix: ' Visitors'
        }
    }]

}));




// Bring life to the dials
setInterval(function () {
    // Speed
        var point,
            newVal,
            inc;

        if (chartSpeed) {
            point = chartSpeed.series[0].points[0];
            inc = Math.round((Math.random() - 0.5) * 100);
            newVal = point.y + inc;

            if (newVal < 0 || newVal > 200) {
            newVal = point.y - inc;
            }

            point.update(newVal);
        }

        // RPM
        if (chartRpm) {
            point = chartRpm.series[0].points[0];
            inc = Math.random() - 0.5;
            newVal = point.y + inc;

            if (newVal < 0 || newVal > 5) {
            newVal = point.y - inc;
            }

            point.update(newVal);
        }
    }, 2000);